//! Displays the program as a Coq term for use with the formalized
//! semantics.

use std::fmt;
use ast::*;

pub struct DisplayAsCoq<'a>(pub &'a Program);

impl<'a> fmt::Display for DisplayAsCoq<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(writeln!(f, "Require Import List ZArith String."));
        try!(writeln!(f, "Require Import program types tactics."));
        try!(writeln!(f, "Import List.ListNotations."));
        try!(writeln!(f, "Local Open Scope Z_scope."));
        try!(writeln!(f, "Local Open Scope string_scope."));
        try!(writeln!(f, ""));
        for vd in &self.0.var_decls {
            try!(write!(f, "Definition var_{} := ", &vd.name.id));
            try!(disp_expr(&vd.expr, f));
            try!(writeln!(f, "."));

            if let Some(ref ty) = vd.the_type {
                try!(write!(f, "Definition type_var_{} := ", &vd.name.id));
                try!(disp_type(ty, f));
                try!(writeln!(f, "."));
            }
        }

        for fd in &self.0.fun_decls {
            try!(write!(f, "Definition fun_{} := ", &fd.name.id));
            try!(disp_vars_then_stmts(&fd.var_decls, &fd.stmts, f));
            try!(writeln!(f, "."));

            if let Some(ref fty) = fd.the_type {
                try!(write!(f, "Definition type_fun_{} := ", &fd.name.id));
                try!(disp_fun_type(fty, f));
                try!(writeln!(f, "."));
            }
        }

        let mut closers = 0;
        try!(write!(f, "Definition the_program := "));
        for vd in &self.0.var_decls {
            try!(write!(f, "(PVal \"{}\" var_{} ", &vd.name.id, &vd.name.id));
            closers += 1;
        }
        for fd in &self.0.fun_decls {
            try!(write!(f, "(PFun \"{}\" ", &fd.name.id));
            try!(disp_list(&fd.arg_names, |x, f| write!(f, "\"{}\"", &x.id), f));
            try!(write!(f, " fun_{} ", &fd.name.id));
            closers += 1;
        }
        try!(write!(f, "PEnd"));
        for _ in 0 .. closers {
            try!(write!(f, ")"));
        }
        try!(writeln!(f, "."));
        try!(writeln!(f, ""));

        try!(writeln!(f, "Theorem program_typed : prog_type empty_env the_program."));
        try!(writeln!(f, "Proof. (* Consider this proof as a template. *)"));
        for vd in &self.0.var_decls {
            try!(writeln!(f, "  solve_var type_var_{}.", &vd.name.id));
        }
        for fd in &self.0.fun_decls {
            try!(writeln!(f, "  solve_fun type_fun_{}.", &fd.name.id));
        }
        write!(f, "Qed.")
    }
}

fn disp_list<T: fmt::Display, F>(l: &[T], disp_x: F, f: &mut fmt::Formatter) -> fmt::Result
    where F: Fn(&T, &mut fmt::Formatter) -> fmt::Result
{
    try!(write!(f, "["));
    let mut iter = l.iter();
    if let Some(x) = iter.next() {
        try!(disp_x(x, f));
        for x in iter {
            try!(write!(f, ", "));
            try!(disp_x(x, f));
        }
    }
    write!(f, "]")
}

fn disp_vars_then_stmts(vds: &[VarDecl], stmts: &[Stmt], f: &mut fmt::Formatter) -> fmt::Result {
    if vds.len() > 0 {
        try!(write!(f, "(SVal \"{}\" ", &vds[0].name.id));
        try!(disp_expr(&vds[0].expr, f));
        try!(write!(f, " "));
        try!(disp_vars_then_stmts(&vds[1 ..], stmts, f));
        write!(f, ")")
    } else {
        disp_stmts(stmts, f)
    }
}

fn disp_stmts(stmts: &[Stmt], f: &mut fmt::Formatter) -> fmt::Result {
    if stmts.len() > 0 {
        try!(write!(f, "(SSeq "));
        try!(disp_stmt(&stmts[0], f));
        try!(write!(f, " "));
        try!(disp_stmts(&stmts[1 ..], f));
        write!(f, ")")
    } else {
        write!(f, "SSkip")
    }
}

fn disp_stmt(stmt: &Stmt, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::StmtEnum::*;
    match stmt.stmt_enum {
        If { ref cond, ref true_branch, ref false_branch } => {
            try!(write!(f, "(SIf "));
            try!(disp_expr(cond, f));
            try!(write!(f, " "));
            try!(disp_stmts(true_branch, f));
            try!(write!(f, " "));
            try!(disp_stmts(false_branch, f));
            write!(f, ")")
        }
        While { ref cond, ref stmts } => {
            try!(write!(f, "(SWhile "));
            try!(disp_expr(cond, f));
            try!(write!(f, " "));
            try!(disp_stmts(stmts, f));
            write!(f, ")")
        }
        Assignment { ref fieldsel, ref expr } => {
            try!(write!(f, "(SAssign "));
            try!(disp_fieldsel(fieldsel, f));
            try!(write!(f, " "));
            try!(disp_expr(expr, f));
            write!(f, ")")
        }
        FunCall(ref fc) => {
            try!(write!(f, "(SFunCall \"{}\" ", &fc.name.id));
            try!(disp_list(&fc.args, disp_expr, f));
            write!(f, ")")
        }
        Return(None) => write!(f, "(SReturn None)"),
        Return(Some(ref e)) => {
            try!(write!(f, "(SReturn (Some "));
            try!(disp_expr(e, f));
            write!(f, "))")
        }
    }
}

fn disp_fieldsel(fs: &FieldSel, f: &mut fmt::Formatter) -> fmt::Result {
    disp_fields(&fs.fields, &fs.var.id, f)
}

fn disp_fields(fields: &[Field], name: &str, f: &mut fmt::Formatter) -> fmt::Result {
    let len = fields.len();
    if len > 0 {
        try!(write!(f, "("));
        match fields[len - 1] {
            Field::Hd => try!(write!(f, "FHd ")),
            Field::Tl => try!(write!(f, "FTl ")),
            Field::Fst => try!(write!(f, "FFst ")),
            Field::Snd => try!(write!(f, "FSnd ")),
        }
        try!(disp_fields(&fields[.. len - 1], name, f));
        write!(f, ")")
    } else {
        write!(f, "(FVar \"{}\")", name)
    }
}

fn disp_expr(e: &Expr, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::ExprEnum::*;
    match e.expr_enum {
        FieldSel(ref fs) => {
            try!(write!(f, "(EFieldSel "));
            try!(disp_fieldsel(fs, f));
            write!(f, ")")
        }
        IntLit(n) => write!(f, "(ENum {})", n),
        CharLit(c) => write!(f, "(EChar {})", c as u32),
        FunCall(ref fc) => {
            try!(write!(f, "(EFunCall \"{}\" ", &fc.name.id));
            try!(disp_list(&fc.args, disp_expr, f));
            write!(f, ")")
        }
        BinExpr { ref arg1, ref arg2, op, .. } => {
            try!(write!(f, "(EBinOp "));
            try!(disp_binop(op, f));
            try!(write!(f, " "));
            try!(disp_expr(arg1, f));
            try!(write!(f, " "));
            try!(disp_expr(arg2, f));
            write!(f, ")")
        }
        UnExpr { ref arg, op } => {
            try!(write!(f, "(EUnOp "));
            try!(disp_unop(op, f));
            try!(write!(f, " "));
            try!(disp_expr(arg, f));
            write!(f, ")")
        }
        ListNil => write!(f, "ENil"),
        True => write!(f, "ETrue"),
        False => write!(f, "EFalse"),
        Tuple(ref e1, ref e2) => {
            try!(write!(f, "(ETuple "));
            try!(disp_expr(e1, f));
            try!(write!(f, " "));
            try!(disp_expr(e2, f));
            write!(f, ")")
        }
    }
}

fn disp_unop(op: UnOp, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::UnOp::*;
    match op {
        Neg => write!(f, "Neg"),
        Opp => write!(f, "Opp"),
    }
}

fn disp_binop(op: BinOp, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::BinOp::*;
    match op {
        Or => write!(f, "Or"),
        And => write!(f, "And"),
        Eq => write!(f, "Eq"),
        LT => write!(f, "LT"),
        GT => write!(f, "GT"),
        LEq => write!(f, "LEq"),
        GEq => write!(f, "GEq"),
        NEq => write!(f, "NEq"),
        Cons => write!(f, "Cons"),
        Add => write!(f, "Add"),
        Subtract => write!(f, "Sub"),
        Mult => write!(f, "Mul"),
        Divide => write!(f, "Div"),
        Remainder => write!(f, "Rem"),
    }
}

fn disp_type(ty: &Type, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::TypeEnum::*;
    match ty.type_enum {
        Basic(bty) => disp_basic_type(bty, f),
        Tuple(ref ty1, ref ty2) => {
            try!(write!(f, "(TTuple "));
            try!(disp_type(ty1, f));
            try!(write!(f, " "));
            try!(disp_type(ty2, f));
            write!(f, ")")
        }
        List(ref ty) => {
            try!(write!(f, "(TList "));
            try!(disp_type(ty, f));
            write!(f, ")")
        }
        Parm(ref name) =>
            write!(f, "(TVar \"{}\")", &name.id),
    }
}

fn disp_basic_type(bty: BasicType, f: &mut fmt::Formatter) -> fmt::Result {
    let name = match bty {
        BasicType::Int => "TInt",
        BasicType::Bool => "TBool",
        BasicType::Char => "TChar",
    };
    write!(f, "{}", name)
}

fn disp_fun_type(fty: &FunType, f: &mut fmt::Formatter) -> fmt::Result {
    try!(write!(f, "(TFun "));
    try!(disp_list(&fty.arg_types, disp_type, f));
    try!(write!(f, " "));
    match fty.ret_type {
        None => try!(write!(f, "TVoid")),
        Some(ref ty) => try!(disp_type(ty, f)),
    }
    write!(f, ")")
}
