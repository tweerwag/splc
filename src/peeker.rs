//! Implements a peeking structure for an `Iterator`. It also
//! remembers the position of the iterator.
//!
//! # Examples
//!
//! It is assumed that the iterator also implements `GetPos`. This
//! isn't actually the case for `Vec`!
//!
//! ```ignore
//! use splc::peeker::Peeker;
//! let mut p = Peeker::new(vec![1, 2, 3]);
//! assert_eq!(p.next(), Some(1));
//! assert_eq!(p.peek(1), Some(&3));
//! assert_eq!(p.next(), Some(2));
//! assert_eq!(p.peek(0), Some(&3));
//! assert_eq!(p.peek(1), None);
//! assert_eq!(p.next(), Some(3));
//! assert_eq!(p.next(), None);
//! ```

use std::collections::VecDeque;
use posmap::{GetPos, Pos};

pub struct Peeker<I> where I: Iterator + GetPos {
    buf: VecDeque<I::Item>,
    posbuf: VecDeque<Pos>,
    iter: I,
}

impl<I> Peeker<I> where I: Iterator + GetPos {
    /// Construct a new `Peeker` from an existing `Iterator`.
    pub fn new<II>(iter: II) -> Peeker<I>
        where II: IntoIterator<Item=I::Item,IntoIter=I>
    {
        Peeker {
            buf: VecDeque::new(),
            posbuf: VecDeque::new(),
            iter: iter.into_iter(),
        }
    }

    /// Peek `n` items ahead. Note that `n = 0` means peeking at the
    /// first item. To avoid consuming, the item is returned as a ref.
    pub fn peek(&mut self, n: usize) -> Option<&I::Item> {
        // We need to possibly fill the buffer a bit first
        for _ in self.buf.len() .. (n + 1) {
            self.posbuf.push_back(self.iter.pos());
            self.iter.next().map(|it| self.buf.push_back(it));
        }

        // VecDeque::get already yields an Option<&T>
        self.buf.get(n)
    }
}

impl<I> Iterator for Peeker<I> where I: Iterator + GetPos {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        self.posbuf.pop_front();
        self.buf.pop_front()
            .or_else(|| self.iter.next())
    }
}

impl<I> GetPos for Peeker<I> where I: Iterator + GetPos {
    fn pos(&self) -> Pos {
        self.posbuf.front().map(|x| *x)
            .unwrap_or_else(|| self.iter.pos())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use posmap::{GetPos, Pos};
    use std::vec;

    // Implement a dummy GetPos to test the basic peek functionality
    impl<T> GetPos for vec::IntoIter<T> {
        fn pos(&self) -> Pos {
            Pos { begin: 0, end: 0 }
        }
    }

    #[test]
    fn test_peeker_next() {
        let p = Peeker::new(vec![1, 2, 3]);
        assert_eq!(p.collect::<Vec<_>>().as_slice(), &[1, 2, 3]);
    }

    #[test]
    fn test_peeker_peek() {
        let mut p = Peeker::new(vec![1, 2, 3]);
        assert_eq!(p.peek(0), Some(&1));
        assert_eq!(p.next(), Some(1));
        assert_eq!(p.peek(1), Some(&3));
        assert_eq!(p.peek(1), Some(&3));
        assert_eq!(p.peek(0), Some(&2));
    }

    #[test]
    fn test_peeker_peek_far() {
        let mut p = Peeker::new(vec![1, 2, 3]);
        assert_eq!(p.peek(3), None);
        assert_eq!(p.peek(2), Some(&3));
    }

    #[test]
    fn test_peeker_peek_empty() {
        let mut p = Peeker::new(vec![1]);
        assert_eq!(p.peek(0), Some(&1));
        assert_eq!(p.next(), Some(1));
        assert_eq!(p.peek(0), None);
    }

    #[test]
    fn test_peeker_pos() {
        struct StrStack<'a>(Vec<&'a str>, u32);
        impl<'a> Iterator for StrStack<'a> {
            type Item = &'a str;
            fn next(&mut self) -> Option<&'a str> {
                let top = self.0.pop();
                if let Some(s) = top {
                    self.1 += s.len() as u32;
                }
                top
            }
        }
        impl<'a> GetPos for StrStack<'a> {
            fn pos(&self) -> Pos {
                Pos { begin: self.1, end: self.1 }
            }
        }
        let mut p = Peeker::new(StrStack(
            vec![
                "doei",
                "hoi",
                "derde",
                "tweede",
                "bovenop",
            ], 0));
        assert_eq!(p.pos(), Pos { begin: 0, end: 0 } );
        p.next();
        assert_eq!(p.pos(), Pos { begin: 7, end: 7 } );
        p.peek(2);
        assert_eq!(p.pos(), Pos { begin: 7, end: 7 } );
        p.next();
        assert_eq!(p.pos(), Pos { begin: 13, end: 13 } );
        p.next(); p.next(); p.next();
        assert_eq!(p.pos(), Pos { begin: 25, end: 25 } );
    }
}
