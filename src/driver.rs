//! Some common functions for the two driver binaries (splc, splpp).

use std::io;
use std::io::prelude::*;
use std::process::exit;
use std::fs::File;
use std::env;
use getopts::Options;
use lexer::Lexer;
use parser::Parser;
use parser::ParseError::{LexErr, ParseErr, SoftErr};
use posmap::{PosMap, ErrorKind};
use ast::Program;
use namecheck::{check_names, check_main};
use typecheck::infer_types;
use freetyvar::free_type_variables;
use ir;
use spl2coq::DisplayAsCoq;

const STATUS_WRONGARG: i32 = 1;
const STATUS_COMPILEFAIL: i32 = 101;
const STATUS_BUG: i32 = 102;

/// Prints an error and exits with the given status.
macro_rules! error_exit {
    ( $stat:expr , $msg:expr , $( $rest:tt )* ) => {{
        use std::io::stderr;
        let mut stderr = stderr();
        writeln!(&mut stderr, $msg , $( $rest )*).unwrap();
        exit($stat);
    }};
    ( $stat:expr , $msg:expr ) => {{
        use std::io::stderr;
        let mut stderr = stderr();
        writeln!(&mut stderr, $msg ).unwrap();
        exit($stat);
    }};
}

/// Read input and check for malformed UTF-8.
fn read_inp(inp: &mut Read) -> String {
    let mut ret = String::new();
    match inp.read_to_string(&mut ret) {
        Ok(_) => ret,
        Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not read input: {}", e),
    }
}

/// Parse text into an abstract syntax tree, and handle any parse and
/// lexer errors.
fn parse_program(pm: &mut PosMap) -> Program {
    let prog = {
        // Create the lexer
        let lex = Lexer::new(pm.mapper_iter());

        // Create the parser
        let mut parser = Parser::new(lex);

        // Parse the program
        parser.parse_program()
    }; // Destruct the lexer to free up the PosMap

    match prog {
        Ok(prog) => prog,
        Err(LexErr(e)) => {
            error_exit!(STATUS_COMPILEFAIL, "{}", e);
        }
        Err(ParseErr(e, pos)) => {
            error_exit!(STATUS_COMPILEFAIL, "{}", pm.format_error(pos, &e, ErrorKind::Error));
        }
        Err(SoftErr) => {
            error_exit!(STATUS_BUG, "Parser yielded SoftErr");
        }
    }
}

/// Indicates the activity the driver needs to do.
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ToolKind {
    Compiler,
    PrettyPrinter,
    SPL2Coq,
}
pub use self::ToolKind::*;

enum SpecificConfig {
    Comp {
        emit_ir: bool,
    },
    PP {
        typecheck: bool,
    },
    SPL2Coq {
        typecheck: bool,
    },
}

struct Config {
    input: Box<Read>,
    output: Box<Write>,
    sc: SpecificConfig,
}

fn create_options(tk: ToolKind) -> Options {
    let mut opts = Options::new();
    opts.optopt("o", "", "set output file name", "NAME");
    opts.optflag("h", "help", "print this help menu");

    match tk {
        PrettyPrinter => {
            opts.optflag("t", "typecheck", "typecheck input and infer omitted types");
        }
        Compiler => {
            opts.optflag("i", "emit-ir", "emit IR code");
        }
        SPL2Coq => {
            opts.optflag("t", "typecheck", "typecheck input and infer omitted types");
        }
    }

    opts
}

/// Prints a nice usage message to `stderr` and exits with status 1.
fn print_usage(cmd: &str, opts: Options) -> ! {
    let brief = format!("Usage: {} [IN-FILE] [options]", cmd);
    error_exit!(STATUS_WRONGARG, "{}", opts.usage(&brief));
}

/// Parses the cmdline arguments.
fn parse_args(tk: ToolKind) -> Config {
    let args: Vec<String> = env::args().collect();
    let cmd = args[0].clone();
    let opts = create_options(tk);

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => error_exit!(STATUS_WRONGARG, "{}", e),
    };

    if matches.opt_present("h") {
        print_usage(&cmd, opts);
    }

    let input: Box<Read> = if matches.free.is_empty() {
        Box::new(io::stdin())
    } else if matches.free.len() == 1 {
        let arg = &matches.free[0];
        match File::open(arg) {
            Ok(f) => Box::new(f),
            Err(e) => {
                error_exit!(STATUS_COMPILEFAIL, "Could not open file {}: {}", arg, e);
            }
        }
    } else {
        print_usage(&cmd, opts);
    };

    let output: Box<Write> = match matches.opt_str("o") {
        Some(name) => {
            match File::create(&name) {
                Ok(f) => Box::new(f),
                Err(e) => {
                    error_exit!(STATUS_COMPILEFAIL, "Could not open file {}: {}", &name, e);
                }
            }
        }
        None => Box::new(io::stdout()),
    };

    let sc = match tk {
        Compiler => SpecificConfig::Comp {
            emit_ir: matches.opt_present("i"),
        },
        PrettyPrinter => SpecificConfig::PP {
            typecheck: matches.opt_present("t"),
        },
        SPL2Coq => SpecificConfig::SPL2Coq {
            typecheck: matches.opt_present("t"),
        },
    };

    Config {
        input: input,
        output: output,
        sc: sc,
    }
}

/// Main driver of the tools.
pub fn main_driver(tk: ToolKind) {
    let mut conf = parse_args(tk);
    match conf.sc {
        SpecificConfig::Comp { .. } => assert_eq!(tk, Compiler),
        SpecificConfig::PP { .. } => assert_eq!(tk, PrettyPrinter),
        SpecificConfig::SPL2Coq { .. } => assert_eq!(tk, SPL2Coq),
    }

    let inp_str = read_inp(&mut conf.input);

    // To print nicer errors we remember where all lines start
    let mut posmap = PosMap::new(&inp_str);

    let mut prog = parse_program(&mut posmap);

    if let SpecificConfig::PP { typecheck, .. } = conf.sc {
        if !typecheck {
            match writeln!(conf.output, "{}", &prog) {
                Ok(_) => return,
                Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
            }
        }
    }

    if let SpecificConfig::SPL2Coq { typecheck, .. } = conf.sc {
        if !typecheck {
            match writeln!(conf.output, "{}", DisplayAsCoq(&prog)) {
                Ok(_) => return,
                Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
            }
        }
    }

    if check_names(&prog, &posmap) { exit(STATUS_COMPILEFAIL); }

    if infer_types(&mut prog, &posmap) { exit(STATUS_COMPILEFAIL); }

    if check_main(&prog, tk == Compiler, &posmap) { exit(STATUS_COMPILEFAIL); }

    match tk {
        PrettyPrinter => {
            free_type_variables(&prog, false, &posmap);
        }
        Compiler => {
            if free_type_variables(&prog, true, &posmap) {
                exit(STATUS_COMPILEFAIL);
            }
        }
        SPL2Coq => {
            free_type_variables(&prog, false, &posmap);
        }
    }

    if tk == PrettyPrinter {
        match writeln!(conf.output, "{}", &prog) {
            Ok(_) => return,
            Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
        }
    } else if tk == SPL2Coq {
        match writeln!(conf.output, "{}", DisplayAsCoq(&prog)) {
            Ok(_) => return,
            Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
        }
    }

    assert_eq!(tk, Compiler);

    let ir = ir::from_ast::program(&prog);

    if let SpecificConfig::Comp { emit_ir, .. } = conf.sc {
        if emit_ir {
            match writeln!(conf.output, "{}", &ir) {
                Ok(_) => return,
                Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
            }
        }
    }

    match writeln!(conf.output, "{}", ir::to_asm::program(&ir)) {
        Ok(_) => return,
        Err(e) => error_exit!(STATUS_COMPILEFAIL, "Could not write output: {}", e),
    }
}
