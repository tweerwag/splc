//! A minimal implementation of `Either`, the general sum type.

pub use either::Either::{Left, Right};

pub enum Either<T, U> {
    Left(T),
    Right(U),
}
