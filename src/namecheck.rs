//! Small compiler pass which checks for conflicting identifiers.

use std::io;
use std::io::prelude::*;
use std::collections::HashMap;
use ast::*;
use posmap::{Pos, PosMap, ErrorKind};

fn print_clash_error(pos1: Pos, pos2: Pos, pm: &PosMap) {
    let mut stderr = io::stderr();
    writeln!(stderr, "{}\n{}",
             pm.format_error(pos2, "Identifier defined here:", ErrorKind::Error),
             pm.format_error(pos1, "Is first defined here:", ErrorKind::None)
    ).unwrap();
}

fn print_clash_warning(pos1: Pos, pos2: Pos, pm: &PosMap) {
    let mut stderr = io::stderr();
    writeln!(stderr, "{}\n{}",
             pm.format_error(pos2, "Identifier defined here:", ErrorKind::Warning),
             pm.format_error(pos1, "Shadows identifier defined here:", ErrorKind::None)
    ).unwrap();
}

/// Check for a main function and check its type. Prints a warning or
/// error message depending on `is_error` if no main function is
/// found. Prints a warning when the type is non-standard (not `:: ->
/// Void`). Returns `true` on error, i.e. if no main is found or it
/// has non-standard type.
///
/// Run this function after types are inferred!
pub fn check_main(prog: &Program, is_error: bool, pm: &PosMap) -> bool {
    let mut stderr = io::stderr();
    match prog.fun_decls.iter().find(|fd| fd.name.id == "main") {
        None => {
            let kind = if is_error { ErrorKind::Error } else { ErrorKind::Warning };
            writeln!(stderr, "{}",
                     pm.format_error(Pos::default(), "No main function found.", kind)
            ).unwrap();
            if is_error {
                return true;
            }
        }
        Some(fd) => {
            let the_type = fd.the_type.as_ref().expect("type of main not yet inferred");
            if the_type.arg_types != vec![] || the_type.ret_type != None {
                writeln!(stderr, "{}",
                         pm.format_error(fd.pos, "Main function has non-standard type.", ErrorKind::Warning)
                ).unwrap();
            }
        }
    }
    false
}

/// Check whether globel names clash with each other, and executes
/// `check_local_names` on each function. Returns true if an error
/// occurred.
pub fn check_names(prog: &Program, pm: &PosMap) -> bool {
    let mut names = HashMap::new();
    let empty_names = HashMap::new(); // used as local_names arg for ckexpr
    let mut have_errors = false;

    names.insert(Qualid::fun("print"), Pos::default());
    names.insert(Qualid::fun("isEmpty"), Pos::default());

    for vdecl in &prog.var_decls {
        have_errors |= ckexpr(&vdecl.expr, &names, &empty_names, pm);
        let qid = Qualid::var(&vdecl.name.id);
        match names.get(&qid) {
            Some(&pos1) => {
                let pos2 = vdecl.name.pos;
                print_clash_error(pos1, pos2, pm);
                have_errors = true;
            }
            None => {
                names.insert(qid, vdecl.name.pos);
            }
        }
    }

    for fdecl in &prog.fun_decls {
        let qid = Qualid::fun(&fdecl.name.id);
        match names.get(&qid) {
            Some(&pos1) => {
                let pos2 = fdecl.name.pos;
                print_clash_error(pos1, pos2, pm);
                have_errors = true;
            }
            None => {
                names.insert(qid, fdecl.name.pos);
            }
        }
    }

    for fdecl in &prog.fun_decls {
        have_errors |= check_local_names(fdecl, &names, pm);
    }

    have_errors
}

/// Check whether local names clash or shadow global names. Returns
/// true if an error occurred.
fn check_local_names(fdecl: &FunDecl, global_names: &HashMap<Qualid, Pos>, pm: &PosMap) -> bool {
    let mut names = HashMap::new();
    let mut have_errors = false;

    for arg in &fdecl.arg_names {
        let qid = Qualid::var(&arg.id);
        let qid_pos = arg.pos;

        if let Some(&pos1) = global_names.get(&qid) {
            print_clash_warning(pos1, qid_pos, pm);
        }

        match names.get(&qid) {
            Some(&pos1) => {
                print_clash_error(pos1, qid_pos, pm);
                have_errors = true;
            }
            None => {
                names.insert(qid, qid_pos);
            }
        }
    }

    for vdecl in &fdecl.var_decls {
        let qid = Qualid::var(&vdecl.name.id);
        let qid_pos = vdecl.name.pos;

        if let Some(&pos1) = global_names.get(&qid) {
            print_clash_warning(pos1, qid_pos, pm);
        }

        match names.get(&qid) {
            Some(&pos1) => {
                print_clash_error(pos1, qid_pos, pm);
                have_errors = true;
            }
            None => {
                names.insert(qid, qid_pos);
            }
        }
    }

    for stmt in &fdecl.stmts {
        have_errors |= ckstmt(stmt, global_names, &names, pm);
    }

    have_errors
}

/// Check whether the tables contain the given identifier, otherwise
/// print error message to stderr and return true.
fn ckident(ident: &Ident,
           ns: IdentNamespace,
           global_names: &HashMap<Qualid, Pos>,
           local_names: &HashMap<Qualid, Pos>,
           pm: &PosMap) -> bool {
    let qid = Qualid::new(&ident.id, ns);
    let qid_pos = ident.pos;
    if !global_names.contains_key(&qid) && !local_names.contains_key(&qid) {
        let mut stderr = io::stderr();
        writeln!(stderr, "{}",
                 pm.format_error(qid_pos, "Unknown identifier", ErrorKind::Error)
        ).unwrap();
        true
    } else {
        false
    }
}

/// Check whether all identifiers in the given statement are defined.
fn ckstmt(stmt: &Stmt,
          global_names: &HashMap<Qualid, Pos>,
          local_names: &HashMap<Qualid, Pos>,
          pm: &PosMap) -> bool {
    use ast::StmtEnum::*;
    match stmt.stmt_enum {
        If { ref cond, ref true_branch, ref false_branch } => {
            let mut ret = false;
            ret |= ckexpr(cond, global_names, local_names, pm);
            for stmt in true_branch {
                ret |= ckstmt(stmt, global_names, local_names, pm);
            }
            for stmt in false_branch {
                ret |= ckstmt(stmt, global_names, local_names, pm);
            }
            ret
        }
        While { ref cond, ref stmts } => {
            let mut ret = false;
            ret |= ckexpr(cond, global_names, local_names, pm);
            for stmt in stmts {
                ret |= ckstmt(stmt, global_names, local_names, pm);
            }
            ret
        }
        Assignment { ref fieldsel, ref expr } => {
            let mut ret = false;
            ret |= ckident(&fieldsel.var, IdentNamespace::Variable, global_names, local_names, pm);
            ret |= ckexpr(expr, global_names, local_names, pm);
            ret
        }
        FunCall(ref fc) => {
            let mut ret = false;
            ret |= ckident(&fc.name, IdentNamespace::Function, global_names, local_names, pm);
            for expr in &fc.args {
                ret |= ckexpr(expr, global_names, local_names, pm);
            }
            ret
        }
        Return(Some(ref expr)) => ckexpr(expr, global_names, local_names, pm),
        Return(None) => false,
    }
}

/// Check whether the all identifiers in the given expression are
/// defined.
fn ckexpr(expr: &Expr,
          global_names: &HashMap<Qualid, Pos>,
          local_names: &HashMap<Qualid, Pos>,
          pm: &PosMap) -> bool {
    use ast::ExprEnum::*;
    match expr.expr_enum {
        FieldSel(ref fs) => ckident(&fs.var, IdentNamespace::Variable, global_names, local_names, pm),
        FunCall(ref fc) => {
            let mut ret = false;
            ret |= ckident(&fc.name, IdentNamespace::Function, global_names, local_names, pm);
            for expr in &fc.args {
                ret |= ckexpr(expr, global_names, local_names, pm);
            }
            ret
        }
        BinExpr { ref arg1, ref arg2, .. } => {
            let mut ret = false;
            ret |= ckexpr(arg1, global_names, local_names, pm);
            ret |= ckexpr(arg2, global_names, local_names, pm);
            ret
        }
        UnExpr { ref arg, .. } => ckexpr(arg, global_names, local_names, pm),
        Tuple(ref expr1, ref expr2) => {
            let mut ret = false;
            ret |= ckexpr(expr1, global_names, local_names, pm);
            ret |= ckexpr(expr2, global_names, local_names, pm);
            ret
        }
        IntLit(_) | CharLit(_) | ListNil | True | False => false,
    }
}
