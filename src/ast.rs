//! Defines the abstract syntax tree.

use posmap::Pos;
use std::cell::RefCell;

#[derive(Debug, PartialEq, Clone)]
pub struct Ident {
    pub id: String,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Program {
    pub var_decls: Vec<VarDecl>,
    pub fun_decls: Vec<FunDecl>,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub struct VarDecl {
    pub name: Ident,
    pub the_type: Option<Type>,
    pub expr: Expr,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FunDecl {
    pub name: Ident,
    pub arg_names: Vec<Ident>,
    pub the_type: Option<FunType>,
    pub var_decls: Vec<VarDecl>,
    pub stmts: Vec<Stmt>,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub struct FunType {
    pub arg_types: Vec<Type>,
    pub ret_type: Option<Type>,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Stmt {
    pub stmt_enum: StmtEnum,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub enum StmtEnum {
    If { cond: Expr, true_branch: Vec<Stmt>, false_branch: Vec<Stmt>, },
    While { cond: Expr, stmts: Vec<Stmt>, },
    Assignment { fieldsel: FieldSel, expr: Expr, },
    FunCall(FunCall),
    Return(Option<Expr>),
}

#[derive(Debug, PartialEq, Clone)]
pub struct FieldSel {
    pub var: Ident,
    pub fields: Vec<Field>,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Field {
    Hd, Tl, Fst, Snd,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Expr {
    pub expr_enum: ExprEnum,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub enum ExprEnum {
    FieldSel(FieldSel),
    IntLit(u32),
    CharLit(char),
    FunCall(FunCall),
    BinExpr { arg1: Box<Expr>, arg2: Box<Expr>, op: BinOp, overloaded: RefCell<Option<String>> },
    UnExpr { arg: Box<Expr>, op: UnOp },
    ListNil,
    True,
    False,
    Tuple(Box<Expr>, Box<Expr>),
}

#[derive(Debug, PartialEq, Clone)]
pub struct FunCall {
    pub name: Ident,
    pub args: Vec<Expr>,
    pub pos: Pos,
    /// Ugly hack to mutate this cell without a mut ref. (`RefCell`
    /// borrowing is checked on runtime.)
    pub overloaded: RefCell<Option<String>>,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum BinOp {
    Or, And,
    Eq, LT, GT, LEq, GEq, NEq,
    Cons,
    Add, Subtract,
    Mult, Divide, Remainder,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum UnOp {
    Neg, Opp,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Type {
    pub type_enum: TypeEnum,
    pub pos: Pos,
}

#[derive(Debug, PartialEq, Clone)]
pub enum TypeEnum {
    Basic(BasicType),
    Tuple(Box<Type>, Box<Type>),
    List(Box<Type>),
    Parm(Ident),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum BasicType {
    Int, Bool, Char,
}


#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum IdentNamespace {
    Function,
    Variable,
}

/// A qualified identifier. Currently there are two separate
/// namespaces: functions and variables.  Note that these `Qualid`s
/// don't actually appear in the AST.
///
/// Maybe this file is not the best place to define them, but on the
/// other hand, creating a file for 10 lines of code is probably
/// worse.
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Qualid<'a> {
    name: &'a str,
    ns: IdentNamespace,
}

impl<'a> Qualid<'a> {
    // Note the lifetime elision here, actually reads:
    // fn new(name: &'a str, ns: _) -> Qualid<'a>
    pub fn new(name: &str, ns: IdentNamespace) -> Qualid {
        Qualid { name: name, ns: ns }
    }

    pub fn fun(name: &str) -> Qualid {
        Qualid { name: name, ns: IdentNamespace::Function }
    }

    pub fn var(name: &str) -> Qualid {
        Qualid { name: name, ns: IdentNamespace::Variable }
    }

    pub fn is_fun(&self) -> bool {
        match self.ns {
            IdentNamespace::Function => true,
            IdentNamespace::Variable => false,
        }
    }

    pub fn unwrap_fun(&self) -> &str {
        match self.ns {
            IdentNamespace::Function => self.name,
            IdentNamespace::Variable => panic!("{:?} is not a function identifier", self),
        }
    }
}

#[cfg(test)]
pub mod test_util {
    use super::*;
    use posmap::Pos;

    pub fn tuple(expr1: Expr, expr2: Expr) -> Expr {
        Expr {
            expr_enum: ExprEnum::Tuple(Box::new(expr1), Box::new(expr2)),
            pos: Pos::default(),
        }
    }

    pub fn list_nil() -> Expr {
        Expr {
            expr_enum: ExprEnum::ListNil,
            pos: Pos::default(),
        }
    }

    pub fn tru() -> Expr {
        Expr {
            expr_enum: ExprEnum::True,
            pos: Pos::default(),
        }
    }

    pub fn int(x: i64) -> Expr {
        Expr {
            expr_enum: ExprEnum::IntLit(x),
            pos: Pos::default(),
        }
    }

    pub fn neg(e: Expr) -> Expr {
        Expr {
            expr_enum: ExprEnum::UnExpr {
                arg: Box::new(e),
                op: UnOp::Neg,
            },
            pos: Pos::default(),
        }
    }

     pub fn opp(e: Expr) -> Expr {
        Expr {
            expr_enum: ExprEnum::UnExpr {
                arg: Box::new(e),
                op: UnOp::Opp,
            },
            pos: Pos::default(),
        }
    }

    pub fn cons(e1: Expr, e2: Expr) -> Expr {
        Expr {
            expr_enum: ExprEnum::BinExpr {
                arg1: Box::new(e1),
                arg2: Box::new(e2),
                op: BinOp::Cons,
            },
            pos: Pos::default(),
        }
    }

    pub fn add(e1: Expr, e2: Expr) -> Expr {
        Expr {
            expr_enum: ExprEnum::BinExpr {
                arg1: Box::new(e1),
                arg2: Box::new(e2),
                op: BinOp::Add,
            },
            pos: Pos::default(),
        }
    }

    pub fn fieldsel(id: &str, fields: &[Field]) -> Expr {
        Expr {
            expr_enum: ExprEnum::FieldSel(
                FieldSel {
                    var: ident(id),
                    fields: fields.iter().cloned().collect(),
                    pos: Pos::default(),
                }),
            pos: Pos::default(),
        }
    }

    pub fn ident(id: &str) -> Ident {
        Ident {
            id: id.to_owned(),
            pos: Pos::default(),
        }
    }
}
