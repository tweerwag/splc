extern crate getopts;
extern crate petgraph;

mod ast;
mod callgraph;
pub mod driver;
mod either;
mod freetyvar;
mod ir;
mod lexer;
mod namecheck;
mod parser;
mod peeker;
mod posmap;
mod prettyprint;
mod putback;
mod spl2coq;
mod typecheck;
