//! Walk the AST and look for any free type variables.

use std::io;
use std::io::prelude::*;
use ast::*;
use posmap::{Pos, PosMap, ErrorKind};

/// Prints a message about any free type variables found in any type
/// of `prog`. Depending on `is_error` this will be presented as a
/// warning or an error. Returns `true` if free type variables are
/// found.
pub fn free_type_variables(prog: &Program, is_error: bool, pm: &PosMap) -> bool {
    let mut have_errors = false;

    for vd in &prog.var_decls {
        have_errors |= vardecl(vd, is_error, pm);
    }

    for fd in &prog.fun_decls {
        have_errors |= fundecl(fd, is_error, pm);
    }

    have_errors
}

fn print_message(pos: Pos, is_error: bool, pm: &PosMap) {
    let mut stderr = io::stderr();
    let kind = if is_error { ErrorKind::Error} else { ErrorKind::Warning };
    let msg = "Could not generalize free type variable here:";
    writeln!(stderr, "{}", pm.format_error(pos, msg, kind)).unwrap();
}

fn vardecl(vd: &VarDecl, is_error: bool, pm: &PosMap) -> bool {
    if let Some(ref ty) = vd.the_type {
        ast_type(ty, is_error, pm)
    } else {
        false
    }
}

fn fundecl(fd: &FunDecl, is_error: bool, pm: &PosMap) -> bool {
    let mut have_errs = false;
    if let Some(ref funty) = fd.the_type {
        have_errs |= ast_funtype(funty, is_error, pm);
    }
    for vd in &fd.var_decls {
        have_errs |= vardecl(vd, is_error, pm);
    }
    have_errs
}

fn ast_funtype(funty: &FunType, is_error: bool, pm: &PosMap) -> bool {
    let mut have_errs = false;
    for argty in &funty.arg_types {
        have_errs |= ast_type(argty, is_error, pm);
    }
    if let Some(ref retty) = funty.ret_type {
        have_errs |= ast_type(retty, is_error, pm);
    }
    have_errs
}

fn ast_type(ty: &Type, is_error: bool, pm: &PosMap) -> bool {
    use ast::TypeEnum::*;
    match ty.type_enum {
        Basic(_) => false,
        Tuple(ref ty1, ref ty2) =>
            ast_type(ty1, is_error, pm) | ast_type(ty2, is_error, pm),
        List(ref ty) => ast_type(ty, is_error, pm),
        Parm(ref p) => parm(p, is_error, pm),
    }
}

fn parm(p: &Ident, is_error: bool, pm: &PosMap) -> bool {
    if p.id.chars().next().unwrap_or(' ') == '_' {
        print_message(p.pos, is_error, pm);
        true
    } else {
        false
    }
}
