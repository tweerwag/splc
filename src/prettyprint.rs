//! Pretty printer for the abstract syntax tree. This is implemented
//! through implementations for `Display` on the AST in a separate
//! file to keep the file with the AST clean.

use std::{fmt, iter};
use ast::*;

/// Formats the contents of a `Vec` with displayable elements as a
/// list with a specifiable separator between them.
struct Joiner<'a, T: 'a + fmt::Display> {
    its: &'a Vec<T>,
    sep: &'a str,
}

impl<'a, T: fmt::Display> Joiner<'a, T> {
    fn new(its: &'a Vec<T>, sep: &'a str) -> Joiner<'a, T> {
        Joiner { its: its, sep: sep }
    }
}

impl<'a, T: fmt::Display> fmt::Display for Joiner<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut iter = self.its.iter();
        if let Some(it) = iter.next() {
            try!(write!(f, "{}", it));
            for it in iter {
                try!(write!(f, "{}{}", self.sep, it));
            }
        }
        Ok(())
    }
}

/// Formats the contents of a `Vec` with displayable elements by
/// putting a specifiable `&str` before and after each element and
/// concatenating them.
struct Repeater<'a, T: 'a + fmt::Display> {
    its: &'a Vec<T>,
    front: &'a str,
    back: &'a str,
}

impl<'a, T: fmt::Display> Repeater<'a, T> {
    fn new(its: &'a Vec<T>, front: &'a str, back: &'a str) -> Repeater<'a, T> {
        Repeater { its: its, front: front, back: back }
    }
}

impl<'a, T: fmt::Display> fmt::Display for Repeater<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for it in self.its.iter() {
            try!(write!(f, "{}{}{}", self.front, it, self.back));
        }
        Ok(())
    }
}

impl fmt::Display for Ident {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.id)
    }
}

impl fmt::Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}", Joiner::new(&self.var_decls, "\n")));
        if !self.var_decls.is_empty() { try!(write!(f, "\n\n")); }
        try!(write!(f, "{}", Joiner::new(&self.fun_decls, "\n\n")));
        Ok(())
    }
}

impl fmt::Display for VarDecl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.the_type {
            None => write!(f, "var {} = {};", self.name, self.expr),
            Some(ref ty) => write!(f, "{} {} = {};", ty, self.name, self.expr),
        }
    }
}

const INDENT_SPACE: &'static str = "    ";

fn write_indent(indent: u32, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", iter::repeat(INDENT_SPACE).take(indent as usize).collect::<String>())
}

impl fmt::Display for FunDecl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ftype = match self.the_type {
            Some(ref fty) => format!(" :: {}", fty),
            None => String::new(),
        };
        try!(write!(f, "{}({}){} {{\n", self.name, Joiner::new(&self.arg_names, ", "), ftype));
        for vd in self.var_decls.iter() {
            try!(write_indent(1, f));
            try!(write!(f, "{}\n", vd));
        }
        for stmt in self.stmts.iter() {
            try!(fmt_stmt(stmt, 1, f));
            try!(write!(f, "\n"));
        }
        try!(write!(f, "}}"));
        Ok(())
    }
}

impl fmt::Display for FunType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ret = match self.ret_type {
            Some(ref ty) => format!("{}", ty),
            None => "Void".to_string(),
        };
        write!(
            f, "{}{}-> {}",
            Joiner::new(&self.arg_types, " "),
            if self.arg_types.is_empty() { "" } else { " " },
            ret)
    }
}

fn fmt_stmt(stmt: &Stmt, indent: u32, f: &mut fmt::Formatter) -> fmt::Result {
    fmt_stmt_enum(&stmt.stmt_enum, indent, f)
}

impl fmt::Display for Stmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt_stmt(self, 0, f)
    }
}

fn fmt_stmt_enum(stmt_enum: &StmtEnum, indent: u32, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::StmtEnum::*;
    try!(write_indent(indent, f));
    match stmt_enum {
        &If { ref cond, ref true_branch, ref false_branch } => {
            try!(write!(f, "if ({}) {{\n", cond));
            for stmt in true_branch.iter() {
                try!(fmt_stmt(stmt, indent + 1, f));
                try!(write!(f, "\n"));
            }
            try!(write_indent(indent, f));
            try!(write!(f, "}}"));
            if !false_branch.is_empty() {
                try!(write!(f, " else {{\n"));
                for stmt in false_branch.iter() {
                    try!(fmt_stmt(stmt, indent + 1, f));
                    try!(write!(f, "\n"));
                }
                try!(write_indent(indent, f));
                try!(write!(f, "}}"));
            };
            Ok(())
        }
        &While { ref cond, ref stmts } => {
            try!(write!(f, "while ({}) {{\n", cond));
            for stmt in stmts.iter() {
                try!(fmt_stmt(stmt, indent + 1, f));
                try!(write!(f, "\n"));
            }
            try!(write_indent(indent, f));
            try!(write!(f, "}}"));
            Ok(())
        }
        &Assignment { ref fieldsel, ref expr } =>
            write!(f, "{} = {};", fieldsel, expr),
        &FunCall(ref fc) =>
            write!(f, "{};", fc),
        &Return(ref maybe_expr) => {
            match maybe_expr {
                &Some(ref expr) => write!(f, "return {};", expr),
                &None => write!(f, "return;"),
            }
        }
    }
}

impl fmt::Display for StmtEnum {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt_stmt_enum(self, 0, f)
    }
}

impl fmt::Display for FieldSel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.var, Repeater::new(&self.fields, ".", ""))
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ast::Field::*;
        let s = match self {
            &Hd => "hd",
            &Tl => "tl",
            &Fst => "fst",
            &Snd => "snd",
        };
        write!(f, "{}", s)
    }
}

fn precedence(op: BinOp) -> i32 {
    use ast::BinOp::*;
    match op {
        Or => 0,
        And => 1,
        Eq | LT | GT | LEq | GEq | NEq => 2,
        Cons => 3,
        Add | Subtract => 4,
        Mult | Divide | Remainder => 5,
    }
}

enum Assoc { Left, Right }

fn assoc(op: BinOp) -> Assoc {
    use ast::BinOp::*;
    match op {
        Or => Assoc::Left,
        And => Assoc::Left,
        Eq | LT | GT | LEq | GEq | NEq => Assoc::Left,
        Cons => Assoc::Right,
        Add | Subtract => Assoc::Left,
        Mult | Divide | Remainder => Assoc::Left,
    }
}

const UNARY_PRECEDENCE: i32 = 6;

#[derive(PartialEq)]
enum WhichBranch { None, Left, Right }

fn fmt_expr(expr: &Expr, cur_lvl: i32, br: WhichBranch, f: &mut fmt::Formatter) -> fmt::Result {
    fmt_expr_enum(&expr.expr_enum, cur_lvl, br, f)
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt_expr(self, -1, WhichBranch::None, f)
    }
}

fn fmt_expr_enum(expr_enum: &ExprEnum, cur_lvl: i32, br: WhichBranch, f: &mut fmt::Formatter) -> fmt::Result {
    use ast::ExprEnum::*;
    match expr_enum {
        &FieldSel(ref fs) => write!(f, "{}", fs),
        &IntLit(x) => write!(f, "{}", x),
        &CharLit(c) => write!(f, "`{}", c),
        &FunCall(ref fc) => write!(f, "{}", fc),
        &BinExpr { ref arg1, ref arg2, op, .. } => {
            let new_lvl = precedence(op);
            let need_ps = {
                use std::cmp::Ordering::*;
                match new_lvl.cmp(&cur_lvl) {
                    Less => true,
                    Equal => {
                        match assoc(op) {
                            Assoc::Left if br == WhichBranch::Left => false,
                            Assoc::Right if br == WhichBranch::Right => false,
                            _ => true,
                        }
                    }
                    Greater => false,
                }
            };
            if need_ps { try!(write!(f, "(")); }
            try!(fmt_expr(arg1, new_lvl, WhichBranch::Left, f));
            try!(write!(f, " {} ", op));
            try!(fmt_expr(arg2, new_lvl, WhichBranch::Right, f));
            if need_ps { try!(write!(f, ")")); }
            Ok(())
        }
        &UnExpr { ref arg, op } => {
            try!(write!(f, "{}", op));
            try!(fmt_expr(arg, UNARY_PRECEDENCE, WhichBranch::None, f));
            Ok(())
        }
        &ListNil => write!(f, "[]"),
        &True => write!(f, "True"),
        &False => write!(f, "False"),
        &Tuple(ref expr1, ref expr2) => write!(f, "({}, {})", expr1, expr2),
    }
}

impl fmt::Display for ExprEnum {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt_expr_enum(self, -1, WhichBranch::None, f)
    }
}

impl fmt::Display for FunCall {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({})", self.name, Joiner::new(&self.args, ", "))
    }
}

impl fmt::Display for BinOp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ast::BinOp::*;
        let s = match self {
            &Or => "||",
            &And => "&&",
            &Eq => "==",
            &LT => "<",
            &GT => ">",
            &LEq => "<=",
            &GEq => ">=",
            &NEq => "!=",
            &Cons => ":",
            &Add => "+",
            &Subtract => "-",
            &Mult => "*",
            &Divide => "/",
            &Remainder => "%",
        };
        write!(f, "{}", s)
    }
}

impl fmt::Display for UnOp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ast::UnOp::*;
        let s = match self {
            &Neg => "!",
            &Opp => "-",
        };
        write!(f, "{}", s)
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.type_enum)
    }
}

impl fmt::Display for TypeEnum {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ast::TypeEnum::*;
        match self {
            &Basic(ref bty) => write!(f, "{}", bty),
            &Tuple(ref ty1, ref ty2) => write!(f, "({}, {})", ty1, ty2),
            &List(ref ty) => write!(f, "[{}]", ty),
            &Parm(ref id) => write!(f, "{}", id),
        }
    }
}

impl fmt::Display for BasicType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ast::BasicType::*;
        let s = match self {
            &Int => "Int",
            &Bool => "Bool",
            &Char => "Char",
        };
        write!(f, "{}", s)
    }
}
