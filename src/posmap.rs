//! Map of character indices in a file to line and column number.
//!
//! This is used to format an error message to something sensical
//! like:
//!
//! ```text
//! foo(x, var);
//!        ~~~
//! 1:7:1:10:Expected identifier
//! ```

use std::ops::Add;
use std::cmp::{min, max};
use std::str::Chars;
use std::fmt::Write;

/// Resembles a position of a token or syntax element in a file. Note
/// that the implementation for `Add` is only for easy notation. The
/// actual operation does not behave like you would expect from
/// addition.
#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Pos {
    pub begin: u32,
    pub end: u32,
}

impl Default for Pos {
    fn default() -> Pos {
        Pos { begin: 0, end: 0 }
    }
}

impl Add for Pos {
    type Output = Pos;

    fn add(self, rhs: Pos) -> Pos {
        Pos {
            begin: min(self.begin, rhs.begin),
            end: max(self.end, rhs.end),
        }
    }
}

/// Used as an interface for the parser to retrieve the current
/// position.
pub trait GetPos {
    fn pos(&self) -> Pos;
}

impl<T, E> GetPos for Result<T, E> where T: GetPos {
    fn pos(&self) -> Pos {
        match self {
            &Ok(ref x) => x.pos(),
            &Err(_) => Pos::default(),
        }
    }
}

pub struct PosMap<'a> {
    text: &'a str,
    lines: Vec<u32>,
}

#[derive(Clone,Copy)]
pub enum ErrorKind {
    None,
    Error,
    Warning,
}

impl ErrorKind {
    // TODO: Find out whether the terminal (if any) supports this
    fn as_str(self) -> &'static str {
        match self {
            ErrorKind::None => "",
            ErrorKind::Error => " \x1b[31merror:\x1b[0m",
            ErrorKind::Warning => " \x1b[33mwarning:\x1b[0m",
        }
    }
}

impl<'a> PosMap<'a> {
    pub fn new(text: &'a str) -> PosMap<'a> {
        PosMap { text: text, lines: Vec::new() }
    }

    pub fn mapper_iter<'b>(&'b mut self) -> MapperIter<'a, 'b> {
        self.lines.clear();
        
        // There is no newline, but there does start a line on the first character ;)
        self.lines.push(0);

        MapperIter { posmap: self, pos: 0, iter: self.text.chars() }
    }

    pub fn line_and_column(&self, pos: u32) -> (u32, u32) {
        let idx = match self.lines.binary_search(&pos) {
            // if the pos is a start of a line, this is the correct index
            Ok(idx) => idx,
            // if the pos is between two lines, binary_search returns the next index
            Err(idx) => idx - 1,
        };
        (idx as u32 + 1, pos - self.lines[idx])
    }

    pub fn format_error(&self, pos: Pos, msg: &str, kind: ErrorKind) -> String {
        let mut res = String::new();
        let (line1, col1) = self.line_and_column(pos.begin);
        let (line2, mut col2) = self.line_and_column(pos.end);
        res.push_str(&format!(
            "{}:{}:{}:{}:{} {}",
            line1, col1,
            line2, col2,
            kind.as_str(), msg));
        if line1 == line2 {
            if col1 == col2 { col2 += 1; }
            let line = self.text
                .lines()
                .nth(line1 as usize - 1)
                .expect("formatting error for non existing line");
            write!(res, "\n{}: ", line1).unwrap();
            res.push_str(line);
            res.push_str("\n  ");
            { let mut l = line1; while l != 0 { res.push(' '); l /= 10; } }
            for _ in 0 .. col1 { res.push(' '); }
            for _ in col1 .. col2 { res.push('~'); }
        } else {
            // TODO: nicer errors for pos's spanning multiple lines
        }
        res
    }
}

pub struct MapperIter<'a: 'b, 'b> {
    posmap: &'b mut PosMap<'a>,
    pos: u32,
    iter: Chars<'a>,
}

impl<'a: 'b, 'b> Iterator for MapperIter<'a, 'b> {
    type Item = char;

    fn next(&mut self) -> Option<char> {
        let maybe_c = self.iter.next();
        if let Some('\n') = maybe_c {
            // new line on the next character
            self.posmap.lines.push(self.pos + 1);
        }
        self.pos += 1;
        maybe_c
    }
}
