//! Iterator whose items can be put back.
//!
//! # Examples
//!
//! ```rust
//! use splc::putback::PutBack;
//! let v = vec![1, 2, 3];
//! let mut pb = PutBack::new(v.into_iter());
//!
//! assert_eq!(pb.next(), Some(1));
//! pb.put_back(4);
//! pb.put_back(5);
//! assert_eq!(pb.next(), Some(5));
//! assert_eq!(pb.next(), Some(4));
//! assert_eq!(pb.next(), Some(2));
//! ```

pub struct PutBack<I: Iterator> {
    iter: I,
    putback: Vec<I::Item>,
}

impl<I> PutBack<I> where I: Iterator {
    /// Construct a new `PutBack` for the `Iterator` with an empty put
    /// back vector.
    pub fn new(iter: I) -> PutBack<I> {
        PutBack { iter: iter, putback: Vec::new(), }
    }

    /// Put a value back into the `Iterator`. The value will be the
    /// first value in the `Iterator`.
    pub fn put_back(&mut self, item: I::Item) {
        self.putback.push(item);
    }
}

impl<I> Iterator for PutBack<I> where I: Iterator {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        self.putback.pop().or_else(|| self.iter.next())
    }
}
