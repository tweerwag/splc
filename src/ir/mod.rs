//! Defines the intermediate representation.

pub mod builtins;
pub mod display;
pub mod from_ast;
pub mod to_asm;

/// Used internally to represent variables. Positive values are local
/// variables, negative values are parameters, and zero represents the
/// return register.
#[derive(Debug, Clone, Copy)]
pub struct Var(i32);
/// Used internally to represent labels.
pub type Lbl = u32;

#[derive(Debug, Clone, Copy)]
pub enum BinOpKind {
    Or, And,
    Eq, LT, GT, LEq, GEq, NEq,
    Add, Subtract,
    Mult, Divide, Remainder,
}

#[derive(Debug, Clone, Copy)]
pub enum UnOpKind {
    Neg,
    Opp,
}

#[derive(Debug, Clone)]
pub struct FunCall {
    name: String,
    args: Vec<Expr>,
}

#[derive(Debug, Clone)]
pub enum Expr {
    Imm(u32),
    Local(Var),
    Global(u32),
    BinOp { kind: BinOpKind, expr1: Box<Expr>, expr2: Box<Expr> },
    UnOp { kind: UnOpKind, expr: Box<Expr> },
    FunCall(FunCall),
    Deref { expr: Box<Expr>, offset: u32 },
    Malloc { exprs: Vec<Expr> },
}

#[derive(Debug, Clone)]
pub enum IR {
    Label(Lbl),
    FunCall(FunCall),
    Assign { dest: Var, expr: Box<Expr> },
    AssignGlobal { dest: u32, expr: Box<Expr> },
    StoreAt { d_expr: Box<Expr>, offset: u32, v_expr: Box<Expr> },
    Jump { to: Lbl },
    Branch { to: Lbl, expr: Box<Expr> },
    Trap { syscall: u32, exprs: Vec<Expr> },
}

#[derive(Debug, Clone)]
pub struct Fun {
    name: String,
    code: Vec<IR>,
}

#[derive(Debug, Clone)]
pub struct Program {
    global_vars: Vec<Expr>,
    funs: Vec<Fun>,
}
