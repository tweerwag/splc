use ir;
use std::fmt;

impl fmt::Display for ir::BinOpKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ir::BinOpKind::*;
        match *self {
            Or => write!(f, "|"),
            And => write!(f, "&"),
            Eq => write!(f, "=="),
            LT => write!(f, "<"),
            GT => write!(f, ">"),
            LEq => write!(f, "<="),
            GEq => write!(f, ">="),
            NEq => write!(f, "!="),
            Add => write!(f, "+"),
            Subtract => write!(f, "-"),
            Mult => write!(f, "*"),
            Divide => write!(f, "/"),
            Remainder => write!(f, "%"),
        }
    }
}

impl fmt::Display for ir::UnOpKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ir::UnOpKind::*;
        match *self {
            Neg => write!(f, "!"),
            Opp => write!(f, "-"),
        }
    }
}

impl fmt::Display for ir::Var {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.0 < 0 {
            write!(f, "%parm_{}", -self.0 - 1)
        } else if self.0 == 0 {
            write!(f, "%rr")
        } else {
            write!(f, "%{}", self.0)
        }
    }
}

impl fmt::Display for ir::FunCall {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}(", &self.name));
        let mut iter = self.args.iter();
        if let Some(arg) = iter.next() {
            try!(write!(f, "{}", arg));
            for arg in iter {
                try!(write!(f, ", {}", arg));
            }
        }
        write!(f, ")")
    }
}

impl fmt::Display for ir::Expr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ir::Expr::*;
        match *self {
            Imm(n) => write!(f, "${}", n),
            Local(v) => write!(f, "{}", v),
            Global(n) => write!(f, "%global_{}", n),
            BinOp { kind, ref expr1, ref expr2 } =>
                write!(f, "({} {} {})", expr1, kind, expr2),
            UnOp { kind, ref expr } =>
                write!(f, "({} {})", kind, expr),
            FunCall(ref fc) =>
                write!(f, "{}", fc),
            Deref { ref expr, offset } =>
                write!(f, "{}({})", offset, &*expr),
            Malloc { ref exprs  } => {
                try!(write!(f, "(malloc"));
                for e in exprs {
                    try!(write!(f, " {}", e));
                }
                write!(f, ")")
            }
        }
    }
}

impl fmt::Display for ir::IR {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ir::IR::*;
        match *self {
            Label(lbl) =>
                write!(f, "_l{}:", lbl),
            FunCall(ref fc) =>
                write!(f, "\t{}", fc),
            Assign { dest, ref expr } =>
                write!(f, "\t{} = {}", dest, expr),
            AssignGlobal { dest, ref expr } =>
                write!(f, "\t%global_{} = {}", dest, expr),
            StoreAt { ref d_expr, offset, ref v_expr, } =>
                write!(f, "\t{}({}) = {}", offset, d_expr, v_expr),
            Jump { to } =>
                write!(f, "\tjmp _l{}", to),
            Branch { to, ref expr } =>
                write!(f, "\tbrt _l{}, {}", to, expr),
            Trap { syscall, ref exprs } => {
                try!(write!(f, "\ttrap {}", syscall));
                for expr in exprs {
                    try!(write!(f, ", {}", expr));
                }
                Ok(())
            }
        }
    }
}

impl fmt::Display for ir::Fun {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}:", &self.name));
        for ir in &self.code {
            try!(write!(f, "\n{}", ir));
        }
        Ok(())
    }
}

impl fmt::Display for ir::Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut iter = self.funs.iter();
        if let Some(fun) = iter.next() {
            try!(write!(f, "{}", fun));
            for fun in iter {
                try!(write!(f, "\n\n{}", fun));
            }
        }
        Ok(())
    }
}
