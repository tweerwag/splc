//! Conversion from AST to IR.

use ir::{self, Var, Lbl, IR, BinOpKind, UnOpKind};
use ir::builtins::generate_builtin;
use ast;
use std::collections::{HashMap, HashSet};

const RETURN_LBL: Lbl = 0;
const RETURN_VAR: Var = Var(0);

struct Ctx {
    fresh_var_cnt: Var,
    fresh_lbl_cnt: Lbl,
    local_symb_table: HashMap<String, Var>,
    global_symb_table: HashMap<String, u32>,
    gen_builtins: HashSet<String>,
}

impl Ctx {
    fn new() -> Ctx {
        Ctx {
            fresh_var_cnt: Var(1),
            fresh_lbl_cnt: 1,
            local_symb_table: HashMap::new(),
            global_symb_table: HashMap::new(),
            gen_builtins: HashSet::new(),
        }
    }

    fn clear_local(&mut self) {
        self.fresh_var_cnt = Var(1);
        self.fresh_lbl_cnt = 1;
        self.local_symb_table = HashMap::new();
    }

    fn fresh_var(&mut self) -> Var {
        let ret = self.fresh_var_cnt;
        self.fresh_var_cnt.0 += 1;
        ret
    }

    fn fresh_lbl(&mut self) -> Lbl {
        let ret = self.fresh_lbl_cnt;
        self.fresh_lbl_cnt += 1;
        ret
    }

    fn add_local_symbol(&mut self, s: String, v: Var) {
        self.local_symb_table.insert(s, v);
    }

    fn get_local_symbol(&self, s: &str) -> Option<Var> {
        self.local_symb_table.get(s).cloned()
    }

    fn add_global_symbol(&mut self, s: String, n: u32) {
        self.global_symb_table.insert(s, n);
    }

    fn get_global_symbol(&self, s: &str) -> Option<u32> {
        self.global_symb_table.get(s).cloned()
    }

    /// Yields the expression for the symbol treated as an r-value.
    fn get_symbol_rexpr(&self, s: &str) -> Option<ir::Expr> {
        self.get_local_symbol(s)
            .map(|v| ir::Expr::Local(v))
            .or_else(|| self.get_global_symbol(s)
                     .map(|n| ir::Expr::Global(n)))
    }

    fn add_builtin(&mut self, s: &str) {
        self.gen_builtins.insert(String::from(s));
    }
}

fn un_op_kind(op: ast::UnOp) -> UnOpKind {
    match op {
        ast::UnOp::Neg => UnOpKind::Neg,
        ast::UnOp::Opp => UnOpKind::Opp,
    }
}

fn bin_op_kind(op: ast::BinOp) -> BinOpKind {
    match op {
        ast::BinOp::Or => BinOpKind::Or,
        ast::BinOp::And => BinOpKind::And,
        ast::BinOp::Eq => BinOpKind::Eq,
        ast::BinOp::LT => BinOpKind::LT,
        ast::BinOp::GT => BinOpKind::GT,
        ast::BinOp::LEq => BinOpKind::LEq,
        ast::BinOp::GEq => BinOpKind::GEq,
        ast::BinOp::NEq => BinOpKind::NEq,
        ast::BinOp::Add => BinOpKind::Add,
        ast::BinOp::Subtract => BinOpKind::Subtract,
        ast::BinOp::Mult => BinOpKind::Mult,
        ast::BinOp::Divide => BinOpKind::Divide,
        ast::BinOp::Remainder => BinOpKind::Remainder,
        ast::BinOp::Cons => panic!("the cons operator is not a primitive ir expression"),
    }
}

fn fun_call(fc: &ast::FunCall, ctx: &mut Ctx) -> ir::FunCall {
    let name = match *fc.overloaded.borrow() {
        None => fc.name.id.clone(),
        Some(ref name) => {
            ctx.add_builtin(name);
            name.clone()
        }
    };
    let args = fc.args.iter().map(|e| expr(e, ctx)).collect();
    ir::FunCall { name: name, args: args }
}

fn field_sel_r(varname: &str, fields: &[ast::Field], ctx: &Ctx) -> ir::Expr {
    let mut e = ctx.get_symbol_rexpr(varname).unwrap();
    for field in fields {
        use ast::Field::*;
        match *field {
            Hd | Fst =>
                e = ir::Expr::Deref {
                    expr: Box::new(e),
                    offset: 0,
                },
            Tl | Snd =>
                e = ir::Expr::Deref {
                    expr: Box::new(e),
                    offset: 1,
                },
        }
    }
    e
}

/// Generates an ir expression from an ast expression.
fn expr(e: &ast::Expr, ctx: &mut Ctx) -> ir::Expr {
    use ast::ExprEnum::*;
    match e.expr_enum {
        FieldSel(ref fs) => field_sel_r(&*fs.var.id, &*fs.fields, ctx),
        IntLit(n) => ir::Expr::Imm(n),
        CharLit(c) => ir::Expr::Imm(c as u32),
        FunCall(ref fc) => ir::Expr::FunCall(fun_call(fc, ctx)),
        BinExpr { ref arg1, ref arg2, op: ast::BinOp::Cons, .. } => {
            let e1 = expr(arg1, ctx);
            let e2 = expr(arg2, ctx);
            ir::Expr::Malloc { exprs: vec![e1, e2] }
        }
        BinExpr { ref arg1, ref arg2, op, ref overloaded } => {
            match *overloaded.borrow() {
                None => {
                    ir::Expr::BinOp {
                        kind: bin_op_kind(op),
                        expr1: Box::new(expr(arg1, ctx)),
                        expr2: Box::new(expr(arg2, ctx)),
                    }
                }
                Some(ref name) => {
                    ctx.add_builtin(name);
                    let funcall = ir::FunCall {
                        name: name.clone(),
                        args: vec![
                            expr(arg1, ctx),
                            expr(arg2, ctx),
                        ],
                    };
                    ir::Expr::FunCall(funcall)
                }
            }
        }
        UnExpr { ref arg, op } => {
            ir::Expr::UnOp {
                kind: un_op_kind(op),
                expr: Box::new(expr(arg, ctx)),
            }
        }
        ListNil => ir::Expr::Imm(0),
        True => ir::Expr::Imm(1),
        False => ir::Expr::Imm(0),
        Tuple(ref arg1, ref arg2) => {
            let e1 = expr(arg1, ctx);
            let e2 = expr(arg2, ctx);
            ir::Expr::Malloc { exprs: vec![e1, e2] }
        }
    }
}

fn stmt(s: &ast::Stmt, code: &mut Vec<IR>, ctx: &mut Ctx) {
    use ast::StmtEnum::*;
    match s.stmt_enum {
        If { ref cond, ref true_branch, ref false_branch } => {
            let l_true = ctx.fresh_lbl();
            let l_end = ctx.fresh_lbl();
            let e_cond = expr(cond, ctx);
            code.push(IR::Branch {
                to: l_true,
                expr: Box::new(e_cond),
            });
            for s in false_branch {
                stmt(s, code, ctx);
            }
            code.push(IR::Jump { to: l_end });
            code.push(IR::Label(l_true));
            for s in true_branch {
                stmt(s, code, ctx);
            }
            code.push(IR::Label(l_end));
        }
        While { ref cond, ref stmts } => {
            let l_begin = ctx.fresh_lbl();
            let l_cond = ctx.fresh_lbl();
            let e_cond = expr(cond, ctx);
            code.push(IR::Jump { to: l_cond });
            code.push(IR::Label(l_begin));
            for s in stmts {
                stmt(s, code, ctx);
            }
            code.push(IR::Label(l_cond));
            code.push(IR::Branch {
                to: l_begin,
                expr: Box::new(e_cond),
            });
        }
        Assignment { ref fieldsel, expr: ref e } => {
            let len = fieldsel.fields.len();
            let e = expr(e, ctx);
            if len > 0 {
                // We need to do an indirect store
                let deref_fields = &fieldsel.fields[..len-1];
                let deref_e = field_sel_r(&fieldsel.var.id, deref_fields, ctx);
                use ast::Field::*;
                let offset = match fieldsel.fields[len-1] {
                    Hd | Fst => 0,
                    Tl | Snd => 1,
                };
                code.push(IR::StoreAt {
                    d_expr: Box::new(deref_e),
                    offset: offset,
                    v_expr: Box::new(e)
                });
            } else {
                if let Some(v) = ctx.get_local_symbol(&fieldsel.var.id) {
                    code.push(IR::Assign { dest: v, expr: Box::new(e) });
                } else {
                    // It must be a global variable
                    let n = ctx.get_global_symbol(&fieldsel.var.id).unwrap();
                    code.push(IR::AssignGlobal { dest: n, expr: Box::new(e) });
                }
            }
        }
        FunCall(ref fc) =>
            code.push(IR::FunCall(fun_call(fc, ctx))),
        Return(ref opt_e) => {
            if let Some(ref e) = *opt_e {
                let ir_e = expr(e, ctx);
                code.push(IR::Assign {
                    dest: RETURN_VAR,
                    expr: Box::new(ir_e),
                });
            }
            code.push(IR::Jump { to: RETURN_LBL });
        }
    }
}

fn fun_decl(fd: &ast::FunDecl, ctx: &mut Ctx) -> ir::Fun {
    let mut code = Vec::new();
    ctx.clear_local();
    for (n, arg) in fd.arg_names.iter().enumerate() {
        ctx.add_local_symbol(arg.id.clone(), Var(-(n as i32) - 1));
    }
    for vd in &fd.var_decls {
        let v = ctx.fresh_var();
        let e = expr(&vd.expr, ctx);
        code.push(IR::Assign { dest: v, expr: Box::new(e) });
        ctx.add_local_symbol(vd.name.id.clone(), v);
    }
    for s in &fd.stmts {
        stmt(s, &mut code, ctx);
    }
    code.push(IR::Label(RETURN_LBL));
    ir::Fun {
        name: fd.name.id.clone(),
        code: code,
    }
}

pub fn program(prog: &ast::Program) -> ir::Program {
    let mut ctx = Ctx::new();
    let mut global_vars = Vec::new();
    for (n, vd) in prog.var_decls.iter().enumerate() {
        global_vars.push(expr(&vd.expr, &mut ctx));
        ctx.add_global_symbol(vd.name.id.clone(), n as u32);
    }
    let mut funs = prog.fun_decls.iter()
        .map(|fd| fun_decl(fd, &mut ctx))
        .collect::<Vec<_>>();

    funs.extend(ctx.gen_builtins.iter().map(|ref name| generate_builtin(name)));

    ir::Program {
        global_vars: global_vars,
        funs: funs,
    }
}
