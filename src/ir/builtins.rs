/// Generates IR code for builtin functions.

use ir::{self, Var};
use std::str::Chars;

enum Type {
    Void, Int, Bool, Char,
    Tuple(Box<Type>, Box<Type>),
    List(Box<Type>),
}

const SYSCALL_PRINT_INT: u32 = 0;
const SYSCALL_PRINT_CHAR: u32 = 1;

fn unmangle(s: &str) -> Type {
    fn _unmangle(cs: &mut Chars) -> Type {
        match cs.next() {
            Some('v') => Type::Void,
            Some('i') => Type::Int,
            Some('b') => Type::Bool,
            Some('c') => Type::Char,
            Some('t') => {
                let ty1 = _unmangle(cs);
                let ty2 = _unmangle(cs);
                Type::Tuple(Box::new(ty1), Box::new(ty2))
            }
            Some('l') => {
                let ty = _unmangle(cs);
                Type::List(Box::new(ty))
            }
            _ => panic!("invalid mangled type"),
        }
    }
    _unmangle(&mut s.chars())
}

/// Generic function which dispatches on the name of the builtin.
pub fn generate_builtin(name: &str) -> ir::Fun {
    let parts = name.split('_').skip(2).collect::<Vec<_>>();
    match parts[0] {
        "isEmpty" => generate_is_empty(),
        "print" => generate_print(parts[1]),
        "eq" => generate_eq(parts[1]),
        "neq" => generate_neq(parts[1]),
        _ => panic!("does not know how to generate builtin '{}'", name),
    }
}

fn generate_is_empty() -> ir::Fun {
    let expr1 = ir::Expr::Local(Var(-1));
    let expr2 = ir::Expr::Imm(0);
    let expr = ir::Expr::BinOp {
        kind: ir::BinOpKind::Eq,
        expr1: Box::new(expr1),
        expr2: Box::new(expr2),
    };
    let stmt = ir::IR::Assign {
        dest: Var(0),
        expr: Box::new(expr),
    };
    ir::Fun {
        name: String::from("__isEmpty"),
        code: vec![stmt],
    }
}

fn generate_print(arg: &str) -> ir::Fun {
    fn _print_str(s: &str, code: &mut Vec<ir::IR>) {
        for c in s.chars() {
            code.push(ir::IR::Trap {
                syscall: SYSCALL_PRINT_CHAR,
                exprs: vec![ir::Expr::Imm(c as u32)],
            });
        }
    }

    fn _generate_print(ty: Type, code: &mut Vec<ir::IR>, lbl_cnt: &mut u32, var_cnt: &mut i32) {
        match ty {
            Type::Void => panic!("cannot print values of type 'Void'"),
            Type::Int => {
                code.push(ir::IR::Trap {
                    syscall: SYSCALL_PRINT_INT,
                    exprs: vec![ir::Expr::Local(Var(-1))],
                });
            }
            Type::Bool => {
                let false_lbl = *lbl_cnt;
                let end_lbl = *lbl_cnt + 1;
                *lbl_cnt += 2;
                let e_cond = ir::Expr::BinOp {
                    kind: ir::BinOpKind::Eq,
                    expr1: Box::new(ir::Expr::Local(Var(-1))),
                    expr2: Box::new(ir::Expr::Imm(0)),
                };
                code.push(ir::IR::Branch {
                    to: false_lbl,
                    expr: Box::new(e_cond),
                });
                _print_str("True", code);
                code.push(ir::IR::Jump { to: end_lbl });
                code.push(ir::IR::Label(false_lbl));
                _print_str("False", code);
                code.push(ir::IR::Label(end_lbl));
            }
            Type::Char => {
                code.push(ir::IR::Trap {
                    syscall: SYSCALL_PRINT_CHAR,
                    exprs: vec![ir::Expr::Local(Var(-1))],
                });
            }
            Type::Tuple(ty1, ty2) => {
                let tmp_var = Var(*var_cnt); *var_cnt += 1;
                code.push(ir::IR::Assign {
                    dest: tmp_var,
                    expr: Box::new(ir::Expr::Local(Var(-1))),
                });
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp_var)),
                        offset: 0,
                    }),
                });
                _generate_print(*ty1, code, lbl_cnt, var_cnt);
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp_var)),
                        offset: 1,
                    }),
                });
                _generate_print(*ty2, code, lbl_cnt, var_cnt);
            }
            Type::List(ty) => {
                let body_lbl = *lbl_cnt;
                let cond_lbl = *lbl_cnt + 1;
                *lbl_cnt += 2;
                let tmp_var = Var(*var_cnt); *var_cnt += 1;
                code.push(ir::IR::Assign {
                    dest: tmp_var,
                    expr: Box::new(ir::Expr::Local(Var(-1))),
                });
                code.push(ir::IR::Jump { to: cond_lbl });
                code.push(ir::IR::Label(body_lbl));
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp_var)),
                        offset: 0,
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: tmp_var,
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp_var)),
                        offset: 1,
                    }),
                });
                _generate_print(*ty, code, lbl_cnt, var_cnt);
                code.push(ir::IR::Label(cond_lbl));
                code.push(ir::IR::Branch {
                    to: body_lbl,
                    expr: Box::new(ir::Expr::BinOp {
                        kind: ir::BinOpKind::NEq,
                        expr1: Box::new(ir::Expr::Local(tmp_var)),
                        expr2: Box::new(ir::Expr::Imm(0)),
                    }),
                });
            }
        }
    }

    let mut code = Vec::new();
    _generate_print(unmangle(arg), &mut code, &mut 0, &mut 1);
    ir::Fun {
        name: format!("__print_{}", arg),
        code: code,
    }
}

fn generate_eq(arg: &str) -> ir::Fun {
    fn _generate_eq(ty: Type, code: &mut Vec<ir::IR>, lbl_cnt: &mut u32, var_cnt: &mut i32) {
        match ty {
            Type::Void => panic!("cannot compare values of type 'Void'"),
            Type::Int | Type::Char => {
                code.push(ir::IR::Assign {
                    dest: Var(0),
                    expr: Box::new(ir::Expr::BinOp {
                        kind: ir::BinOpKind::Eq,
                        expr1: Box::new(ir::Expr::Local(Var(-1))),
                        expr2: Box::new(ir::Expr::Local(Var(-2))),
                    }),
                });
            }
            Type::Bool => {
                // We have to normalize the values first.
                // (e.g. 1 and 2 both represent True, but don't compare equal.)
                code.push(ir::IR::Assign {
                    dest: Var(0),
                    expr: Box::new(ir::Expr::BinOp {
                        kind: ir::BinOpKind::Eq,
                        expr1: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::NEq,
                            expr1: Box::new(ir::Expr::Local(Var(-1))),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                        expr2: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::NEq,
                            expr1: Box::new(ir::Expr::Local(Var(-2))),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                    }),
                });
            }
            Type::Tuple(ty1, ty2) => {
                let tmp1_var = Var(*var_cnt); *var_cnt += 1;
                let tmp2_var = Var(*var_cnt); *var_cnt += 1;
                let end_lbl = *lbl_cnt; *lbl_cnt += 1;
                code.push(ir::IR::Assign {
                    dest: tmp1_var,
                    expr: Box::new(ir::Expr::Local(Var(-1))),
                });
                code.push(ir::IR::Assign {
                    dest: tmp2_var,
                    expr: Box::new(ir::Expr::Local(Var(-2))),
                });

                // Compare the first components of the tuples
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp1_var)),
                        offset: 0,
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: Var(-2),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp2_var)),
                        offset: 0,
                    }),
                });
                _generate_eq(*ty1, code, lbl_cnt, var_cnt);

                // Look if we can exit already
                code.push(ir::IR::Branch {
                    to: end_lbl,
                    expr: Box::new(ir::Expr::UnOp {
                        kind: ir::UnOpKind::Neg,
                        expr: Box::new(ir::Expr::Local(Var(0))),
                    }),
                });

                // Compare the second components of the tuples
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp1_var)),
                        offset: 1,
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: Var(-2),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp2_var)),
                        offset: 1,
                    }),
                });
                _generate_eq(*ty2, code, lbl_cnt, var_cnt);

                // And an end label
                code.push(ir::IR::Label(end_lbl));
            }
            Type::List(ty) => {
                let tmp1_var = Var(*var_cnt); *var_cnt += 1;
                let tmp2_var = Var(*var_cnt); *var_cnt += 1;
                let body_lbl = *lbl_cnt; *lbl_cnt += 1;
                let cond_lbl = *lbl_cnt; *lbl_cnt += 1;
                let end_lbl = *lbl_cnt; *lbl_cnt += 1;
                code.push(ir::IR::Assign {
                    dest: tmp1_var,
                    expr: Box::new(ir::Expr::Local(Var(-1))),
                });
                code.push(ir::IR::Assign {
                    dest: tmp2_var,
                    expr: Box::new(ir::Expr::Local(Var(-2))),
                });
                code.push(ir::IR::Jump { to: cond_lbl });

                code.push(ir::IR::Label(body_lbl));
                code.push(ir::IR::Assign {
                    dest: Var(-1),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp1_var)),
                        offset: 0,
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: Var(-2),
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp2_var)),
                        offset: 0,
                    }),
                });
                _generate_eq(*ty, code, lbl_cnt, var_cnt);
                code.push(ir::IR::Branch {
                    to: end_lbl,
                    expr: Box::new(ir::Expr::UnOp {
                        kind: ir::UnOpKind::Neg,
                        expr: Box::new(ir::Expr::Local(Var(0))),
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: tmp1_var,
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp1_var)),
                        offset: 1,
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: tmp2_var,
                    expr: Box::new(ir::Expr::Deref {
                        expr: Box::new(ir::Expr::Local(tmp2_var)),
                        offset: 1,
                    }),
                });

                code.push(ir::IR::Label(cond_lbl));
                code.push(ir::IR::Branch {
                    to: body_lbl,
                    expr: Box::new(ir::Expr::BinOp {
                        kind: ir::BinOpKind::And,
                        expr1: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::NEq,
                            expr1: Box::new(ir::Expr::Local(tmp1_var)),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                        expr2: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::NEq,
                            expr1: Box::new(ir::Expr::Local(tmp2_var)),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                    }),
                });

                code.push(ir::IR::Assign {
                    dest: Var(0),
                    expr: Box::new(ir::Expr::Imm(1)),
                });
                code.push(ir::IR::Branch {
                    to: end_lbl,
                    expr: Box::new(ir::Expr::BinOp {
                        kind: ir::BinOpKind::And,
                        expr1: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::Eq,
                            expr1: Box::new(ir::Expr::Local(tmp1_var)),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                        expr2: Box::new(ir::Expr::BinOp {
                            kind: ir::BinOpKind::Eq,
                            expr1: Box::new(ir::Expr::Local(tmp2_var)),
                            expr2: Box::new(ir::Expr::Imm(0)),
                        }),
                    }),
                });
                code.push(ir::IR::Assign {
                    dest: Var(0),
                    expr: Box::new(ir::Expr::Imm(0)),
                });

                code.push(ir::IR::Label(end_lbl));
            }
        }
    }

    let mut code = Vec::new();
    _generate_eq(unmangle(arg), &mut code, &mut 0, &mut 1);
    ir::Fun {
        name: format!("__eq_{}", arg),
        code: code,
    }
}

fn generate_neq(arg: &str) -> ir::Fun {
    let mut fun = generate_eq(arg);
    fun.code.push(ir::IR::Assign {
        dest: Var(0),
        expr: Box::new(ir::Expr::UnOp {
            kind: ir::UnOpKind::Neg,
            expr: Box::new(ir::Expr::Local(Var(0))),
        }),
    });
    fun.name = format!("__neq_{}", arg);
    fun
}
