use ir::{Var, Lbl, Program, Fun, IR, Expr, FunCall, BinOpKind, UnOpKind};
use std::fmt::Write;

struct Ctx<'a> {
    code: String,
    fun_name: &'a str,
    highest_local: i32,
}

impl<'a> Ctx<'a> {
    fn new(fun_name: &str) -> Ctx {
        Ctx {
            code: String::new(),
            fun_name: fun_name,
            highest_local: 0,
        }
    }

    fn gen_label(&self, lbl: Lbl) -> String {
        format!("_{}_{}", self.fun_name, lbl)
    }
}

macro_rules! write_code {
    ($ctx:expr, $($arg:tt)+) => ({
        write!($ctx.code, $($arg)+).unwrap();
    })
}

fn load_var(v: Var, ctx: &mut Ctx) {
    if v.0 < 0 {
        write_code!(ctx, "\tldl {}\n", v.0 - 1);
    } else if v.0 == 0 {
        write_code!(ctx, "\tldr RR\n");
    } else {
        if v.0 > ctx.highest_local {
            ctx.highest_local = v.0;
        }
        write_code!(ctx, "\tldl {}\n", v.0);
    }
}

fn store_var(v: Var, ctx: &mut Ctx) {
    if v.0 < 0 {
        write_code!(ctx, "\tstl {}\n", v.0 - 1);
    } else if v.0 == 0 {
        write_code!(ctx, "\tstr RR\n");
    } else {
        if v.0 > ctx.highest_local {
            ctx.highest_local = v.0;
        }
        write_code!(ctx, "\tstl {}\n", v.0);
    }
}

fn binop(kind: BinOpKind, ctx: &mut Ctx) {
    use ir::BinOpKind::*;
    let s = match kind {
        Or => "or",
        And => "and",
        Eq => "eq",
        LT => "lt",
        GT => "gt",
        LEq => "le",
        GEq => "ge",
        NEq => "ne",
        Add => "add",
        Subtract => "sub",
        Mult => "mul",
        Divide => "div",
        Remainder => "mod",
    };
    write_code!(ctx, "\t{}\n", s);
}

fn unop(kind: UnOpKind, ctx: &mut Ctx) {
    use ir::UnOpKind::*;
    let s = match kind {
        Neg => "not",
        Opp => "neg",
    };
    write_code!(ctx, "\t{}\n", s);
}

fn ir_funcall(fc: &FunCall, ctx: &mut Ctx) {
    let n = fc.args.len();
    for expr in fc.args.iter().rev() {
        ir_expr(expr, ctx);
    }
    write_code!(ctx, "\tbsr {}\n", &fc.name);
    if n > 0 {
        write_code!(ctx, "\tajs {}\n", -(n as i32));
    }
}

fn ir_expr(e: &Expr, ctx: &mut Ctx) {
    use ir::Expr::*;
    match *e {
        Imm(n) => write_code!(ctx, "\tldc {}\n", n),
        Local(v) => load_var(v, ctx),
        Global(n) => {
            write_code!(ctx, "\tldr R5\n");
            write_code!(ctx, "\tlda {}\n", n);
        }
        BinOp { kind, ref expr1, ref expr2 } => {
            ir_expr(expr1, ctx);
            ir_expr(expr2, ctx);
            binop(kind, ctx);
        }
        UnOp { kind, ref expr } => {
            ir_expr(expr, ctx);
            unop(kind, ctx);
        }
        FunCall(ref fc) => {
            ir_funcall(fc, ctx);
            write_code!(ctx, "\tldr RR\n");
        }
        Deref { ref expr, offset } => {
            ir_expr(expr, ctx);
            write_code!(ctx, "\tlda {}\n", offset);
        }
        Malloc { ref exprs } => {
            let n = exprs.len();
            for expr in exprs {
                ir_expr(expr, ctx);
            }
            write_code!(ctx, "\tstmh {}\n", n);
            if n > 1 {
                // adjust the returned pointer, since it points to the last value
                write_code!(ctx, "\tldc {}\n", n - 1);
                write_code!(ctx, "\tsub\n");
            }
        }
    }
}

fn ir_stmt(s: &IR, ctx: &mut Ctx) {
    use ir::IR::*;
    match *s {
        Label(lbl) => {
            let lbl_name = ctx.gen_label(lbl);
            write_code!(ctx, "{}:\n", lbl_name);
        }
        FunCall(ref fc) => {
            ir_funcall(fc, ctx);
        }
        Assign { dest, ref expr } => {
            ir_expr(expr, ctx);
            store_var(dest, ctx);
        }
        AssignGlobal { dest, ref expr } => {
            ir_expr(expr, ctx);
            write_code!(ctx, "\tldr R5\n");
            write_code!(ctx, "\tsta {}\n", dest);
        }
        StoreAt { ref d_expr, offset, ref v_expr } => {
            ir_expr(v_expr, ctx);
            ir_expr(d_expr, ctx);
            write_code!(ctx, "\tsta {}\n", offset);
        }
        Jump { to } => {
            let lbl_name = ctx.gen_label(to);
            write_code!(ctx, "\tbra {}\n", lbl_name);
        }
        Branch { to, ref expr } => {
            let lbl_name = ctx.gen_label(to);
            ir_expr(expr, ctx);
            write_code!(ctx, "\tbrt {}\n", lbl_name);
        }
        Trap { syscall, ref exprs } => {
            for expr in exprs {
                ir_expr(expr, ctx);
            }
            write_code!(ctx, "\ttrap {}\n", syscall);
        }
    }
}

fn fun(f: &Fun) -> String {
    // First generate code for the body of the function.
    // This way, we know how many locals we encounter.
    let mut ctx = Ctx::new(&f.name);
    for s in &f.code {
        ir_stmt(s, &mut ctx);
    }

    let mut code = String::new();
    write!(code, "{}:\n", f.name).unwrap();
    write!(code, "\tlink {}\n", ctx.highest_local).unwrap();
    write!(code, "{}", ctx.code).unwrap();
    write!(code, "\tunlink\n").unwrap();
    write!(code, "\tret\n").unwrap();
    code
}

pub fn program(prog: &Program) -> String {
    let mut code = String::new();
    write!(code, "\tbra __init\n").unwrap();

    for f in &prog.funs {
        write!(code, "{}", fun(f)).unwrap();
    }

    // write the init function
    let ref mut ctx = Ctx::new("__init");
    write_code!(ctx, "__init:\n");
    let n_gvars = prog.global_vars.len();
    if n_gvars > 0 {
        for expr in &prog.global_vars {
            ir_expr(expr, ctx);
        }
        write_code!(ctx, "\tstmh {}\n", n_gvars);
        write_code!(ctx, "\tldc {}\n", n_gvars - 1);
        write_code!(ctx, "\tsub\n");
        write_code!(ctx, "\tstr R5\n");
    }
    write_code!(ctx, "\tbsr main\n");
    write_code!(ctx, "\thalt\n");

    write!(code, "{}", ctx.code).unwrap();

    code
}
