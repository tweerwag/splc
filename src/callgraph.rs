//! Utility to create a call graph from an AST.

use std::collections::HashMap;
use ast::*;
use petgraph::Graph;
use petgraph::graph::NodeIndex;

pub fn call_graph(prog: &Program) -> Graph<&str, ()> {
    let mut g = Graph::new();
    let names = prog.fun_decls.iter()
        .map(|fd| &*fd.name.id)
        .map(|name| (name, g.add_node(name)))
        .collect::<HashMap<_, _>>();
    let mut info = Info { names: names, graph: g, cur: "" };
    for fd in &prog.fun_decls {
        call_graph_fundecl(fd, &mut info);
    }
    info.graph
}

struct Info<'a> {
    names: HashMap<&'a str, NodeIndex>,
    graph: Graph<&'a str, ()>,
    cur: &'a str,
}

fn call_graph_fundecl<'a>(fd: &'a FunDecl, info: &mut Info<'a>) {
    info.cur = &*fd.name.id;
    for vd in &fd.var_decls {
        call_graph_vardecl(vd, info);
    }
    for stmt in &fd.stmts {
        call_graph_stmt(stmt, info);
    }
}

fn call_graph_vardecl<'a>(vd: &'a VarDecl, info: &mut Info<'a>) {
    call_graph_expr(&vd.expr, info);
}

fn call_graph_stmt<'a>(stmt: &'a Stmt, info: &mut Info<'a>) {
    use ast::StmtEnum::*;
    match stmt.stmt_enum {
        If { ref cond, ref true_branch, ref false_branch } => {
            call_graph_expr(cond, info);
            for stmt in true_branch {
                call_graph_stmt(stmt, info);
            }
            for stmt in false_branch {
                call_graph_stmt(stmt, info);
            }
        }
        While { ref cond, ref stmts } => {
            call_graph_expr(cond, info);
            for stmt in stmts {
                call_graph_stmt(stmt, info);
            }
        }
        Assignment { ref expr, .. } => {
            call_graph_expr(expr, info);
        }
        FunCall(ref fc) => {
            call_graph_funcall(fc, info);
        }
        Return(Some(ref expr)) => {
            call_graph_expr(expr, info);
        }
        Return(None) => { }
    }
}

fn call_graph_expr<'a>(expr: &'a Expr, info: &mut Info<'a>) {
    use ast::ExprEnum::*;
    match expr.expr_enum {
        FunCall(ref fc) => {
            call_graph_funcall(fc, info);
        }
        BinExpr { ref arg1, ref arg2, .. } => {
            call_graph_expr(arg1, info);
            call_graph_expr(arg2, info);
        }
        UnExpr { ref arg, .. } => {
            call_graph_expr(arg, info);
        }
        Tuple(ref expr1, ref expr2) => {
            call_graph_expr(expr1, info);
            call_graph_expr(expr2, info);
        }
        _ => { }
    }
}

const BUILTINS: [&'static str; 2] = ["print", "isEmpty"];

fn call_graph_funcall<'a>(fc: &'a FunCall, info: &mut Info<'a>) {
    if !BUILTINS.contains(&&*fc.name.id) {
        let n1 = info.names[info.cur];
        let n2 = info.names[&*fc.name.id];
        info.graph.update_edge(n1, n2, ());
    }

    for expr in &fc.args {
        call_graph_expr(expr, info);
    }
}
