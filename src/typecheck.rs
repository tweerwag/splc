//! Type checker based on algorithm M.

use std::collections::{HashMap, HashSet};
use std::io;
use std::io::prelude::*;
use std::char;
use petgraph::algo::{condensation, toposort};
use ast::{self, Qualid};
use posmap::{PosMap, Pos, ErrorKind};
use callgraph::call_graph;

/// Type variables are represented as integers.
pub type TyVar = u32;

#[derive(PartialEq, Clone, Copy)]
pub enum SeenReturn {
    /// We have not seen any return yet, or only void returns.
    No,
    /// We have seen a return in a branch, but the other didn't have one.
    Partial,
    /// We have seen returns in every branch.
    Full,
}

/// Is used to keep track of fresh type variables and other state used
/// for type inference.
pub struct TyCtx<'a> {
    var_cnt: u32,
    pm: &'a PosMap<'a>,
    have_err: bool,
    seen_ret: SeenReturn,
}

impl<'a> TyCtx<'a> {
    fn new(pm: &'a PosMap) -> TyCtx<'a> {
        TyCtx {
            var_cnt: 0,
            pm: pm,
            have_err: false,
            seen_ret: SeenReturn::No,
        }
    }

    /// Returns a fresh type variable in this context.
    fn fresh_var(&mut self) -> TyVar {
        self.var_cnt += 1;
        self.var_cnt - 1
    }

    fn overload_funcall(&mut self, fc_overloaded: &mut Option<String>, name: &str) {
        *fc_overloaded = Some(String::from(name));
    }
}

/// We represent a substitution as a map between `TyVar`s and `Type`s.
pub type Substitution = HashMap<TyVar, Type>;

/// Extend `Substitution` with a compose function.
trait Compose {
    /// Compose two `Substitution`s. Note that `s1.compose(s2)`
    /// results in `s1` being altered to represent `s2 \circ s1`.
    fn compose(&mut self, s: Self);
}

impl Compose for Substitution {
    fn compose(&mut self, s2: Substitution) {
        // First apply s2 to existing mappings
        for (_, ty) in self.iter_mut() {
            *ty = ty.substitute(&s2);
        }

        // Collect all var/type pairs which don't yet exist in self
        let ext: Vec<_> = s2.into_iter()
            .filter(|&(var, _)| !self.contains_key(&var))
            .collect();

        // Extend self with those pairs
        self.extend(ext);
    }
}

/// We are going to implement the functions `free_vars` and
/// `substitute` multiple times. It is a bit nicer to put that
/// interface in a trait, even though we only use them in this module.
trait TypeOps {
    /// Computes which free variables occur.
    fn free_vars(&self) -> HashSet<TyVar>;

    /// Substitutes all free occurring `TyVar`s according to the given
    /// `Substitution`.
    fn substitute(&self, s: &Substitution) -> Self;
}

/// We have our own version of types, since our type
/// parameters/variables are different, and we don't care about the
/// position of the corresponding syntax element in the input.
#[derive(Clone, Debug)]
pub enum Type {
    Void,
    Int,
    Bool,
    Char,
    Tuple(Box<Type>, Box<Type>),
    List(Box<Type>),
    Var(TyVar),
    Fun(Vec<Type>, Box<Type>),
}
use self::Type::*;

impl Type {
    pub fn from_ast_type(ast_ty: &ast::Type, tyctx: &mut TyCtx) -> Type {
        match ast_ty.type_enum {
            ast::TypeEnum::Basic(ast::BasicType::Int) => Int,
            ast::TypeEnum::Basic(ast::BasicType::Bool) => Bool,
            ast::TypeEnum::Basic(ast::BasicType::Char) => Char,
            ast::TypeEnum::Tuple(ref ast_ty1, ref ast_ty2) =>
                Tuple(Box::new(Type::from_ast_type(ast_ty1, tyctx)),
                      Box::new(Type::from_ast_type(ast_ty2, tyctx))),
            ast::TypeEnum::List(ref ast_ty) =>
                List(Box::new(Type::from_ast_type(ast_ty, tyctx))),
            // v FIXME: should emit same type variables for earlier used identifiers
            ast::TypeEnum::Parm(_) => Var(tyctx.fresh_var()),
        }
    }

    pub fn from_ast_funtype(ast_funty: &ast::FunType, tyctx: &mut TyCtx) -> Type {
        let argtys = ast_funty.arg_types.iter()
            .map(|ty| Type::from_ast_type(ty, tyctx))
            .collect();
        let retty = ast_funty.ret_type.as_ref()
            .map(|ty| Type::from_ast_type(ty, tyctx))
            .unwrap_or(Void);
        Fun(argtys, Box::new(retty))
    }

    pub fn to_ast_funtype(&self) -> ast::FunType {
        match *self {
            Fun(ref argtys, ref retty) => {
                let arg_types: Vec<_> = argtys.iter().map(|ty| ty.to_ast_type()).collect();
                let ret_type = match **retty {
                    Void => None,
                    ref other_ty => Some(other_ty.to_ast_type()),
                };
                ast::FunType {
                    arg_types: arg_types,
                    ret_type: ret_type,
                    pos: Pos::default(),
                }
            }
            _ => panic!("ty must be a Fun(..)"),
        }
    }

    pub fn to_ast_type(&self) -> ast::Type {
        let type_enum = match *self {
            Void => panic!("cannot convert Void to an ast::Type"),
            Int => ast::TypeEnum::Basic(ast::BasicType::Int),
            Bool => ast::TypeEnum::Basic(ast::BasicType::Bool),
            Char => ast::TypeEnum::Basic(ast::BasicType::Char),
            Tuple(ref tyl, ref tyr) =>
                ast::TypeEnum::Tuple(Box::new(tyl.to_ast_type()),
                                     Box::new(tyr.to_ast_type())),
            List(ref ty) =>
                ast::TypeEnum::List(Box::new(ty.to_ast_type())),
            Var(v) =>
                // This is hackish: we replace a tyvar with a string
                // containing that number, so that we can replace them
                // later on
                ast::TypeEnum::Parm(ast::Ident {
                    id: format!("_{}", v),
                    pos: Pos::default(),
                }),
            Fun(..) => panic!("cannot convert Fun(..) to an ast::Type"),
        };
        ast::Type {
            type_enum: type_enum,
            pos: Pos::default(),
        }
    }

    fn closure(&self, env: &TypeEnvironment) -> TypeScheme {
        TypeScheme {
            vars: &self.free_vars() - &env.free_vars(),
            inner: self.clone(),
        }
    }

    fn is_fun(&self) -> bool {
        match *self {
            Fun(..) => true,
            _ => false,
        }
    }

    /// Mangles a type into a short unique string using a prefix
    /// notation. For example, the type `(Bool, [Char])` gets mangled
    /// to `tblc`.
    fn mangle(&self) -> String {
        fn _mangle(ty: &Type, s: &mut String) {
            match *ty {
                Void => s.push('v'),
                Int => s.push('i'),
                Bool => s.push('b'),
                Char => s.push('c'),
                Tuple(ref ty1, ref ty2) => {
                    s.push('t');
                    _mangle(ty1, s);
                    _mangle(ty2, s);
                }
                List(ref ty) => {
                    s.push('l');
                    _mangle(ty, s);
                }
                Var(..) | Fun(..) =>
                    panic!("cannot mangle type variables and function types"),
            }
        }
        let mut s = String::new();
        _mangle(self, &mut s);
        s
    }
}

impl TypeOps for Type {
    fn free_vars(&self) -> HashSet<TyVar> {
        fn free_vars_(ty: &Type, fvs: &mut HashSet<TyVar>) {
            match ty {
                &Void | &Int | &Bool | &Char => { }
                &Tuple(ref ty1, ref ty2) => {
                    free_vars_(ty1, fvs);
                    free_vars_(ty2, fvs);
                }
                &List(ref ty) => free_vars_(ty, fvs),
                &Var(v) => { fvs.insert(v); }
                &Fun(ref argtys, ref retty) => {
                    for ty in argtys {
                        free_vars_(ty, fvs);
                    }
                    free_vars_(retty, fvs);
                }
            }
        }
        let mut fvs = HashSet::new();
        free_vars_(self, &mut fvs);
        fvs
    }

    fn substitute(&self, s: &Substitution) -> Type {
        match self {
            &Void | &Int | &Bool | &Char => self.clone(),
            &Tuple(ref ty1, ref ty2) => Tuple(Box::new(ty1.substitute(s)), Box::new(ty2.substitute(s))),
            &List(ref ty) => List(Box::new(ty.substitute(s))),
            &Var(v) => {
                match s.get(&v) {
                    Some(ty) => ty.clone(),
                    None => self.clone(),
                }
            }
            &Fun(ref argtys, ref retty) => {
                let new_argtys = argtys.iter().map(|ty| ty.substitute(s)).collect();
                let new_retty = Box::new(retty.substitute(s));
                Fun(new_argtys, new_retty)
            }
        }
    }
}

/// Represents a type scheme, e.g. `\forall \alpha . [\alpha]` in
/// LaTeX notation.
#[derive(Clone, Debug)]
pub struct TypeScheme {
    vars: HashSet<TyVar>,
    inner: Type,
}

impl TypeOps for TypeScheme {
    fn free_vars(&self) -> HashSet<TyVar> {
        &self.inner.free_vars() - &self.vars
    }

    fn substitute(&self, s: &Substitution) -> TypeScheme {
        let mut new_s = s.clone();
        for var in &self.vars {
            new_s.remove(var);
        }
        TypeScheme {
            vars: self.vars.clone(),
            inner: self.inner.substitute(&new_s),
        }
    }
}

impl TypeScheme {
    /// Promote a `Type` to a `TypeScheme`.
    fn promote(ty: Type) -> TypeScheme {
        TypeScheme { vars: HashSet::new(), inner: ty }
    }

    /// Substitute the bound variables by fresh variables and return
    /// the type.
    fn instantiate(&self, tyctx: &mut TyCtx) -> Type {
        let mut s = Substitution::new();
        for &var in &self.vars {
            s.insert(var, Var(tyctx.fresh_var()));
        }
        self.inner.substitute(&s)
    }
}

/// A type environment is a map between `Qualid`s and `TypeScheme`s.
pub type TypeEnvironment<'a> = HashMap<Qualid<'a>, TypeScheme>;

fn add_builtins(env: &mut TypeEnvironment, tyctx: &mut TyCtx) {
    let tyvar = tyctx.fresh_var();
    let mut vars = HashSet::new();
    vars.insert(tyvar);
    let scm = TypeScheme {
        vars: vars,
        inner: Fun(vec![Var(tyvar)], Box::new(Void)),
    };
    env.insert(Qualid::fun("print"), scm);

    let tyvar = tyctx.fresh_var();
    let mut vars = HashSet::new();
    vars.insert(tyvar);
    let scm = TypeScheme {
        vars: vars,
        inner: Fun(vec![List(Box::new(Var(tyvar)))], Box::new(Bool)),
    };
    env.insert(Qualid::fun("isEmpty"), scm);
}

impl<'a> TypeOps for TypeEnvironment<'a> {
    fn free_vars(&self) -> HashSet<TyVar> {
        let mut fvs = HashSet::new();
        for (_, ref tyscm) in self {
            fvs.extend(tyscm.free_vars());
        }
        fvs
    }

    fn substitute(&self, s: &Substitution) -> TypeEnvironment<'a> {
        let mut new_tyenv = TypeEnvironment::new();
        for (var, ref tyscm) in self {
            new_tyenv.insert(*var, tyscm.substitute(s));
        }
        new_tyenv
    }
}

fn print_type_error(pos: Pos, ty1: &Type, ty2: &Type, tyctx: &mut TyCtx) {
    let mut stderr = io::stderr();
    let msg = format!("Could not unify types '{:?}' and '{:?}' used here:", ty1, ty2);
    writeln!(stderr, "{}",
             tyctx.pm.format_error(pos, &msg, ErrorKind::Error)
    ).unwrap();
    tyctx.have_err = true;
}

fn print_void_error(pos: Pos, tyctx: &mut TyCtx) {
    let mut stderr = io::stderr();
    let msg = "Using uninhabited type 'Void' as r-value here:";
    writeln!(stderr, "{}",
             tyctx.pm.format_error(pos, msg, ErrorKind::Error)
    ).unwrap();
    tyctx.have_err = true;
}

fn print_return_error(pos: Pos, name: &str, tyctx: &mut TyCtx) {
    let mut stderr = io::stderr();
    let msg = format!("Not every branch of '{}' returns a value:", name);
    writeln!(stderr, "{}",
             tyctx.pm.format_error(pos, &*msg, ErrorKind::Error)
    ).unwrap();
    tyctx.have_err = true;
}

/// Computes a most general `Substitution` that unifies the two given
/// types.
pub fn unify_types(ty1: &Type, ty2: &Type, pos: Pos, tyctx: &mut TyCtx) -> Substitution {
    fn unify_types_(ty1: &Type, ty2: &Type, s: &mut Substitution, pos: Pos, tyctx: &mut TyCtx) {
        match (ty1, ty2) {
            (&Var(v1), &Var(v2)) if v1 == v2 => { }
            (&Var(v), ty) if !ty.free_vars().contains(&v) => {
                s.insert(v, (*ty).clone());
            }
            (ty, &Var(v)) if !ty.free_vars().contains(&v) => {
                s.insert(v, (*ty).clone());
            }
            (&Tuple(ref tyl1, ref tyl2), &Tuple(ref tyr1, ref tyr2)) => {
                unify_types_(tyl1, tyr1, s, pos, tyctx);
                let newtyl2 = tyl2.substitute(s);
                let newtyr2 = tyr2.substitute(s);
                unify_types_(&newtyl2, &newtyr2, s, pos, tyctx);
            }
            (&List(ref ty1), &List(ref ty2)) => unify_types_(ty1, ty2, s, pos, tyctx),
            (&Fun(ref argtys1, ref retty1), &Fun(ref argtys2, ref retty2))
                if argtys1.len() == argtys2.len() =>
            {
                for (ty1, ty2) in argtys1.iter().zip(argtys2.iter()) {
                    let newty1 = ty1.substitute(s);
                    let newty2 = ty2.substitute(s);
                    unify_types_(&newty1, &newty2, s, pos, tyctx);
                }

                let new_retty1 = retty1.substitute(s);
                let new_retty2 = retty2.substitute(s);
                unify_types_(&new_retty1, &new_retty2, s, pos, tyctx);
            }
            (&Void, &Void) | (&Int, &Int) | (&Bool, &Bool) | (&Char, &Char) => { }
            _ => { print_type_error(pos, ty1, ty2, tyctx); }
        }
    };
    let mut s = Substitution::new();
    unify_types_(ty1, ty2, &mut s, pos, tyctx);
    s
}

fn infer_type_fieldsel(env: &TypeEnvironment, name: &str, fields: &[ast::Field], ty: &Type, tyctx: &mut TyCtx) -> Substitution {
    // No slice patterns in stable Rust yet :(
    let n = fields.len();
    if n > 0 {
        let (argty, retty) = match fields[n-1] {
            ast::Field::Hd => {
                let tyvar = Var(tyctx.fresh_var());
                let listty = List(Box::new(tyvar.clone()));
                (listty, tyvar)
            }
            ast::Field::Tl => {
                let tyvar = Var(tyctx.fresh_var());
                let listty1 = List(Box::new(tyvar.clone()));
                let listty2 = listty1.clone();
                (listty1, listty2)
            }
            ast::Field::Fst => {
                let tyvar1 = Var(tyctx.fresh_var());
                let tyvar2 = Var(tyctx.fresh_var());
                let tuplety = Tuple(Box::new(tyvar1.clone()), Box::new(tyvar2.clone()));
                (tuplety, tyvar1)
            }
            ast::Field::Snd => {
                let tyvar1 = Var(tyctx.fresh_var());
                let tyvar2 = Var(tyctx.fresh_var());
                let tuplety = Tuple(Box::new(tyvar1.clone()), Box::new(tyvar2.clone()));
                (tuplety, tyvar2)
            }
        };
        let mut s = unify_types(ty, &retty, Pos::default(), tyctx);

        let env = env.substitute(&s);
        let argty = argty.substitute(&s);
        s.compose(infer_type_fieldsel(&env, name, &fields[0..n-1], &argty, tyctx));

        s
    } else {
        if let Some(tyscm) = env.get(&Qualid::var(name)) {
            let varty = tyscm.instantiate(tyctx);
            unify_types(ty, &varty, Pos::default(), tyctx)
        } else {
            panic!("encountered free variable {}, should have been caught by namecheck", name);
        }
    }
}

pub fn infer_type_funcall(env: &TypeEnvironment, fc: &ast::FunCall, ty: &Type, can_void: bool, tyctx: &mut TyCtx) -> Substitution {
    let vars: Vec<Type> = fc.args.iter().map(|_| Var(tyctx.fresh_var())).collect();
    let funty = Fun(vars, Box::new(ty.clone()));

    let mut s;

    // First we check the type of the function in the environment
    if let Some(tyscm) = env.get(&Qualid::fun(&fc.name.id)) {
        let varty = tyscm.instantiate(tyctx);
        s = unify_types(&funty, &varty, fc.pos, tyctx);
    } else {
        panic!("encountered free variable, should have been caught by namecheck");
    }

    // Destruct the dummy function type again
    let (argtys, retty) = match funty {
        Fun(argtys, retty) => (argtys, retty),
        _ => panic!("unexpected non-function type"),
    };

    if !can_void {
        // We check if the return type isn't Void. Note that this is a
        // little hackish. We *should* decorate all expressions in the
        // ast with types, and run over them after we have inferred
        // all types to check for Voids. However, this is the only
        // place we can get a Void, and we also know that if the
        // function returns a Void, we already have that inferred at
        // this point. (I did not formally check this.)
        if let Void = retty.substitute(&s) {
            print_void_error(fc.pos, tyctx);
        }
    }

    // Now infer the type of all arguments
    for (expr, argty) in fc.args.iter().zip(argtys.iter()) {
        let env = env.substitute(&s);
        let argty = argty.substitute(&s);
        s.compose(infer_type_expr(&env, expr, &argty, tyctx));
    }

    // Check for overloading
    if &fc.name.id == "print" {
        let mut overloaded_name = String::from("__");
        overloaded_name.push_str(&fc.name.id);
        for argty in &argtys {
            overloaded_name.push('_');
            overloaded_name.push_str(&argty.substitute(&s).mangle());
        }
        tyctx.overload_funcall(&mut fc.overloaded.borrow_mut(), &overloaded_name);
    } else if &fc.name.id == "isEmpty" {
        tyctx.overload_funcall(&mut fc.overloaded.borrow_mut(), "__isEmpty");
    }

    s
}

pub fn infer_type_expr(env: &TypeEnvironment, expr: &ast::Expr, ty: &Type, tyctx: &mut TyCtx) -> Substitution {
    use ast::ExprEnum as e;
    match expr.expr_enum {
        e::FieldSel(ref fs) => {
            infer_type_fieldsel(env, &fs.var.id, &fs.fields, ty, tyctx)
        }
        e::FunCall(ref fc) => {
            infer_type_funcall(env, fc, ty, false, tyctx)
        }
        e::BinExpr { ref arg1, ref arg2, op, ref overloaded } => {
            use ast::BinOp::*;
            let (argty1, argty2, retty) = match op {
                Or | And => (Bool, Bool, Bool),
                LT | GT | LEq | GEq => (Int, Int, Bool),
                Eq | NEq => {
                    let tyvar = tyctx.fresh_var();
                    (Var(tyvar), Var(tyvar), Bool)
                }
                Cons => {
                    let tyvar = Var(tyctx.fresh_var());
                    let listty1 = List(Box::new(tyvar.clone()));
                    let listty2 = listty1.clone();
                    (tyvar, listty1, listty2)
                }
                Add | Subtract => {
                    // We allow some operations also on characters,
                    // we'll check that after inference.
                    (Var(tyctx.fresh_var()), Var(tyctx.fresh_var()), Var(tyctx.fresh_var()))
                }
                Mult | Divide | Remainder => (Int, Int, Int),
            };
            let mut s = unify_types(ty, &retty, expr.pos, tyctx);

            let env = env.substitute(&s);
            let argty1 = argty1.substitute(&s);
            s.compose(infer_type_expr(&env, arg1, &argty1, tyctx));

            let env = env.substitute(&s);
            let argty2 = argty2.substitute(&s);
            s.compose(infer_type_expr(&env, arg2, &argty2, tyctx));

            // Check the arithmetic operations
            match op {
                Add => {
                    let argty1 = argty1.substitute(&s);
                    let argty2 = argty2.substitute(&s);
                    // We allow:
                    // Int + Int  --> Int
                    // Int + Char --> Char
                    // Char + Int --> Char
                    match (&argty1, &argty2) {
                        (&Int , &Int ) => s.compose(unify_types(&retty, &Int, expr.pos, tyctx)),
                        (&Int , &Char) => s.compose(unify_types(&retty, &Char, expr.pos, tyctx)),
                        (&Char, &Int ) => s.compose(unify_types(&retty, &Char, expr.pos, tyctx)),
                        _ => {
                            // Resort to default
                            s.compose(unify_types(&argty1, &Int, arg1.pos, tyctx));
                            s.compose(unify_types(&argty2, &Int, arg2.pos, tyctx));
                            s.compose(unify_types(&retty, &Int, arg2.pos, tyctx));
                        }
                    }
                }
                Subtract => {
                    let argty1 = argty1.substitute(&s);
                    let argty2 = argty2.substitute(&s);
                    // We allow:
                    // Int  - Int  --> Int
                    // Char - Int  --> Char
                    // Char - Char --> Int
                    match (&argty1, &argty2) {
                        (&Int , &Int ) => s.compose(unify_types(&retty, &Int, expr.pos, tyctx)),
                        (&Char, &Int ) => s.compose(unify_types(&retty, &Char, expr.pos, tyctx)),
                        (&Char, &Char) => s.compose(unify_types(&retty, &Int, expr.pos, tyctx)),
                        _ => {
                            // Resort to default
                            s.compose(unify_types(&argty1, &Int, arg1.pos, tyctx));
                            s.compose(unify_types(&argty2, &Int, arg2.pos, tyctx));
                            s.compose(unify_types(&retty, &Int, arg2.pos, tyctx));
                        }
                    }
                }
                _ => { }
            }

            // Check for overloading of equality
            if op == Eq || op == NEq {
                let mut overloaded_name = String::from("__");
                match op {
                    Eq => overloaded_name.push_str("eq"),
                    NEq => overloaded_name.push_str("neq"),
                    _ => unreachable!(),
                }
                overloaded_name.push('_');
                overloaded_name.push_str(&argty1.substitute(&s).mangle());
                tyctx.overload_funcall(&mut overloaded.borrow_mut(), &overloaded_name);
            }

            s
        }
        e::UnExpr { ref arg, op } => {
            let (argty, retty) = match op {
                ast::UnOp::Neg => (Bool, Bool),
                ast::UnOp::Opp => (Int, Int),
            };
            let mut s = unify_types(ty, &retty, expr.pos, tyctx);

            let env = env.substitute(&s);
            let argty = argty.substitute(&s);
            s.compose(infer_type_expr(&env, arg, &argty, tyctx));

            s
        }
        e::Tuple(ref expr1, ref expr2) => {
            let tyvar1 = Var(tyctx.fresh_var());
            let tyvar2 = Var(tyctx.fresh_var());

            let mut s = unify_types(ty,
                                    &Tuple(Box::new(tyvar1.clone()),
                                           Box::new(tyvar2.clone())),
                                    expr.pos, tyctx);

            let mut env = env.substitute(&s);
            let ty1 = tyvar1.substitute(&s);
            s.compose(infer_type_expr(&env, expr1, &ty1, tyctx));

            env = env.substitute(&s);
            let ty2 = tyvar2.substitute(&s);
            s.compose(infer_type_expr(&env, expr2, &ty2, tyctx));

            s
        }
        e::ListNil => unify_types(ty,
                                  &List(Box::new(Var(tyctx.fresh_var()))),
                                  expr.pos, tyctx),
        e::IntLit(_) => unify_types(ty, &Int, expr.pos, tyctx),
        e::CharLit(_) => unify_types(ty, &Char, expr.pos, tyctx),
        e::True | e::False => unify_types(ty, &Bool, expr.pos, tyctx),
    }
}

pub fn infer_type_stmt(env: &TypeEnvironment, stmt: &ast::Stmt, retty: &Type, tyctx: &mut TyCtx) -> Substitution {
    use ast::StmtEnum::*;
    match stmt.stmt_enum {
        If { ref cond, ref true_branch, ref false_branch } => {
            let mut s = infer_type_expr(env, cond, &Bool, tyctx);

            let old_seen_ret = tyctx.seen_ret;

            let env = env.substitute(&s);
            let retty = retty.substitute(&s);
            s.compose(infer_type_block(&env, true_branch, &retty, tyctx));

            if tyctx.seen_ret == SeenReturn::Full && old_seen_ret != SeenReturn::Full {
                // we have encountered a non-void return, let's first
                // look at the other branch before claiming we have a
                // full return
                tyctx.seen_ret = SeenReturn::Partial;
            }

            let retty = retty.substitute(&s);
            let env = env.substitute(&s);
            s.compose(infer_type_block(&env, false_branch, &retty, tyctx));

            s
        }
        While { ref cond, ref stmts } => {
            let mut s = infer_type_expr(env, cond, &Bool, tyctx);

            let env = env.substitute(&s);
            let retty = retty.substitute(&s);
            s.compose(infer_type_block(&env, stmts, &retty, tyctx));

            s
        }
        Assignment { ref fieldsel, ref expr } => {
            // First infer type of LHS
            let tyvar = Var(tyctx.fresh_var());
            let mut s = infer_type_fieldsel(env, &fieldsel.var.id, &fieldsel.fields, &tyvar, tyctx);

            // Then infer type of RHS
            let env = env.substitute(&s);
            let tyvar = tyvar.substitute(&s);
            s.compose(infer_type_expr(&env, expr, &tyvar, tyctx));

            s
        }
        FunCall(ref fc) => {
            infer_type_funcall(env, fc, &Var(tyctx.fresh_var()), true, tyctx)
        }
        Return(Some(ref expr)) => {
            tyctx.seen_ret = SeenReturn::Full;
            infer_type_expr(env, expr, retty, tyctx)
        }
        Return(None) => {
            unify_types(retty, &Void, stmt.pos, tyctx)
        }
    }
}

pub fn infer_type_block(env: &TypeEnvironment, stmts: &[ast::Stmt], retty: &Type, tyctx: &mut TyCtx) -> Substitution {
    let mut s = Substitution::new();
    for stmt in stmts {
        let ref env = env.substitute(&s);
        let ref retty = retty.substitute(&s);
        s.compose(infer_type_stmt(env, stmt, retty, tyctx));
    }
    s
}

pub fn infer_type_fundecl(env: &TypeEnvironment, fd: &mut ast::FunDecl, ty: &Type, tyctx: &mut TyCtx) -> Substitution {
    let funty = fd.the_type
        .as_ref().map(|ty| Type::from_ast_funtype(ty, tyctx))
        .unwrap_or_else(|| {
            let arg_tyvars = fd.arg_names.iter().map(|_| Var(tyctx.fresh_var())).collect();
            let ret_tyvar = Box::new(Var(tyctx.fresh_var()));
            Fun(arg_tyvars, ret_tyvar)
        });

    // First unify the type constraint with the function type
    let mut s = unify_types(ty, &funty, fd.pos, tyctx);

    // Destruct the fun type into its argument types and return type
    let (argtys, retty) = match funty {
        Fun(argtys, retty) => (argtys, retty),
        _ => panic!("unexpected non-function type"),
    };
    assert_eq!(argtys.len(), fd.arg_names.len());

    // Add all argument types to the environment
    let mut env = env.substitute(&s);
    for (argty, argname) in argtys.iter().zip(fd.arg_names.iter()) {
        let newty = argty.substitute(&s);
        env.insert(Qualid::var(&argname.id), TypeScheme::promote(newty));
    }

    for vdecl in &mut fd.var_decls {
        let varty = vdecl.the_type
            .as_ref().map(|ty| Type::from_ast_type(ty, tyctx))
            .unwrap_or_else(|| Var(tyctx.fresh_var()));

        // First infer type of expression
        s.compose(infer_type_expr(&env, &vdecl.expr, &varty, tyctx));

        // Then add the type to the env (without generalizing)
        env = env.substitute(&s);
        let ty = varty.substitute(&s);
        env.insert(Qualid::var(&vdecl.name.id), TypeScheme::promote(ty.clone()));

        // Decorate the ast with the type
        vdecl.the_type = Some(ty.to_ast_type());
    }

    // Infer type of function body
    let new_retty = retty.substitute(&s);
    tyctx.seen_ret = SeenReturn::No;
    s.compose(infer_type_block(&env, &fd.stmts, &new_retty, tyctx));

    // Let's look if the returns are okay
    match tyctx.seen_ret {
        SeenReturn::No => {
            // No problem, let's make the return type Void
            s.compose(unify_types(&new_retty, &Void, fd.name.pos, tyctx));
        }
        SeenReturn::Partial => {
            // This is a problem!
            print_return_error(fd.name.pos, &*fd.name.id, tyctx);
        }
        SeenReturn::Full => { } // Everything okay! :)
    }

    s
}

type FunctionTypeSchemes = HashMap<String, TypeScheme>;
type ASTSubstitution = HashMap<String, ast::Type>;

fn infer_type_program(prog: &mut ast::Program, pm: &PosMap)
                      -> Option<(FunctionTypeSchemes, ASTSubstitution)> {
    // First construct a call graph, condense the strongly connected
    // components, and construct a topological ordering.  We must do
    // this now, since later the mutable borrow on prog will leak into
    // local variables.
    let comps: Vec<Vec<String>> = {
        let graph = call_graph(prog);
        let condensed_graph = condensation(graph, true);
        let sorted = toposort(&condensed_graph);
        sorted.into_iter()
            .rev()
            .map(|i| condensed_graph[i].iter()
                 .cloned()
                 .map(String::from)
                 .collect())
            .collect()
    };

    let mut tyctx = TyCtx::new(pm);
    let mut env = TypeEnvironment::new();
    let mut s = Substitution::new();

    add_builtins(&mut env, &mut tyctx);

    // Then we infer all types of global variable declarations
    for vdecl in &mut prog.var_decls {
        let varty = vdecl.the_type
            .as_ref().map(|ty| Type::from_ast_type(ty, &mut tyctx))
            .unwrap_or_else(|| Var(tyctx.fresh_var()));

        // First infer type of expression
        s.compose(infer_type_expr(&env, &vdecl.expr, &varty, &mut tyctx));

        // Then add the type to the env (without generalizing)
        env = env.substitute(&s);
        let ty = varty.substitute(&s);
        env.insert(Qualid::var(&vdecl.name.id), TypeScheme::promote(ty.clone()));

        // Decorate the ast with the type
        vdecl.the_type = Some(ty.to_ast_type());
    }

    // Now infer the types for function declarations, respecting the order and mutual recursion
    for comp in &comps {
        let mut env2 = env.clone();
        // First generate some fresh variables for all functions in this component
        for fun in comp {
            let tyvar = Var(tyctx.fresh_var());
            env2.insert(Qualid::fun(&**fun), TypeScheme::promote(tyvar));
        }
        // Infer the types of the functions in this component
        for fun in comp {
            let fdecl = prog.fun_decls.iter_mut().find(|fd| *fd.name.id == **fun).unwrap();
            {
                let tyvar = &env2[&Qualid::fun(&**fun)].inner;
                s.compose(infer_type_fundecl(&env2, fdecl, tyvar, &mut tyctx));
            }
            env2 = env2.substitute(&s);
        }
        // Generalize the types and add to the environment
        for fun in comp {
            let ref funty = env2[&Qualid::fun(&**fun)].inner;
            let funtyc = funty.closure(&env);
            env.insert(Qualid::fun(&**fun), funtyc);

            // Decorate the ast with this type
            let fdecl = prog.fun_decls.iter_mut().find(|fd| *fd.name.id == **fun).unwrap();
            fdecl.the_type = Some(funty.to_ast_funtype());
        }
    }

    if tyctx.have_err {
        None
    } else {
        // We are interested in the type schemes of functions
        // (variables are never generalized). To lose the references
        // to the ast, we convert the &str's to owned strings.
        let env = env.into_iter()
            .filter(|&(qid, _)| qid.is_fun())
            .map(|(qid, tyscm)| (String::from(qid.unwrap_fun()), tyscm))
            .collect::<FunctionTypeSchemes>();
        Some((env, ast_subst_from_subst(&s)))
    }
}

/// Infer and check all types of the program and return an ast with
/// all types annotated.
pub fn infer_types(prog: &mut ast::Program, pm: &PosMap) -> bool {
    match infer_type_program(prog, pm) {
        None => true,
        Some((env, s)) => {
            substitute_program(prog, &env, &s);
            false
        }
    }
}

fn ast_subst_from_subst(s: &Substitution) -> ASTSubstitution {
    let mut new_s = ASTSubstitution::new();
    for (v, ty) in s {
        if let Void = *ty { continue; }
        if !ty.is_fun() {
            new_s.insert(format!("_{}", v), ty.to_ast_type());
        }
    }
    new_s
}

fn substitute_ast_type(ast_type: &mut ast::Type, s: &ASTSubstitution) {
    use ast::TypeEnum as t;
    let mut new_type = None;
    match ast_type.type_enum {
        t::Basic(_) => { }
        t::Tuple(ref mut tyl, ref mut tyr) => {
            substitute_ast_type(&mut *tyl, s);
            substitute_ast_type(&mut *tyr, s);
        }
        t::List(ref mut ty) => {
            substitute_ast_type(&mut *ty, s);
        }
        t::Parm(ref parm) => {
            // borrowck workaround :(
            // We can't assign to ast_type, since parm borrows from it.
            if let Some(ty) = s.get(&parm.id) {
                new_type = Some(ty.clone());
            }
        }
    }

    if let Some(ty) = new_type { *ast_type = ty; }
}

fn substitute_ast_funtype(ast_funtype: &mut ast::FunType, s: &ASTSubstitution) {
    for arg_type in &mut ast_funtype.arg_types {
        substitute_ast_type(arg_type, s);
    }
    if let Some(ref mut ret_type) = ast_funtype.ret_type {
        substitute_ast_type(ret_type, s);
    }
}

/// Substitutes all type variables still occurring with the correct types.
fn substitute_program(prog: &mut ast::Program, env: &FunctionTypeSchemes, s: &ASTSubstitution) {
    for vdecl in &mut prog.var_decls {
        if let Some(ref mut the_type) = vdecl.the_type {
            substitute_ast_type(the_type, s);
        }
    }

    for fdecl in &mut prog.fun_decls {
        // First lookup the type scheme of this function
        let tyscm = env.get(&fdecl.name.id)
            .expect(&format!("type scheme of {} not in envorinment", &fdecl.name.id));

        // Generate some fancy type variable names ("a", "b", etc.)
        // TODO: reuse user's names
        let typarms = {
            // We use some machinery to easily generate type parameters
            struct VarGen(u32);
            impl VarGen {
                fn gen(&mut self) -> String {
                    let c = char::from_u32('a' as u32 + self.0 % 26).unwrap();
                    let lvl = self.0 / 26;
                    self.0 += 1;
                    if lvl == 0 {
                        format!("{}", c)
                    } else {
                        format!("{}{}", c, lvl)
                    }
                }
            }

            let mut vg = VarGen(0);
            let mut sortedvars = tyscm.vars.iter()
                .cloned() // convert &TyVar to TyVar
                .collect::<Vec<_>>();
            sortedvars.sort();
            sortedvars.into_iter()
                .map(|v| (v, vg.gen()))
                .collect::<HashMap<_, _>>()
        };

        // Extend the ASTSubstitution with these parameters
        let mut s = s.clone();
        s.extend(
            typarms.into_iter()
                .map(|(v, parm)|
                     (format!("_{}", v),
                      ast::Type {
                          type_enum: ast::TypeEnum::Parm(
                              ast::Ident { id: parm, pos: Pos::default() }
                          ),
                          pos: Pos::default(),
                      })));

        // Now use this to substitute stuff in the function type and body
        if let Some(ref mut the_type) = fdecl.the_type {
            substitute_ast_funtype(the_type, &s);
            substitute_ast_funtype(the_type, &s);
        }
        for vdecl in &mut fdecl.var_decls {
            if let Some(ref mut the_type) = vdecl.the_type {
                // Need two of these since we didn't make the effort
                // to properly compose the maps.
                substitute_ast_type(the_type, &s);
                substitute_ast_type(the_type, &s);
            }
        }
    }
}
