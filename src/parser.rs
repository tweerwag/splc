//! A hand written recursive descent parser for the SPL grammar.
//!
//! The parser is designed such that it doesn't need
//! backtracking. Conflicts are resolved by looking ahead and by
//! careful analyzation of the grammar. For example, if a variable
//! declaration starts with an identifier, the second token also needs
//! to be an identifier (since the corresponding rule is "Type id",
//! where Type can resolve itself to id). With this knowledge, we can
//! differentiate between a variable declaration and a statement,
//! since if a statement starts with an identifier, the second token
//! needs to be an Assign token ('=') or a opening parenthesis (as
//! part of a function call).
//!
//! The implementation itself borrows ideas from monadic parsing. The
//! parsing functions return a Haskell-like result type (which is also
//! standard in Rust) with either the parsed ast, or an error. The
//! flexible macro system of Rust is used to emulate Haskell's
//! do-notation, where errors are propagated. Although this results in
//! a lot of nested closures, the Rust compiler and underlying LLVM
//! are smart enough to optimize and inline most of them.
//!
//! Besides the do-notation, used to parse a sequence of
//! (non-)terminals, an important construct is the biased or. If the
//! biased or receives a soft error, it disregards it and tries the
//! next branch. The last branch is always returned as-is of
//! course. Parse and lexer errors are also propagated
//! immediately. Often a choice can be made depending on what keyword
//! the first token is. In those cases it is safe to "soften" those
//! errors since `parse_keyword` doesn't consume other tokens than the
//! requested token. This softening is done manually, after careful
//! analyzing the grammar.
//!
//! Finally, I thought adding positions (line and column number in the
//! source code) to tokens and the abstract syntax tree would be
//! easy. However, it ended up a bit hackish. Luckily, the reported
//! errors seem to make a lot more sense than I thought.

use ast::*;
use lexer::*;
use posmap::{Pos, GetPos};
use peeker::Peeker;
use either::*;
use std::cell::RefCell;

#[derive(Debug, PartialEq)]
pub enum ParseError {
    /// Indicates a soft error from which the parser can recover. This
    /// is used for the choice (`parse_or!`) function.
    SoftErr,
    /// Indicates an unrecoverable parse error (but can be softened).
    ParseErr(String, Pos),
    /// Used to propagate lexer errors.
    LexErr(LexError),
}
use parser::ParseError::*;

pub type ParseResult<T> = Result<T, ParseError>;

/// A handy function to soften a `ParseErr`. Since `ParseResult` is a
/// type synonym, and `Result` is not defined in this crate, we need
/// to implement this through a trait.
trait Soften<T> {
    fn soften(self) -> Self;
}

impl<T> Soften<T> for ParseResult<T> {
    fn soften(self) -> ParseResult<T> {
        if let Err(ParseErr(_, _)) = self {
            Err(SoftErr)
        } else {
            self
        }
    }
}

/// Trait alias for `Iterator<Item=LexResult<TokenAndPos>>`.
pub trait TokenIter: Iterator<Item=LexResult<TokenAndPos>> { }
impl<T: Iterator<Item=LexResult<TokenAndPos>>> TokenIter for T { }

pub struct Parser<I> where I: TokenIter + GetPos {
    inp: Peeker<I>,
}

/// Emulates the do-notation of Haskell for the monadic
/// `ParseResult<_>`.
///
/// - `x <- parse_something();` binds the result of `parse_something()`
/// to `x`. In this form `x` must be an identifier.
///
/// - `pat (x, y) = parse_something();` is a special form of the
/// previous one, since `<-` is not allowed after patterns.
///
/// - `parse_something();` just throws away the result of
/// `parse_something()`.
///
/// - `return expr` wraps the `expr` in an `Ok`. It is the only final
/// statement.
macro_rules! parse_do {
    ( $x:ident <- $e:expr ; $( $rem:tt )* ) => {
        $e.and_then(|$x| parse_do! { $( $rem )* });
    };
    ( pat $p:pat = $e:expr ; $( $rem:tt )* ) => {
        $e.and_then(|$p| parse_do! { $( $rem )* });
    };
    ( $e:expr ; $( $rem:tt )* ) => {
        $e.and_then(|_| parse_do! { $( $rem )* });
    };
    ( return $e:expr ) => {
        Ok($e)
    };
}

/// Tries every argument in order. Lexer errors are propagated. Parse
/// errors are ignored, unless the argument is the last, which is
/// returned untouched.
macro_rules! parse_or {
    ( $e:expr ; $( $rem:tt )* ) => {
        match $e {
            Err(SoftErr) => parse_or! { $( $rem )* },
            Err(e) => Err(e),
            Ok(x) => Ok(x),
        }
    };
    ( $e:expr ) => { $e };
}

/// Parses rules of the form `($delim $e)*`. Note that `$delim` may
/// only produce a `ParseErr` without touching the input.
macro_rules! parse_star_keep {
    ( $delim:expr , $e:expr ) => {{
        let mut acc = vec![];
        let mut err = Ok(true);
        while let Ok(true) = err {
            err = parse_or! {
                $delim.and_then(|x| $e.map(|y| acc.push((x, y)))).map(|_| true);
                Ok(false)
            };
        }
        match err {
            Ok(false) => Ok(acc),
            Ok(true) => unreachable!(),
            Err(e) => Err(e),
        }
    }};
}

/// Parses the same as `parse_star_keep!`, but throws away the value
/// of the delimiting parser.
macro_rules! parse_star {
    ( $delim:expr, $e:expr ) => {
        parse_star_keep!($delim, $e)
            .map(|xs| xs.into_iter().map(|(_, y)| y).collect::<Vec<_>>())
    };
}

/// Parses rules of the form `$begin [$e ($sep $e)*] $end`. Note that
/// `$end` may only produce a `ParseErr` without touching the input.
macro_rules! parse_star_list_delim {
    ( $begin:expr , $e:expr , $sep:expr , $end:expr ) => {
        $begin.and_then(|_| {
            let mut acc = vec![];
            let mut err;
            {
                err = parse_or! {
                    $end.map(|_| false);
                    $e.map(|x| { acc.push(x); true })
                };
                while let Ok(true) = err {
                    err = parse_or! {
                        $end.map(|_| false);
                        $sep.and_then(|_| $e.map(|x| { acc.push(x); true }))
                    };
                }
            }
            match err {
                Ok(false) => Ok(acc),
                Ok(true) => unreachable!(),
                Err(e) => Err(e),
            }
        })
    };
}

/// Parses rules of the form `$begin $e* $end`. Note that `$end` may
/// only produce a `ParseErr` without touching the input.
macro_rules! parse_star_delim {
    ( $begin:expr , $e:expr , $end:expr ) => {
        parse_star_list_delim! { $begin, $e, Ok(()), $end }
    };
}

/// Parse a left associative infix operator expression.
macro_rules! parse_infixl {
    ( $higher:expr , $ops:expr , $( $p:pat => $e:expr , )+ ) => {
        parse_do! {
            expr1 <- $higher;
            exprs <- parse_star_keep! {
                $ops, $higher
            };
            expr <- {
                let mut expr_l = expr1;
                for ((op, _), expr_r) in exprs {
                    match op {
                        $( $p => expr_l = Expr {
                            pos: expr_l.pos + expr_r.pos,
                            expr_enum: ExprEnum::BinExpr {
                                arg1: Box::new(expr_l),
                                arg2: Box::new(expr_r),
                                op: $e,
                                overloaded: RefCell::new(None),
                            },
                        },)+
                        _ => unreachable!(),
                    };
                }
                Ok(expr_l)
            };
            return expr
        }
    };
}

impl<I> Parser<I> where I: TokenIter + GetPos {
    /// Construct a new `Parser` from an `Iterator` over
    /// `LexResult<TokenAndPos>`s.
    pub fn new(iter: I) -> Parser<I> {
        Parser { inp: Peeker::new(iter) }
    }

    /// Indicates whether the token stream has dried up.
    fn end_of_stream(&mut self) -> bool {
        match self.inp.peek(0) {
            None => true,
            _ => false,
        }
    }

    /// Check whether there is a specific symbol at the specified position
    /// in the token stream.
    fn symb_at_pos(&mut self, symb1: Symb, pos: usize) -> bool {
        if let Some(&Ok(ref tokpos)) = self.inp.peek(pos) {
            match tokpos.token {
                Token::Symb(ref symb2) if symb2 == &symb1 => true,
                _ => false,
            }
        } else {
            false
        }
    }

    /// Check whether there is an identifier at the specified position
    /// in the token stream.
    fn ident_at_pos(&mut self, pos: usize) -> bool {
        if let Some(&Ok(ref tokpos)) = self.inp.peek(pos) {
            match tokpos.token {
                Token::Ident(_) => true,
                _ => false,
            }
        } else {
            false
        }
    }

    /// Peek at the input to see if there is an identifier in
    /// front. In that case, pull it from the stream and return the
    /// identifier.
    fn parse_ident(&mut self) -> ParseResult<Ident> {
        match self.inp.peek(0) {
            Some(&Ok(ref tokpos)) => {
                match tokpos.token {
                    Token::Ident(_) => { }
                    _ => return Err(ParseErr("Expected identifier".to_string(), tokpos.pos)),
                }
            }
            Some(&Err(_)) => { },
            None => return Err(ParseErr("Expected identifier".to_string(), Pos::default())),
        }
        match self.inp.next() {
            Some(Ok(tokpos)) => {
                match tokpos.token {
                    Token::Ident(ident) => Ok(Ident { id: ident, pos: tokpos.pos }),
                    _ => unreachable!(),
                }
            }
            Some(Err(e)) => Err(LexErr(e)),
            None => unreachable!(),
        }
    }

    /// Peek at the input to see if there is an integer literal in
    /// front. In that case, pull it from the stream and return the
    /// integer.
    fn parse_integer_literal(&mut self) -> ParseResult<(u32, Pos)> {
        match self.inp.peek(0) {
            Some(&Ok(ref tokpos)) => {
                match tokpos.token {
                    Token::IntLit(_) => { },
                    _ => return Err(ParseErr("Expected integer literal".to_string(), tokpos.pos)),
                }
            }
            Some(&Err(_)) => { }
            None => return Err(ParseErr("Expected integer literal".to_string(), Pos::default())),
        }
        match self.inp.next() {
            Some(Ok(tokpos)) => {
                match tokpos.token {
                    Token::IntLit(n) => Ok((n, tokpos.pos)),
                    _ => unreachable!(),
                }
            }
            Some(Err(e)) => Err(LexErr(e)),
            None => unreachable!(),
        }
    }

    /// Peek at the input to see if there is an character literal in
    /// front. In that case, pull it from the stream and return the
    /// character.
    fn parse_character_literal(&mut self) -> ParseResult<(char, Pos)> {
        match self.inp.peek(0) {
            Some(&Ok(ref tokpos)) => {
                match tokpos.token {
                    Token::CharLit(_) => { },
                    _ => return Err(ParseErr("Expected character literal".to_string(), tokpos.pos)),
                }
            }
            Some(&Err(_)) => { }
            None => return Err(ParseErr("Expected character literal".to_string(), Pos::default())),
        }
        match self.inp.next() {
            Some(Ok(tokpos)) => {
                match tokpos.token {
                    Token::CharLit(c) => Ok((c, tokpos.pos)),
                    _ => unreachable!(),
                }
            }
            Some(Err(e)) => Err(LexErr(e)),
            None => unreachable!(),
        }
    }

    /// Peek at the input to see if there is a specific symbol in
    /// front. In that case, pull it from the stream.
    fn parse_symbs(&mut self, symbs: &[Symb]) -> ParseResult<(Symb, Pos)> {
        match self.inp.peek(0) {
            Some(&Ok(ref tokpos)) => {
                match tokpos.token {
                    Token::Symb(ref symb) if symbs.contains(symb) => { },
                    _ => return Err(ParseErr(format!("Expected symbols {}", DisplaySymbs(symbs)), tokpos.pos)),
                }
            }
            Some(&Err(_)) => { }
            None => return Err(ParseErr(format!("Expected symbols {}", DisplaySymbs(symbs)), Pos::default())),
        }
        match self.inp.next() {
            Some(Ok(tokpos)) => {
                match tokpos.token {
                    Token::Symb(symb) => Ok((symb, tokpos.pos)),
                    _ => unreachable!(),
                }
            }
            Some(Err(e)) => Err(LexErr(e)),
            None => unreachable!(),
        }
    }

    fn parse_symb(&mut self, symb: Symb) -> ParseResult<Pos> {
        self.parse_symbs(&[symb]).map(|(_, pos)| pos)
    }

    /// Peek at the input to see if there is a specific keyword in
    /// front. In that case, pull it from the stream and return the
    /// keyword.
    fn parse_keywords(&mut self, kws: &[Keyword]) -> ParseResult<(Keyword, Pos)> {
        match self.inp.peek(0) {
            Some(&Ok(ref tokpos)) => {
                match tokpos.token {
                    Token::Keyword(ref kw) if kws.contains(kw) => { }
                    _ => return Err(ParseErr(format!("Expected keywords {}", DisplayKws(kws)), tokpos.pos)),
                }
            }
            Some(&Err(_)) => { }
            None => return Err(ParseErr(format!("Expected keywords {}", DisplayKws(kws)), Pos::default())),
        }
        match self.inp.next() {
            Some(Ok(tokpos)) => {
                match tokpos.token {
                    Token::Keyword(kw) => Ok((kw, tokpos.pos)),
                    _ => unreachable!(),
                }
            }
            Some(Err(e)) => Err(LexErr(e)),
            None => unreachable!(),
        }
    }

    fn parse_keyword(&mut self, kw: Keyword) -> ParseResult<Pos> {
        self.parse_keywords(&[kw]).map(|(_, pos)| pos)
    }

    /// Parse a basic type.
    pub fn parse_basic_type(&mut self) -> ParseResult<(BasicType, Pos)> {
        self.parse_keywords(&[Keyword::Int, Keyword::Bool, Keyword::Char])
            .map(|(kw, pos)| (match kw {
                Keyword::Int => BasicType::Int,
                Keyword::Bool => BasicType::Bool,
                Keyword::Char => BasicType::Char,
                _ => unreachable!(),
            }, pos))
            .soften()
    }

    /// Parse a type.
    pub fn parse_type(&mut self) -> ParseResult<Type> {
        let pos = self.inp.pos();
        parse_or! {
            self.parse_basic_type()
                .map(|(bty, pos)| Type {
                    type_enum: TypeEnum::Basic(bty),
                    pos: pos,
                });
            self.parse_ident()
                .map(|ident| Type {
                    pos: ident.pos,
                    type_enum: TypeEnum::Parm(ident),
                })
                .soften();
            parse_do! {
                posb <- self.parse_symb(Symb::LParen).soften();
                ty1 <- self.parse_type();
                self.parse_symb(Symb::Comma);
                ty2 <- self.parse_type();
                pose <- self.parse_symb(Symb::RParen);
                return Type {
                    type_enum: TypeEnum::Tuple(Box::new(ty1), Box::new(ty2)),
                    pos: posb + pose,
                }
            };
            parse_do! {
                posb <- self.parse_symb(Symb::LBracket).soften();
                ty <- self.parse_type();
                pose <- self.parse_symb(Symb::RBracket);
                return Type {
                    type_enum: TypeEnum::List(Box::new(ty)),
                    pos: posb + pose,
                }
            };
            Err(ParseErr("Expected a type".to_string(), pos))
        }
    }

    /// Parse a field selector. Returns a parse error without
    /// consuming anything if the first token is not an identifier.
    pub fn parse_field_selector(&mut self) -> ParseResult<FieldSel> {
        let pos = self.inp.pos();
        parse_do! {
            ident <- self.parse_ident().soften();
            fields <- parse_star! {
                self.parse_symb(Symb::Dot).soften(),
                self.parse_keywords(&[Keyword::Hd, Keyword::Tl, Keyword::Fst, Keyword::Snd])
                    .map(|(kw, _)| match kw {
                        Keyword::Hd => Field::Hd,
                        Keyword::Tl => Field::Tl,
                        Keyword::Fst => Field::Fst,
                        Keyword::Snd => Field::Snd,
                        _ => unreachable!(),
                    })
            };
            return FieldSel {
                pos: pos + self.inp.pos(),
                var: ident,
                fields: fields,
            }
        }
    }

    /// Parse a function call. Returns a parse error without consuming
    /// anything if the second token in the stream is not an opening
    /// parenthesis.
    pub fn parse_function_call(&mut self) -> ParseResult<FunCall> {
        if !self.symb_at_pos(Symb::LParen, 1) {
            return Err(SoftErr);
        }

        let pos = self.inp.pos();

        parse_do! {
            name <- self.parse_ident().soften();
            args <- parse_star_list_delim! {
                self.parse_symb(Symb::LParen),
                self.parse_or_expression(),
                self.parse_symb(Symb::Comma),
                self.parse_symb(Symb::RParen).soften()
            };
            return FunCall {
                pos: pos + self.inp.pos(),
                name: name,
                args: args,
                overloaded: RefCell::new(None),
            }
        }
    }

    pub fn parse_or_expression(&mut self) -> ParseResult<Expr> {
        parse_infixl! {
            self.parse_and_expression(),
            self.parse_symbs(&[Symb::Or]).soften(),
            Symb::Or => BinOp::Or,
        }
    }

    pub fn parse_and_expression(&mut self) -> ParseResult<Expr> {
        parse_infixl! {
            self.parse_relation_expression(),
            self.parse_symbs(&[Symb::And]).soften(),
            Symb::And => BinOp::And,
        }
    }

    pub fn parse_relation_expression(&mut self) -> ParseResult<Expr> {
        parse_infixl! {
            self.parse_cons_expression(),
            self.parse_symbs(&[Symb::Eq, Symb::LT, Symb::GT, Symb::LEq, Symb::GEq, Symb::NEq]).soften(),
            Symb::Eq => BinOp::Eq,
            Symb::LT => BinOp::LT,
            Symb::GT => BinOp::GT,
            Symb::LEq => BinOp::LEq,
            Symb::GEq => BinOp::GEq,
            Symb::NEq => BinOp::NEq,
        }
    }

    // Note this function is recursive to accommodate the right associativity of cons
    pub fn parse_cons_expression(&mut self) -> ParseResult<Expr> {
        let pos = self.inp.pos();
        parse_do! {
            expr_l <- self.parse_add_expression();
            expr_maybe <- parse_or! {
                parse_do! {
                    self.parse_symb(Symb::Colon).soften();
                    expr_r <- self.parse_cons_expression();
                    return Some(expr_r)
                };
                Ok(None)
            };
            return match expr_maybe {
                Some(expr_r) => Expr {
                    expr_enum: ExprEnum::BinExpr {
                        arg1: Box::new(expr_l),
                        arg2: Box::new(expr_r),
                        op: BinOp::Cons,
                        overloaded: RefCell::new(None),
                    },
                    pos: pos + self.inp.pos(),
                },
                None => expr_l
            }
        }
    }

    pub fn parse_add_expression(&mut self) -> ParseResult<Expr> {
        parse_infixl! {
            self.parse_mul_expression(),
            self.parse_symbs(&[Symb::Plus, Symb::Minus]).soften(),
            Symb::Plus => BinOp::Add,
            Symb::Minus => BinOp::Subtract,
        }
    }

    pub fn parse_mul_expression(&mut self) -> ParseResult<Expr> {
        parse_infixl! {
            self.parse_unary_expression(),
            self.parse_symbs(&[Symb::Mult, Symb::Divide, Symb::Remainder]).soften(),
            Symb::Mult => BinOp::Mult,
            Symb::Divide => BinOp::Divide,
            Symb::Remainder => BinOp::Remainder,
        }
    }

    pub fn parse_unary_expression(&mut self) -> ParseResult<Expr> {
        let pos = self.inp.pos();
        parse_or! {
            parse_do! {
                op <- self.parse_symbs(&[Symb::Not, Symb::Minus])
                    .map(|(symb, _)| match symb {
                        Symb::Not => UnOp::Neg,
                        Symb::Minus => UnOp::Opp,
                        _ => unreachable!(),
                    })
                    .soften();
                expr <- self.parse_unary_expression();
                return Expr {
                    expr_enum: ExprEnum::UnExpr { arg: Box::new(expr), op: op },
                    pos: pos + self.inp.pos(),
                }
            };
            self.parse_primary_expression()
        }
    }

    /// Parse a primary expression. Primary expressions are the
    /// building blocks of expressions. They comprise literals,
    /// variables, and other expressions enclosed in parentheses.
    pub fn parse_primary_expression(&mut self) -> ParseResult<Expr> {
        let pos = self.inp.pos();
        parse_or! {
            // First try a function call to solve the ident/ident-conflict
            self.parse_function_call()
                .map(|fc| Expr {
                    pos: fc.pos,
                    expr_enum: ExprEnum::FunCall(fc),
                });
            self.parse_field_selector()
                .map(|fs| Expr {
                    pos: fs.pos,
                    expr_enum: ExprEnum::FieldSel(fs),
                });
            self.parse_integer_literal()
                .map(|(n, litpos)| Expr {
                    pos: litpos,
                    expr_enum: ExprEnum::IntLit(n),
                })
                .soften();
            self.parse_character_literal()
                .map(|(c, litpos)| Expr {
                    pos: litpos,
                    expr_enum: ExprEnum::CharLit(c),
                })
                .soften();
            parse_do! {
                self.parse_symb(Symb::LParen).soften();
                // Could be a tuple expression or a normal expression in parens
                expr1 <- self.parse_or_expression();
                maybe_expr2 <- parse_or! {
                    self.parse_symb(Symb::RParen).map(|_| None).soften();
                    parse_do! {
                        self.parse_symb(Symb::Comma);
                        e <- self.parse_or_expression();
                        self.parse_symb(Symb::RParen);
                        return Some(e)
                    }
                };
                return match maybe_expr2 {
                    Some(expr2) => Expr {
                        expr_enum: ExprEnum::Tuple(Box::new(expr1), Box::new(expr2)),
                        pos: pos + self.inp.pos(),
                    },
                    None => expr1,
                }
            };
            parse_do! {
                self.parse_symb(Symb::LBracket).soften();
                self.parse_symb(Symb::RBracket);
                return Expr {
                    expr_enum: ExprEnum::ListNil,
                    pos: pos + self.inp.pos(),
                }
            };
            self.parse_keyword(Keyword::True)
                .map(|pos| Expr { expr_enum: ExprEnum::True, pos: pos })
                .soften();
            self.parse_keyword(Keyword::False)
                .map(|pos| Expr { expr_enum: ExprEnum::False, pos: pos })
                .soften();
            Err(ParseErr("Expected a primary expression".to_string(), pos))
        }
    }

    pub fn parse_statement(&mut self) -> ParseResult<Stmt> {
        let pos = self.inp.pos();
        parse_or! {
            parse_do! {
                self.parse_keyword(Keyword::If).soften();
                self.parse_symb(Symb::LParen);
                expr <- self.parse_or_expression();
                self.parse_symb(Symb::RParen);
                true_branch <- self.parse_block();
                false_branch <- parse_or! {
                    parse_do! {
                        self.parse_keyword(Keyword::Else).soften();
                        b <- self.parse_block();
                        return b
                    };
                    Ok(vec![])
                };
                return Stmt {
                    stmt_enum: StmtEnum::If {
                        cond: expr,
                        true_branch: true_branch,
                        false_branch: false_branch,
                    },
                    pos: pos + self.inp.pos(),
                }
            };
            parse_do! {
                self.parse_keyword(Keyword::While).soften();
                self.parse_symb(Symb::LParen);
                expr <- self.parse_or_expression();
                self.parse_symb(Symb::RParen);
                stmts <- self.parse_block();
                return Stmt {
                    stmt_enum: StmtEnum::While { cond: expr, stmts: stmts },
                    pos: pos + self.inp.pos(),
                }
            };
            // First try a function call to solve the ident/ident-conflict
            parse_do! {
                fc <- self.parse_function_call();
                self.parse_symb(Symb::Semicolon);
                return Stmt {
                    stmt_enum: StmtEnum::FunCall(fc),
                    pos: pos + self.inp.pos(),
                }
            };
            parse_do! {
                fs <- self.parse_field_selector();
                self.parse_symb(Symb::Assign);
                expr <- self.parse_or_expression();
                self.parse_symb(Symb::Semicolon);
                return Stmt {
                    stmt_enum: StmtEnum::Assignment { fieldsel: fs, expr: expr },
                    pos: pos + self.inp.pos(),
                }
            };
            parse_do! {
                self.parse_keyword(Keyword::Return).soften();
                expr <- parse_or! {
                    self.parse_symb(Symb::Semicolon).map(|_| None).soften();
                    parse_do! {
                        expr <- self.parse_or_expression();
                        self.parse_symb(Symb::Semicolon);
                        return Some(expr)
                    }
                };
                return Stmt {
                    stmt_enum: StmtEnum::Return(expr),
                    pos: pos + self.inp.pos(),
                }
            };
            Err(ParseErr("Expected a statement".to_string(), pos))
        }
    }

    pub fn parse_block(&mut self) -> ParseResult<Vec<Stmt>> {
        parse_star_delim! {
            self.parse_symb(Symb::LBrace),
            self.parse_statement(),
            self.parse_symb(Symb::RBrace).soften()
        }
    }

    pub fn parse_var_declaration(&mut self) -> ParseResult<VarDecl> {
        // Note: if the type is an identifier, then there should be an
        // identifier immediately after it.  We can use this to
        // differentiate a vardecl from a statement, since a statement
        // cannot begin with two identifiers.  We bail if the first is
        // an identifier, but the second is not.
        if self.ident_at_pos(0) && !self.ident_at_pos(1) {
            return Err(SoftErr);
        }

        let pos = self.inp.pos();

        parse_do! {
            the_type <- (parse_or! {
                self.parse_keyword(Keyword::Var).map(|_| None).soften();
                self.parse_type().map(Some)
            }).soften(); // parse_type doesn't consume anything if there is in fact a statement
            name <- self.parse_ident();
            self.parse_symb(Symb::Assign);
            expr <- self.parse_or_expression();
            self.parse_symb(Symb::Semicolon);
            return VarDecl {
                name: name,
                the_type: the_type,
                expr: expr,
                pos: pos + self.inp.pos(),
            }
        }
    }

    pub fn parse_function_type(&mut self) -> ParseResult<FunType> {
        let pos = self.inp.pos();
        parse_do! {
            arg_types <- parse_star_delim! {
                Ok(()),
                self.parse_type(),
                self.parse_symb(Symb::Arrow).soften()
            };
            ret_type <- parse_or! {
                self.parse_keyword(Keyword::Void).map(|_| None).soften();
                self.parse_type().map(Some)
            };
            return FunType {
                arg_types: arg_types,
                ret_type: ret_type,
                pos: pos + self.inp.pos(),
            }
        }
    }

    pub fn parse_function_declaration(&mut self) -> ParseResult<FunDecl> {
        let pos = self.inp.pos();
        parse_do! {
            name <- self.parse_ident();

            // Parse the formal arguments
            args <- parse_star_list_delim! {
                self.parse_symb(Symb::LParen),
                self.parse_ident(),
                self.parse_symb(Symb::Comma),
                self.parse_symb(Symb::RParen).soften()
            };

            // Parse an optional type
            ftype <- parse_or! {
                parse_do! {
                    self.parse_symb(Symb::DoubleColon).soften();
                    fty <- self.parse_function_type();
                    return Some(fty)
                };
                Ok(None)
            };

            // Parse the function body
            pat (vardecls, stmts) = {
                // Ugh, ugly hack :)
                // 
                // We first want to parse var decls, and then
                // stmts. Moreover, we want to have the safeness of
                // parse_star_delim!.  So, we keep track of a state
                // (are we parsing var decls or stmts?). We create a
                // temporary closures which parses vardecls or stmts
                // depending on the state and adds them to a
                // Vec<Either<VarDecl, Stmt>>. Now we are able to
                // actually parse with parse_star_delim!. Finally, we
                // unpack the Vec<Either<_, _>> into two Vec's and
                // return them as a tuple.
                enum State { VarDecls, Stmts }
                let mut st = State::VarDecls;
                let mut parser = |s: &mut Parser<I>| {
                    match st {
                        State::VarDecls => parse_or! {
                            s.parse_var_declaration().map(Left);
                            { st = State::Stmts; s.parse_statement().map(Right) }
                        },
                        State::Stmts => s.parse_statement().map(Right),
                    }
                };
                (parse_star_delim! {
                    self.parse_symb(Symb::LBrace),
                    parser(self),
                    self.parse_symb(Symb::RBrace).soften()
                }).map(|decls| {
                    let mut vardecls = vec![];
                    let mut stmts = vec![];
                    for decl in decls {
                        match decl {
                            Left(vd) => vardecls.push(vd),
                            Right(stmt) => stmts.push(stmt),
                        }
                    }
                    (vardecls, stmts)
                })
            };

            return FunDecl {
                name: name,
                arg_names: args,
                the_type: ftype,
                var_decls: vardecls,
                stmts: stmts,
                pos: pos + self.inp.pos(),
            }
        }
    }

    pub fn parse_program(&mut self) -> ParseResult<Program> {
        let pos = self.inp.pos();
        let mut vardecls = vec![];
        let mut fundecls = vec![];
        while !self.end_of_stream() {
            let decl = parse_or! {
                self.parse_var_declaration().map(Left);
                self.parse_function_declaration().map(Right)
            };
            match decl {
                Ok(Left(vd)) => vardecls.push(vd),
                Ok(Right(fd)) => fundecls.push(fd),
                Err(e) => return Err(e),
            }
        }
        Ok(Program {
            var_decls: vardecls,
            fun_decls: fundecls,
            pos: pos + self.inp.pos(),
        })
    }
}

// This should be updated for pos...
#[cfg(no)]
mod test {
    use super::*;
    use lexer::*;
    use ast::*;

    #[test]
    fn parse_basic_type() {
        let toks = vec![
            Token::Keyword(Keyword::Int),
            Token::Keyword(Keyword::Bool),
            Token::Keyword(Keyword::Char)];
        let tokiter = toks
            .into_iter()
            .map(|tok| Ok(TokenAndPos { token: tok, pos: Pos::default() }));
        let mut p = Parser::new(tokiter);
        assert_eq!(p.parse_basic_type(), Ok(BasicType::Int));
        assert_eq!(p.parse_basic_type(), Ok(BasicType::Bool));
        assert_eq!(p.parse_basic_type(), Ok(BasicType::Char));
    }

    #[test]
    fn parse_type() {
        let toks = vec![
            Token::Keyword(Keyword::Int),

            Token::Ident("parm".to_string()),

            Token::Symb(Symb::LParen),
            Token::Keyword(Keyword::Int),
            Token::Symb(Symb::Comma),
            Token::Keyword(Keyword::Char),
            Token::Symb(Symb::RParen),

            Token::Symb(Symb::LBracket),
            Token::Keyword(Keyword::Bool),
            Token::Symb(Symb::RBracket)];
        let tokiter = toks
            .into_iter()
            .map(|tok| Ok(TokenAndPos { token: tok, pos: Pos::default() }));
        let mut p = Parser::new(tokiter);
        assert_eq!(p.parse_type(), Ok(Type::Basic(BasicType::Int)));
        assert_eq!(p.parse_type(), Ok(Type::Parm("parm".to_string())));
        assert_eq!(p.parse_type(), Ok(
            Type::Tuple(
                Box::new(Type::Basic(BasicType::Int)),
                Box::new(Type::Basic(BasicType::Char)))));
        assert_eq!(p.parse_type(), Ok(
            Type::List(Box::new(
                Type::Basic(BasicType::Bool)))));
    }

    #[test]
    fn parse_field_selector() {
        let toks = vec![
            Token::Ident("ident".to_string()),

            Token::Symb(Symb::Dot),
            Token::Keyword(Keyword::Hd),
            Token::Symb(Symb::Dot),
            Token::Keyword(Keyword::Tl),
            Token::Symb(Symb::Dot),
            Token::Keyword(Keyword::Fst),
            Token::Symb(Symb::Dot),
            Token::Keyword(Keyword::Snd)];
        let tokiter = toks
            .into_iter()
            .map(|tok| Ok(TokenAndPos { token: tok, pos: Pos::default() }));
        let mut p = Parser::new(tokiter);
        assert_eq!(p.parse_field_selector(), Ok(
            FieldSel {
                var: "ident".to_string(),
                fields: vec![Field::Hd, Field::Tl, Field::Fst, Field::Snd],
            }));
    }

    #[test]
    fn parse_function_type() {
        let program = "-> Void /* */ Int -> Int /* */ Int Int -> Int";
        let lexer = Lexer::new(program);
        let mut parser = Parser::new(lexer);
        assert_eq!(parser.parse_function_type(), Ok(
            FunType {
                arg_types: vec![],
                ret_type: None,
            }));
        assert_eq!(parser.parse_function_type(), Ok(
            FunType {
                arg_types: vec![Type::Basic(BasicType::Int)],
                ret_type: Some(Type::Basic(BasicType::Int)),
            }));
        assert_eq!(parser.parse_function_type(), Ok(
            FunType {
                arg_types: vec![Type::Basic(BasicType::Int), Type::Basic(BasicType::Int)],
                ret_type: Some(Type::Basic(BasicType::Int)),
            }));
    }

    #[test]
    fn parse_statement() {
        let program = "if (True) { return 0; } else { foo(); }
while (False) { x = 2; }
if (True) { if (True) { } }";
        let lexer = Lexer::new(program);
        let mut parser = Parser::new(lexer);
        assert_eq!(parser.parse_statement(),
                   Ok(Stmt::If {
                       cond: Expr::True,
                       true_branch: vec![Stmt::Return(Some(Expr::IntLit(0)))],
                       false_branch: vec![Stmt::FunCall(FunCall { name: "foo".to_string(), args: vec![] })],
                   }));
        assert_eq!(parser.parse_statement(),
                   Ok(Stmt::While {
                       cond: Expr::False,
                       stmts: vec![
                           Stmt::Assignment {
                               fieldsel: FieldSel {
                                   var: "x".to_string(),
                                   fields: vec![],
                               },
                               expr: Expr::IntLit(2),
                           }
                       ],
                   }));
        assert_eq!(parser.parse_statement(),
                   Ok(Stmt::If {
                       cond: Expr::True,
                       true_branch: vec![
                           Stmt::If { cond: Expr::True, true_branch: vec![], false_branch: vec![] },
                       ],
                       false_branch: vec![],
                   }));
    }

    #[test]
    fn parse_var_declaration() {
        let toks = vec![
            Token::Keyword(Keyword::Var),
            Token::Ident("name".to_string()),
            Token::Symb(Symb::Assign),
            Token::IntLit(10),
            Token::Symb(Symb::Semicolon)];
        let tokiter = toks
            .into_iter()
            .map(|tok| Ok(TokenAndPos { token: tok, pos: Pos::default() }));
        let mut parser = Parser::new(tokiter);
        assert_eq!(parser.parse_var_declaration(),
                   Ok(VarDecl {
                       name: "name".to_string(),
                       the_type: None,
                       expr: Expr::IntLit(10)
                   }));
    }

    #[test]
    fn parse_example_1() {
        let program = "facR(n) :: Int -> Int {
    if (n < 2) {
        return 1;
    } else {
        return n * facR(n - 1);
    }
 }";
        let lexer = Lexer::new(program);
        let mut parser = Parser::new(lexer);
        assert_eq!(parser.parse_function_declaration(), Ok(
            FunDecl {
                name: "facR".to_string(),
                arg_names: vec!["n".to_string()],
                the_type: Some(FunType {
                    arg_types: vec![Type::Basic(BasicType::Int)],
                    ret_type: Some(Type::Basic(BasicType::Int)),
                }),
                var_decls: vec![],
                stmts: vec![
                    Stmt::If {
                        cond: Expr::BinExpr {
                            arg1: Box::new(Expr::FieldSel(FieldSel { var: "n".to_string(), fields: vec![] })),
                            arg2: Box::new(Expr::IntLit(2)),
                            op: BinOp::LT,
                        },
                        true_branch: vec![
                            Stmt::Return(Some(Expr::IntLit(1))),
                        ],
                        false_branch: vec![
                            Stmt::Return(Some(Expr::BinExpr {
                                arg1: Box::new(Expr::FieldSel(FieldSel { var: "n".to_string(), fields: vec![] })),
                                arg2: Box::new(Expr::FunCall(FunCall {
                                    name: "facR".to_string(),
                                    args: vec![
                                        Expr::BinExpr {
                                            arg1: Box::new(Expr::FieldSel(FieldSel { var: "n".to_string(), fields: vec![] })),
                                            arg2: Box::new(Expr::IntLit(1)),
                                            op: BinOp::Subtract,
                                        },
                                    ],
                                })),
                                op: BinOp::Mult,
                            })),
                        ],
                    },
                ],
            }));
    }
}
