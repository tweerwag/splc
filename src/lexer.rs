//! The lexical analyzer/scanner.
//!
//! The lexer parses a sort of regular language consisting of:
//!
//! - symbols (`+`, `-`, `&&`, etc.);
//! - keywords (`var`, `if`, `True`, etc.);
//! - identifiers (`[a-zA-Z][a-zA-Z0-9_]*` in regex);
//! - integers (`[0-9]+` in regex); and
//! - character literals (a single backtick `` ` `` and a single
//!   unicode scalar value, e.g. `` `a ``, ``` `` ``` and `` `🐵 ``)
//!
//! In addition, it ignores all whitespace and comments. The result of
//! the analyzation is presented as a stream of `Token`s, wrapped in a
//! `LexResult`
//!
//! The careful reader might have observed that the lexer is unable to
//! parse negative integers. This is entirely correct. At this point
//! it would unnecessarily complicate the lexer to distinguish
//! subtraction from a negative integer. For example, the minus in
//! `2-1` has different semantics that the minus in `-1`. We'll leave
//! this distinction as a job for the parser and the constant folder.
//!
//! As a final remark, line comments end with a line feed (LF)
//! character. If the LF is preceded by a carriage return (CR), the CR
//! gets removed as being part of the comment. This causes both LF
//! (Unix) and CR LF (Windows) line endings to be correctly
//! interpreted. However, a bare CR or a LF CR combo will not be
//! processed as intended.

use putback::PutBack;
use lexer::LexError::*;
use posmap::{Pos, GetPos};
use std::fmt::{self, Display};
use std::error::Error;

#[derive(Debug, PartialEq, Clone)]
pub enum Symb {
    Plus,
    Minus,
    Mult,
    Divide,
    Remainder,
    Eq,
    LT,
    GT,
    LEq,
    GEq,
    NEq,
    And,
    Or,
    Not,
    LParen,
    RParen,
    LBracket,
    RBracket,
    LBrace,
    RBrace,
    Assign,
    Semicolon,
    Colon,
    DoubleColon,
    Arrow,
    Comma,
    Dot,
}

impl fmt::Display for Symb {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use lexer::Symb::*;
        let s = match self {
            &Plus => "+",
            &Minus => "-",
            &Mult => "*",
            &Divide => "/",
            &Remainder => "%",
            &Eq => "==",
            &LT => "<",
            &GT => ">",
            &LEq => "<=",
            &GEq => ">=",
            &NEq => "!=",
            &And => "&&",
            &Or => "||",
            &Not => "!",
            &LParen => "(",
            &RParen => ")",
            &LBracket => "[",
            &RBracket => "]",
            &LBrace => "{",
            &RBrace => "}",
            &Assign => "=",
            &Semicolon => ";",
            &Colon => ":",
            &DoubleColon => "::",
            &Arrow => "->",
            &Comma => ",",
            &Dot => ".",
        };
        write!(f, "'{}'", s)
    }
}

pub struct DisplaySymbs<'a>(pub &'a [Symb]);
impl<'a> fmt::Display for DisplaySymbs<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut iter = self.0.iter();
        if let Some(symb) = iter.next() {
            try!(write!(f, "{}", symb));
            for symb in iter {
                try!(write!(f, ", {}", symb));
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Keyword {
    Var,
    Void,
    Int,
    Bool,
    Char,
    If,
    Else,
    While,
    Return,
    False,
    True,
    Hd,
    Tl,
    Fst,
    Snd,
}

const KEYWORD_TEXT: [(Keyword, &'static str); 15] = [
    (Keyword::Var, "var"),
    (Keyword::Void, "Void"),
    (Keyword::Int, "Int"),
    (Keyword::Bool, "Bool"),
    (Keyword::Char, "Char"),
    (Keyword::If, "if"),
    (Keyword::Else, "else"),
    (Keyword::While, "while"),
    (Keyword::Return, "return"),
    (Keyword::False, "False"),
    (Keyword::True, "True"),
    (Keyword::Hd, "hd"),
    (Keyword::Tl, "tl"),
    (Keyword::Fst, "fst"),
    (Keyword::Snd, "snd")];

impl fmt::Display for Keyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for &(ref kw, s) in &KEYWORD_TEXT {
            if self == kw {
                return write!(f, "{}", s);
            }
        }
        unreachable!();
    }
}

pub struct DisplayKws<'a>(pub &'a [Keyword]);
impl<'a> fmt::Display for DisplayKws<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut iter = self.0.iter();
        if let Some(kw) = iter.next() {
            try!(write!(f, "{}", kw));
            for kw in iter {
                try!(write!(f, ", {}", kw));
            }
        }
        Ok(())
    }
}

#[derive(Debug, PartialEq)]
pub enum Token {
    Symb(Symb),
    Keyword(Keyword),
    Ident(String),
    IntLit(u32),
    CharLit(char),
}

#[derive(Debug, PartialEq)]
pub struct TokenAndPos {
    pub token: Token,
    pub pos: Pos,
}

impl GetPos for TokenAndPos {
    fn pos(&self) -> Pos {
        self.pos
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum LexError {
    Unknown,
    UnclosedComment,
    EndOfStream,
}

impl Display for LexError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl Error for LexError {
    fn description(&self) -> &str {
        match *self {
            LexError::Unknown => "Unknown character encountered",
            LexError::UnclosedComment => "A block comment was not closed",
            LexError::EndOfStream => "End of stream encountered",
        }
    }
}

pub type LexResult<T> = Result<T, LexError>;

trait UnknownElse {
    type Item;

    /// On `LexError::Unknown`, do something else.
    fn unknown_else<O>(self, O) -> LexResult<Self::Item>
            where O: FnOnce() -> LexResult<Self::Item>;
}

impl<T> UnknownElse for LexResult<T> {
    type Item = T;

    fn unknown_else<O>(self, op: O) -> LexResult<T>
            where O: FnOnce() -> LexResult<T> {
        match self {
            Err(Unknown) => op(),
            val @ _ => val,
        }
    }
}

/// A little helper macro which unwraps an `Option` and let the
/// calling function return an `EndOfStream` error if `None`.
macro_rules! let_or_err {
    ( $x:ident = $e:expr ) => {
        let $x = {
            let y = $e;
            if y.is_none() { return Err(LexError::EndOfStream); }
            y
        }.unwrap();
    }
}

/// Helper macro which scans an optional second character, and returns
/// a `Token` which value depends on whether the second character was
/// found. It also takes care of putting back the right amount of
/// characters.
macro_rules! scan_opt_second_char {
    ( $selfie:expr, $t1:expr, $c:expr => $t2:expr ) => {{
        match $selfie.input_next() {
            Some($c) => Ok($t2),
            Some(ch) => { $selfie.input_put_back(ch); Ok($t1) }
            None => Ok($t1),
        }
    }}
}

/// Helper macro which scans a needed second character, and returns
/// the `Token` if it is found, or otherwise an `Unknown` error. It
/// also takes care of putting back the right amount of characters.
macro_rules! scan_needed_second_char {
    ( $selfie:expr, $c_old:expr, $c:expr => $t:expr ) => {{
        match $selfie.input_next() {
            Some($c) => Ok($t),
            Some(ch) => {
                $selfie.input_put_back(ch);
                $selfie.input_put_back($c_old);
                Err(Unknown)
            }
            None => {
                $selfie.input_put_back($c_old);
                Err(Unknown)
            }
        }
    }}
}

pub struct Lexer<I>
    where I: Iterator<Item=char>
{
    input_iter: PutBack<I>,
    pos: u32,
}

impl<I> Lexer<I>
    where I: Iterator<Item=char>
{
    /// Creates a new `Lexer` from a `Iterator` over `char`s.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use std::io;
    /// use std::io::prelude::*;
    /// use splc::lexer::Lexer;
    ///
    /// # fn foo() -> io::Result<()> {
    /// let mut my_string = String::new();
    /// try!(io::stdin().read_to_string(&mut my_string));
    /// let mut lex = Lexer::new(my_string.chars());
    /// # Ok(())
    /// # }
    /// ```
    pub fn new<II>(into: II) -> Lexer<I>
        where II: IntoIterator<Item=I::Item,IntoIter=I>
    {
        Lexer {
            input_iter: PutBack::new(into.into_iter()),
            pos: 0,
        }
    }

    /// Returns the next character in the input stream and keeps track
    /// of position.
    fn input_next(&mut self) -> Option<char> {
        let maybe_c = self.input_iter.next();
        match maybe_c {
            Some(_) => self.pos += 1,
            None => { },
        }
        maybe_c
    }

    /// Puts a character back in the stream and updates the column
    /// number accordingly.
    fn input_put_back(&mut self, c: char) {
        self.pos -= 1;
        self.input_iter.put_back(c);
    }

    /// Eats up whitespace from the input.
    fn eat_whitespace(&mut self) {
        while let Some(c) = self.input_next() {
            if !c.is_whitespace() {
                self.input_put_back(c);
                break;
            }
        }
    }

    /// Eats up comments from the input.
    ///
    /// Indicates whether anything is eaten. Can throw
    /// `LexError::UnclosedComment`.
    fn eat_comment(&mut self) -> LexResult<bool> {
        match self.input_next() {
            Some('/') => {
                match self.input_next() {
                    Some('/') => {
                        while self.input_next() != Some('\n') { }
                        return Ok(true);
                    }
                    Some('*') => {
                        let mut saw_asterisk = false;
                        while let Some(c) = self.input_next() {
                            match c {
                                '*' => saw_asterisk = true,
                                '/' if saw_asterisk => return Ok(true),
                                _ => saw_asterisk = false,
                            }
                        }
                        // if we came here, we have an unclosed comment
                        return Err(UnclosedComment);
                    }
                    Some(c) => { self.input_put_back(c); self.input_put_back('/'); }
                    None => self.input_put_back('/'),
                }
            }
            Some(c) => self.input_put_back(c),
            None => { }
        }
        Ok(false)
    }

    /// Tries to scan a `Symb` and advances the input iterator.
    ///
    /// If the start of the input is not pointing at a symbol, the
    /// underlying iterator is not advanced and `None` is returned.
    fn scan_symbol(&mut self) -> LexResult<Token> {
        use lexer::Symb::*;
        use lexer::Token::Symb;
        let_or_err!(c = self.input_next());
        match c {
            '+' => Ok(Symb(Plus)),
            '-' => scan_opt_second_char!(self, Symb(Minus), '>' => Symb(Arrow)),
            '*' => Ok(Symb(Mult)),
            '/' => Ok(Symb(Divide)),
            '%' => Ok(Symb(Remainder)),
            '=' => scan_opt_second_char!(self, Symb(Assign), '=' => Symb(Eq)),
            '<' => scan_opt_second_char!(self, Symb(LT), '=' => Symb(LEq)),
            '>' => scan_opt_second_char!(self, Symb(GT), '=' => Symb(GEq)),
            '!' => scan_opt_second_char!(self, Symb(Not), '=' => Symb(NEq)),
            '&' => scan_needed_second_char!(self, c, '&' => Symb(And)),
            '|' => scan_needed_second_char!(self, c, '|' => Symb(Or)),
            '(' => Ok(Symb(LParen)),
            ')' => Ok(Symb(RParen)),
            '[' => Ok(Symb(LBracket)),
            ']' => Ok(Symb(RBracket)),
            '{' => Ok(Symb(LBrace)),
            '}' => Ok(Symb(RBrace)),
            ';' => Ok(Symb(Semicolon)),
            ':' => scan_opt_second_char!(self, Symb(Colon), ':' => Symb(DoubleColon)),
            ',' => Ok(Symb(Comma)),
            '.' => Ok(Symb(Dot)),
            _ => { self.input_put_back(c); Err(Unknown) }
        }
    }

    /// Tries to scan an integer literal and advances the input
    /// iterator.
    ///
    /// If the start of the input is not pointing at an integer, the
    /// underlying iterator is not advanced and `None` is returned.
    /// Note that only positive integers are scanned. The parser takes
    /// care of combining a minus and an integer to a negative
    /// integer.
    fn scan_integer_literal(&mut self) -> LexResult<Token> {
        let_or_err!(c = self.input_next());
        if !c.is_digit(10) {
            self.input_put_back(c);
            return Err(LexError::Unknown);
        }

        let mut s = c.to_string();
        while let Some(c) = self.input_next() {
            if c.is_digit(10) {
                s.push(c);
            } else {
                self.input_put_back(c);
                break;
            }
        }

        Ok(Token::IntLit(s.parse().unwrap()))
    }

    /// Tries to scan a character literal and advances the input
    /// iterator.
    fn scan_character_literal(&mut self) -> LexResult<Token> {
        let_or_err!(c = self.input_next());
        if c == '`' {
            self.input_next().map(Token::CharLit).ok_or(LexError::Unknown)
        } else {
            self.input_put_back(c);
            Err(LexError::Unknown)
        }
    }

    /// Tries to scan a keyword or identifier and advances the input
    /// iterator.
    ///
    /// If the start of the input is not pointing at an identifier or
    /// keyword, the underlying iterator is not advanced and `None` is
    /// returned.  Since keywords are a subset of the identifiers, we
    /// first try to scan an identifier and then check if the ident is
    /// in fact a keyword.
    fn scan_ident_keyword(&mut self) -> LexResult<Token> {
        let_or_err!(c = self.input_next());

        let mut ret = String::new();
        match c {
            'a' ... 'z' | 'A' ... 'Z' => ret.push(c),
            _ => { self.input_put_back(c); return Err(Unknown); }
        }

        while let Some(c) = self.input_next() {
            match c {
                'a' ... 'z' | 'A' ... 'Z' | '0' ... '9' | '_' => ret.push(c),
                _ => { self.input_put_back(c); break; }
            }
        }

        // Now check if the ident is in fact a keyword.
        for &(ref val, text) in &KEYWORD_TEXT {
            if ret == text {
                return Ok(Token::Keyword((*val).clone()));
            }
        }

        // It is not, so return it as an ident.
        Ok(Token::Ident(ret))
    }
}

impl<I> Iterator for Lexer<I>
    where I: Iterator<Item=char>
{
    type Item = Result<TokenAndPos, LexError>;

    fn next(&mut self) -> Option<Result<TokenAndPos, LexError>> {
        loop {
            self.eat_whitespace();
            match self.eat_comment() {
                Ok(eaten) if !eaten => break,
                Ok(_) => { }
                Err(Unknown) => return None,
                Err(e) => return Some(Err(e)),
            }
        }

        let begin = self.pos;
        let token_result = self.scan_symbol()
            .unknown_else(|| self.scan_integer_literal())
            .unknown_else(|| self.scan_character_literal())
            .unknown_else(|| self.scan_ident_keyword());
        let end = self.pos;

        match token_result {
            Err(EndOfStream) => None,
            Err(e) => Some(Err(e)),
            Ok(token) => Some(Ok(TokenAndPos {
                token: token,
                pos: Pos { begin: begin, end: end }
            })),
        }
    }
}

impl<I> GetPos for Lexer<I>
    where I: Iterator<Item=char>
{
    fn pos(&self) -> Pos {
        Pos { begin: self.pos, end: self.pos }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use super::LexError::*;
    use posmap::Pos;
    use std::str::Chars;

    fn setup_lexer<'a>(input: &'a str) -> Lexer<Chars<'a>> {
        Lexer::new(input.chars())
    }

    #[test]
    fn scan_symbol() {
        let mut l = setup_lexer("+->-==<=&&%:");
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Plus)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Arrow)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Minus)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Eq)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::LEq)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::And)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Remainder)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Colon)));
        assert_eq!(l.scan_symbol(), Err(LexError::EndOfStream));
    }

    #[test]
    fn scan_symbol_unknown() {
        let mut l = setup_lexer("=1234");
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Assign)));
        assert_eq!(l.scan_symbol(), Err(Unknown));
    }

    #[test]
    fn scan_symbol_single_ampersand() {
        let mut l = setup_lexer("&");
        assert_eq!(l.scan_symbol(), Err(Unknown));
        assert_eq!(l.input_next(), Some('&'));
    }

    #[test]
    fn scan_integer_literal() {
        let mut l = setup_lexer("1234");
        assert_eq!(l.scan_integer_literal(), Ok(Token::IntLit(1234)));
        assert_eq!(l.scan_integer_literal(), Err(EndOfStream));
    }

    #[test]
    fn scan_integer_unknown() {
        let mut l = setup_lexer("1234=");
        assert_eq!(l.scan_integer_literal(), Ok(Token::IntLit(1234)));
        assert_eq!(l.scan_integer_literal(), Err(Unknown));
    }

    #[test]
    fn scan_character_literal() {
        let mut l = setup_lexer("`a```\u{1f435}");
        assert_eq!(l.scan_character_literal(), Ok(Token::CharLit('a')));
        assert_eq!(l.scan_character_literal(), Ok(Token::CharLit('`')));
        assert_eq!(l.scan_character_literal(), Ok(Token::CharLit('\u{1f435}')));
        assert_eq!(l.scan_character_literal(), Err(EndOfStream));
    }

    #[test]
    fn scan_character_literal_unfinished() {
        let mut l = setup_lexer("`");
        assert_eq!(l.scan_character_literal(), Err(Unknown));
    }

    #[test]
    fn scan_ident() {
        let mut l = setup_lexer("x=y_0");
        assert_eq!(l.scan_ident_keyword(), Ok(Token::Ident("x".to_string())));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Assign)));
        assert_eq!(l.scan_ident_keyword(), Ok(Token::Ident("y_0".to_string())));
        assert_eq!(l.scan_ident_keyword(), Err(EndOfStream));
    }

    #[test]
    fn scan_keyword() {
        let mut l = setup_lexer("var=Void");
        assert_eq!(l.scan_ident_keyword(), Ok(Token::Keyword(Keyword::Var)));
        assert_eq!(l.scan_symbol(), Ok(Token::Symb(Symb::Assign)));
        assert_eq!(l.scan_ident_keyword(), Ok(Token::Keyword(Keyword::Void)));
        assert_eq!(l.scan_ident_keyword(), Err(EndOfStream));
    }

    #[test]
    fn lex_end_of_stream() {
        let mut l = setup_lexer("var");
        assert_eq!(l.next().unwrap().unwrap().token, Token::Keyword(Keyword::Var));
        assert_eq!(l.next(), None);
    }

    #[test]
    fn lex_block_comment() {
        let mut l = setup_lexer("var /* comment */ x");
        assert_eq!(l.next().unwrap().unwrap().token, Token::Keyword(Keyword::Var));
        assert_eq!(l.next().unwrap().unwrap().token, Token::Ident("x".to_string()));
    }

    #[test]
    fn lex_unclosed_comment() {
        let mut l = setup_lexer("/*");
        assert_eq!(l.next(), Some(Err(UnclosedComment)));
    }

    #[test]
    fn lex_comment_crlf() {
        let mut l = setup_lexer("// this is a comment\r\nvar");
        assert_eq!(l.next().unwrap().unwrap().token, Token::Keyword(Keyword::Var));
    }

    #[test]
    fn lex_pos() {
        let mut l = setup_lexer("var\n  var\n//\nvar\n/* */var");
        assert_eq!(l.next().unwrap().unwrap(), TokenAndPos {
            token: Token::Keyword(Keyword::Var),
            pos: Pos { begin: 0, end: 3 },
        });
        assert_eq!(l.next().unwrap().unwrap(), TokenAndPos {
            token: Token::Keyword(Keyword::Var),
            pos: Pos { begin: 6, end: 9 },
        });
        assert_eq!(l.next().unwrap().unwrap(), TokenAndPos {
            token: Token::Keyword(Keyword::Var),
            pos: Pos { begin: 13, end: 16 },
        });
        assert_eq!(l.next().unwrap().unwrap(), TokenAndPos {
            token: Token::Keyword(Keyword::Var),
            pos: Pos { begin: 22, end: 25 },
        });
    }

    #[test]
    fn small_lex_test_1() {
        use super::Symb::*;
        use super::Keyword::*;
        use super::Token::*;
        let l = setup_lexer("\
facR(n) :: Int -> Int {
    if (n < 2) {
        return 1;
    } else {
        return n * facR(n - 1);
    }
}");
        let iter = l.take_while(|r| r.is_ok()).map(|r| r.unwrap().token);
        assert_eq!(iter.collect::<Vec<_>>(), vec![
            Ident("facR".to_string()),
            Symb(LParen),
            Ident("n".to_string()),
            Symb(RParen),
            Symb(DoubleColon),
            Keyword(Int),
            Symb(Arrow),
            Keyword(Int),
            Symb(LBrace),
            Keyword(If),
            Symb(LParen),
            Ident("n".to_string()),
            Symb(LT),
            IntLit(2),
            Symb(RParen),
            Symb(LBrace),
            Keyword(Return),
            IntLit(1),
            Symb(Semicolon),
            Symb(RBrace),
            Keyword(Else),
            Symb(LBrace),
            Keyword(Return),
            Ident("n".to_string()),
            Symb(Mult),
            Ident("facR".to_string()),
            Symb(LParen),
            Ident("n".to_string()),
            Symb(Minus),
            IntLit(1),
            Symb(RParen),
            Symb(Semicolon),
            Symb(RBrace),
            Symb(RBrace)]);
    }
}
