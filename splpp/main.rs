extern crate splc;

use splc::driver::{PrettyPrinter, main_driver};

fn main() {
    main_driver(PrettyPrinter);
}
