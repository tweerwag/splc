# Grammar
#
# The grammar is a sort of EBNF.  Non-terminals start with an
# uppercase character.  Terminals start with a lowercase character or
# is literal text enclosed in quotes.  Parentheses are used to group
# elements.  Square brackets indicate optional elements.  A star (*)
# indicates a repetition of zero or more of the preceding element or
# group of elements.  Likewise, a plus sign (+) indicates a repetition
# of one or more.
#
# Each production rule has a corresponding function in parser.rs.
# These rules are in the same order as the functions in parser.rs.
# For some reason, I implemented these bottom-up (starting with the
# "lowest" rules).  Therefore, a program consists of the non-terminal
# Prog, whose rule is actually at the bottom of this grammar.

Type      = BasicType | '(' Type ',' Type ')' | '[' Type ']' | id
BasicType = 'Int' | 'Bool' | 'Char'
FieldSel  = id ( '.' 'hd' | '.' 'tl' | '.' 'fst' | '.' 'snd' )*
FunCall   = id '(' [ OrExp ( ',' OrExp )* ] ')'
OrExp     = AndExp | AndExp '||' OrExp
AndExp    = RelExp | RelExp '&&' AndExp
RelExp    = ConsExp
          | ConsExp '==' RelExp
          | ConsExp '<' RelExp
          | ConsExp '>' RelExp
          | ConsExp '<=' RelExp
          | ConsExp '>=' RelExp
          | ConsExp '!=' RelExp
ConsExp   = AddExp | AddExp ':' ConsExp
AddExp    = MulExp | MulExp '+' AddExp | MulExp '-' AddExp
MulExp    = UnExp | UnExp '*' MulExp | UnExp '/' MulExp | UnExp '%' MulExp
UnExp     = PrimExp | '!' UnExp | '-' UnExp
PrimExp   = FieldSel
          | int_literal | char_literal
          | '(' OrExp ')'
          | FunCall
          | '[' ']'
          | 'True' | 'False'
          | '(' OrExp ',' OrExp ')'
Stmt      = 'if' '(' OrExp ')' Block [ 'else' Block ]
          | 'while' '(' OrExp ')' Block
          | FieldSel '=' OrExp ';'
          | FunCall ';'
          | 'return' [ OrExp ] ';'
Block     = '{' Stmt* '}'
VarDecl   = ( 'var' | Type ) id '=' OrExp ';'
FunType   = Type* '->' ( Type | 'Void' )
FunDecl   = id '(' [ id ( ',' id )* ] ')' [ '::' FunType ] '{' VarDecl* Stmt+ '}'
Prog      = ( VarDecl | FunDecl )*
