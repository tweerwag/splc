extern crate splc;

use splc::driver::{Compiler, main_driver};

fn main() {
    main_driver(Compiler);
}
