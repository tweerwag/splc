Require Export String ZArith.

Inductive binop : Set :=
| Or | And
| Eq | LT | GT | LEq | GEq | NEq
| Cons
| Add | Sub | Mul | Div | Rem.

Inductive unop : Set :=
  Neg | Opp.

Inductive fieldsel : Set :=
| FVar : string -> fieldsel
| FHd : fieldsel -> fieldsel
| FTl : fieldsel -> fieldsel
| FFst : fieldsel -> fieldsel
| FSnd : fieldsel -> fieldsel.

Inductive expr : Set :=
| EFieldSel : fieldsel -> expr
| ENum : Z -> expr
| EChar : Z -> expr
| ETrue | EFalse | ENil
| ETuple : expr -> expr -> expr
| EFunCall : string -> list expr -> expr
| EBinOp : binop -> expr -> expr -> expr
| EUnOp : unop -> expr -> expr.

Inductive stmt : Set :=
| SSkip | SSeq : stmt -> stmt -> stmt
| SVal : string -> expr -> stmt -> stmt
| SIf : expr -> stmt -> stmt -> stmt
| SWhile : expr -> stmt -> stmt
| SAssign : fieldsel -> expr -> stmt
| SFunCall : string -> list expr -> stmt
| SReturn : option expr -> stmt.

Inductive prog : Set :=
| PEnd
| PVal : string -> expr -> prog -> prog
| PFun : string -> list string -> stmt -> prog -> prog.