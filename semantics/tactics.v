Require Import program types.

Ltac solve_fvar :=
  let v := fresh "v" in
  let H := fresh "H" in
  match goal with
  | [|- fieldsel_type ?g (FVar ?x) _] =>
    lazymatch eval compute in (find_env g x) with
    | Some ?tyscm => apply (TFVar _ _ tyscm)
    | _ => fail "not found in environment" x
    end;
    [
    | exists SubstId; split;
      [ intros v H;
        simpl in H;
        repeat match goal with
          [ H : _ \/ _ |- _ ] => inversion_clear H
        end;
        solve [ subst v; reflexivity | contradiction ]
      |
      ]
    ];
    reflexivity
  end.

Ltac solve_binop :=
  match goal with
  | [|- expr_type _ (EBinOp ?op _ _) ?ty] =>
    match op with
    | Or => apply (TEBinOp _ _ _ _ TBool TBool)
    | And => apply (TEBinOp _ _ _ _ TBool TBool)
    | Eq => apply (TEBinOp _ _ _ _ ty ty)
    | NEq => apply (TEBinOp _ _ _ _ ty ty)
    | LT => apply (TEBinOp _ _ _ _ TInt TInt)
    | GT => apply (TEBinOp _ _ _ _ TInt TInt)
    | LEq => apply (TEBinOp _ _ _ _ TInt TInt)
    | GEq => apply (TEBinOp _ _ _ _ TInt TInt)
    | Cons =>
      match ty with
      | TList ?ty => apply (TEBinOp _ _ _ _ ty (TList ty))
      | _ => fail "type" ty "is not list type"
      end
    | Add => fail "Add is ad-hoc overloaded"
    | Sub => fail "Sub is ad-hoc overloaded"
    | Mul => apply (TEBinOp _ _ _ _ TInt TInt)
    | Div => apply (TEBinOp _ _ _ _ TInt TInt)
    | Rem => apply (TEBinOp _ _ _ _ TInt TInt)
    end;
    constructor
  end.

Ltac solve_most_typing :=
  repeat first [constructor | solve_fvar | solve_binop].

Ltac solve_stmt_funcall argtys retty s :=
  let v := fresh "v" in
  let H := fresh "H" in
  match goal with
  | [|- stmt_type ?g (SFunCall ?x _) _] =>
    lazymatch eval compute in (find_env g x) with
    | Some ?tyscm => apply (TSFunCall _ _ _ tyscm argtys retty)
    | _ => fail "not found in environment" x
    end;
    [ reflexivity
    | exists s; split;
      [ intros v H;
        simpl in H;
        repeat match goal with
          [ H : _ \/ _ |- _ ] => inversion_clear H
        end;
        solve [ subst v; reflexivity | contradiction ]
      | reflexivity ]
    | ]
  end; solve_most_typing.

Ltac solve_simple_stmt_funcall argtys retty :=
  solve_stmt_funcall argtys retty SubstId.

Ltac solve_funcall argtys s :=
  let v := fresh "v" in
  let H := fresh "H" in
  match goal with
  | [|- expr_type ?g (EFunCall ?x _) _] =>
    lazymatch eval compute in (find_env g x) with
    | Some ?tyscm => apply (TEFunCall _ _ _ tyscm argtys)
    | _ => fail "not found in environment" x
    end;
    [ reflexivity
    | exists s; split;
      [ intros v H;
        simpl in H;
        repeat match goal with
          [ H : _ \/ _ |- _ ] => inversion_clear H
        end;
        solve [ subst v; reflexivity | contradiction ]
      | reflexivity ]
    | solve [ discriminate | reflexivity ]
    | ]
  end; solve_most_typing.

Ltac solve_simple_funcall argtys :=
  solve_funcall argtys SubstId.

Ltac solve_stmt_val ty :=
  apply (TSVal _ _ _ _ ty); solve_most_typing.

Ltac solve_fun fty :=
  lazymatch eval hnf in fty with
  | TFun ?argtys ?retty => apply (TPFun _ _ _ argtys _ retty)
  | _ => fail "not a function type"
  end; solve_most_typing.

Ltac solve_var ty :=
  apply (TPVal _ _ _ ty); solve_most_typing.

Ltac solve_assign ty :=
  apply (TSAssign _ _ _ ty); solve_most_typing.

Ltac solve_binop_explicit ty1 ty2 :=
  apply (TEBinOp _ _ _ _ ty1 ty2); solve_most_typing.
