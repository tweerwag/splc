Require Import List ZArith String.
Require Import program types tactics.
Import List.ListNotations.
Local Open Scope Z_scope.
Local Open Scope string_scope.

(* This Coq file represents the SPL program:
 * head(x) { return x.hd; }
 * main() { return head(1 : []); } *)

Definition fun_head := (SSeq (SReturn (Some (EFieldSel (FHd (FVar "x"))))) SSkip).
Definition type_fun_head := (TFun [(TList (TVar 0))] (TVar 0)).
Definition fun_main := (SSeq (SReturn (Some (EFunCall "head" [(EBinOp Cons (ENum 1) ENil)]))) SSkip).
Definition type_fun_main := (TFun [] TInt).
Definition the_program := (PFun "head" ["x"] fun_head (PFun "main" [] fun_main PEnd)).

Theorem program_typed : prog_type empty_env the_program.
Proof.
  solve_fun type_fun_head.
  solve_fun type_fun_main.
    (* We call fun_head with a list of integers. We explicitly instantiate the
     * type variable with TInt. *)
    solve_funcall [TList TInt] (update_subst 0 TInt SubstId).
Qed.
