Require Import List ListSet program.
Import ListNotations.

Notation var := (nat).

Inductive type : Set :=
| TVar : var -> type
| TVoid | TInt | TChar | TBool
| TFun : list type -> type -> type
| TList : type -> type
| TTuple : type -> type -> type.
Fixpoint free_vars_ty (ty : type) :=
  match ty with
  | TVar v => [v]
  | TVoid | TInt | TChar | TBool => []
  | TFun tys ty => concat (map free_vars_ty tys) ++ free_vars_ty ty
  | TList ty => free_vars_ty ty
  | TTuple ty1 ty2 => free_vars_ty ty1 ++ free_vars_ty ty2
  end.

Record type_scheme : Set := MkTypeScheme {
  bound_vars : list var;
  type_of : type;
}.
Definition free_vars_tyscm (tyscm : type_scheme) :=
  fold_left
    (fun vs v => remove eq_nat_dec v vs)
    (bound_vars tyscm)
    (free_vars_ty (type_of tyscm)).
Definition promote_type ty := MkTypeScheme [] ty.

Record env := MkEnv {
  env_dom : set string;
  env_fun : string -> type_scheme;
}.
Definition empty_env : env :=
  MkEnv (empty_set string) (fun x => MkTypeScheme [] TVoid).
Definition free_vars_env (gamma : env) :=
  fold_left
    (fun acc x => free_vars_tyscm ((env_fun gamma) x))
    (env_dom gamma)
    [].
Definition find_env (gamma : env) (x : string) : option type_scheme :=
  if set_mem string_dec x (env_dom gamma)
  then Some ((env_fun gamma) x)
  else None.
Definition update_env (x : string) (tyscm : type_scheme) (gamma : env) : env :=
  MkEnv
    (set_add string_dec x (env_dom gamma))
    (fun y => if string_dec x y then tyscm else (env_fun gamma) y).
Definition update_env_with_type (tup : string * type) : env -> env :=
  update_env (fst tup) (promote_type (snd tup)).

Definition type_closure (gamma : env) (ty : type) : type_scheme :=
  MkTypeScheme
    (fold_left
      (fun vs v => remove eq_nat_dec v vs)
      (free_vars_env gamma)
      (free_vars_ty ty))
    ty.

Definition subst : Set := var -> type.

Definition SubstId : subst := fun v => TVar v.
Definition update_subst (v : var) (ty : type) (s : subst) : subst :=
  fun v' => if v' =? v then ty else s v'.

Fixpoint substitute (s : subst) (ty : type) :=
  match ty with
  | TVar v => s v
  | TVoid | TInt | TChar | TBool => ty
  | TFun argtys retty => TFun (map (substitute s) argtys) (substitute s retty)
  | TList ty => TList (substitute s ty)
  | TTuple ty1 ty2 => TTuple (substitute s ty1) (substitute s ty2)
  end.

Definition instance_of tyscm ty :=
  exists s : subst, (forall v, In v (free_vars_tyscm tyscm) -> s v = TVar v)
    /\ substitute s (type_of tyscm) = ty.

Inductive binop_type : binop -> type -> type -> type -> Prop :=
| TBOOr : binop_type Or TBool TBool TBool
| TBOAnd : binop_type And TBool TBool TBool
| TBOEq : forall ty, binop_type Eq ty ty TBool
| TBONEq : forall ty, binop_type NEq ty ty TBool
| TBOLT : binop_type LT TInt TInt TBool
| TBOGT : binop_type GT TInt TInt TBool
| TBOLEq : binop_type LEq TInt TInt TBool
| TBOGEq : binop_type GEq TInt TInt TBool
| TBOCons : forall ty, binop_type Cons ty (TList ty) (TList ty)
| TBOAddII : binop_type Add TInt TInt TInt
| TBOAddCI : binop_type Add TChar TInt TChar
| TBOAddIC : binop_type Add TInt TChar TChar
| TBOSubII : binop_type Sub TInt TInt TInt
| TBOSubCI : binop_type Sub TChar TInt TChar
| TBOSubCC : binop_type Sub TChar TChar TChar
| TBOMul : binop_type Mul TInt TInt TInt
| TBODiv : binop_type Div TInt TInt TInt
| TBORem : binop_type Rem TInt TInt TInt.

Inductive unop_type : unop -> type -> type -> Prop :=
| TUONeg : unop_type Neg TBool TBool
| TUOOpp : unop_type Opp TInt TInt.

Inductive fieldsel_type : env -> fieldsel -> type -> Prop :=
| TFVar : forall gamma x tyscm ty,
    find_env gamma x = Some tyscm ->
    instance_of tyscm ty ->
    fieldsel_type gamma (FVar x) ty
| TFHd : forall gamma f ty,
    fieldsel_type gamma f (TList ty) ->
    fieldsel_type gamma (FHd f) ty
| TFTl : forall gamma f ty,
    fieldsel_type gamma f (TList ty) ->
    fieldsel_type gamma (FTl f) (TList ty)
| TFFst : forall gamma f ty1 ty2,
    fieldsel_type gamma f (TTuple ty1 ty2) ->
    fieldsel_type gamma (FFst f) ty1
| TFSnd : forall gamma f ty1 ty2,
    fieldsel_type gamma f (TTuple ty1 ty2) ->
    fieldsel_type gamma (FSnd f) ty2.

Inductive expr_type : env -> expr -> type -> Prop :=
| TEFieldSel : forall gamma f ty,
    fieldsel_type gamma f ty ->
    expr_type gamma (EFieldSel f) ty
| TENum : forall gamma n, expr_type gamma (ENum n) TInt
| TEChar : forall gamma n, expr_type gamma (EChar n) TChar
| TETrue : forall gamma, expr_type gamma ETrue TBool
| TEFalse : forall gamma, expr_type gamma EFalse TBool
| TENil : forall gamma ty, expr_type gamma ENil (TList ty)
| TETuple : forall gamma e1 e2 ty1 ty2,
    expr_type gamma e1 ty1 -> expr_type gamma e2 ty2 ->
    expr_type gamma (ETuple e1 e2) (TTuple ty1 ty2)
| TEFunCall : forall gamma x es tyscm tys ty',
    find_env gamma x = Some tyscm ->
    instance_of tyscm (TFun tys ty') ->
    ty' <> TVoid ->
    Forall2 (expr_type gamma) es tys ->
    expr_type gamma (EFunCall x es) ty'
| TEBinOp : forall gamma op e1 e2 ty1 ty2 ty3,
    binop_type op ty1 ty2 ty3 ->
    expr_type gamma e1 ty1 -> expr_type gamma e2 ty2 ->
    expr_type gamma (EBinOp op e1 e2) ty3
| TEUnOp : forall gamma op e ty1 ty2,
    unop_type op ty1 ty2 -> expr_type gamma e ty1 ->
    expr_type gamma (EUnOp op e) ty2.

Inductive stmt_type : env -> stmt -> type -> Prop :=
| TSSkip : forall gamma ty,
    stmt_type gamma SSkip ty
| TSSeq : forall gamma s1 s2 ty,
    stmt_type gamma s1 ty ->
    stmt_type gamma s2 ty ->
    stmt_type gamma (SSeq s1 s2) ty
| TSVal : forall gamma x e s ty' ty,
    expr_type gamma e ty' ->
    stmt_type (update_env x (promote_type ty') gamma) s ty ->
    stmt_type gamma (SVal x e s) ty
| TSIf : forall gamma e s1 s2 ty,
    expr_type gamma e TBool ->
    stmt_type gamma s1 ty ->
    stmt_type gamma s2 ty ->
    stmt_type gamma (SIf e s1 s2) ty
| TSWhile : forall gamma e s ty,
    expr_type gamma e TBool ->
    stmt_type gamma s ty ->
    stmt_type gamma (SWhile e s) ty
| TSAssign : forall gamma f e ty' ty,
    fieldsel_type gamma f ty' ->
    expr_type gamma e ty' ->
    stmt_type gamma (SAssign f e) ty
| TSFunCall : forall gamma x es tyscm tys' ty'' ty,
    find_env gamma x = Some tyscm ->
    instance_of tyscm (TFun tys' ty'') ->
    Forall2 (expr_type gamma) es tys' ->
    stmt_type gamma (SFunCall x es) ty
| TSReturnV : forall gamma,
    stmt_type gamma (SReturn None) TVoid
| TSReturnE : forall gamma e ty,
    expr_type gamma e ty ->
    stmt_type gamma (SReturn (Some e)) ty.

Inductive prog_type : env -> prog -> Prop :=
| TPEnd : forall gamma,
    prog_type gamma PEnd
| TPVal : forall gamma x e ty p,
    expr_type gamma e ty ->
    prog_type (update_env x (promote_type ty) gamma) p ->
    prog_type gamma (PVal x e p)
| TPFun : forall gamma x ys tys s ty' p,
    stmt_type (fold_right update_env_with_type gamma (combine ys tys)) s ty' ->
    prog_type (update_env x (type_closure gamma (TFun tys ty')) gamma) p ->
    prog_type gamma (PFun x ys s p).
