# SPLC: A compiler for Simple Programming Language

SPLC is coded in Rust. Compilation is done with cargo, which automatically
downloads and compiles dependencies.

## Building

Currently the code is tested with Rust 1.8.0, the latest stable. Run
```sh
cargo build
```
to compile. The resulting binaries (`splc` and `splpp`) are found in
the `target` directory, or can be run with
```sh
cargo run --bin splc
cargo run --bin splpp
```

## Running

SPLC includes two binaries, a pretty printer `splpp`, and the compiler `splc`.
Both accept input from stdin or a file. The output can be set with the `-o` option.
If no output is set, output is printed to stdout. Example:
```sh
cargo run --bin splc -- -o factorial.ssm factorial.spl
```
Or using the binaries directly:
```sh
target/debug/splc -o factorial.ssm factorial.spl
```

A usage message and short help on the options can be obtained with `-h` or `--help`.
```sh
cargo run --bin splpp -- --help
cargo run --bin splc -- --help
```

## Code documentation

Documentation of the code can be built with
```sh
cargo rustdoc -- --no-defaults --passes "collapse-docs" --passes "unindent-comments"
```
(Note that the normal `cargo doc` only builds documentation for publicly
visible types, structs, etc.) The resulting documentation can be found in
`target/doc/splc/index.html`.
