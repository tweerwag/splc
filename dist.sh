#!/bin/sh
echo "Make sure the semantics dir is clean!"
TEMP=`mktemp -d`
cp -r src "$TEMP"
cp -r spl2coq "$TEMP"
cp -r splc "$TEMP"
cp -r splpp "$TEMP"
cp Cargo.toml Cargo.lock README.md LICENSE grammar.txt "$TEMP"
cargo rustdoc -- --no-defaults --passes "collapse-docs" --passes "unindent-comments"
cp -r target/doc "$TEMP"
cp -r ex-should-work "$TEMP"
cp -r ex-should-fail "$TEMP"
cp -r semantics "$TEMP"

HERE=`pwd`
cd "$TEMP/semantics"
rm -f .gitignore .*.aux *.vo *.glob *.d Makefile.bak
cd ..
zip -r "$HERE/splc.zip" .
cd "$HERE"
rm -r "$TEMP"
