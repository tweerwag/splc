extern crate splc;

use splc::driver::{SPL2Coq, main_driver};

fn main() {
    main_driver(SPL2Coq);
}
